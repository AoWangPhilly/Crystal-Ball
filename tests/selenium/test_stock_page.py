from selenium import webdriver
from selenium.webdriver.common.by import By

# Set up the web driver
driver = webdriver.Chrome()

# Navigate to the web page with the table
url = "https://crystal-ball.herokuapp.com/dash/stock"

driver.get(url)
driver.implicitly_wait(3)

# Find the table
table = driver.find_element(
    By.XPATH, "/html/body/div/div/div/div[1]/div/div/div[2]/div/div[2]/div[2]/table/tbody")

# Find all the table cells headers
header_cells = table.find_elements(By.XPATH, "//tr/th/div/span")

valid_headers = ["Previous Close", "Day Range", "Year Range",
                 "Market Cap", "Avg Volume", "P/E Ratio", "Dividend Yield"]

for cell in header_cells:
    if cell.text not in valid_headers:
        print(cell.text + " is not a valid header")
if valid_headers:
    print("All headers are valid")

# Loop through the cells to check if any are empty
cells = table.find_elements(By.XPATH, "//tr")
empty_cells = []
for cell in cells:
    if not cell.text.strip():
        empty_cells.append(cell)

# Output the results
if len(empty_cells) == 0:
    print("There are no empty cells in the table.")
else:
    print("There are", len(empty_cells), "empty cells in the table")
    for cell in empty_cells:
        print("Row:", cell.get_attribute("data-row"),
              "Column:", cell.get_attribute("data-col"))

# Loop through the cells to check if color of pill is correct
invalid_cell = False
for cell in cells:
    cellTextList = cell.text.split("\n")
    for text in cellTextList:
        if text not in cellTextList:
            print("Cell not found")
            invalid_cell = True
            
if not invalid_cell:
    print("Not all cells were found")

# Test if charts are present
# Find the chartss
charts = driver.find_element(
    By.XPATH, "/html/body/div/div/div/div[1]/div/div/div[2]/div/chart/tbody")

for chart in charts:
    if "Generated" in table:
        print(f"{chart} was generated")
    else:
        print(f"{chart} was failed to generate")


# Find the news cards
news_cards = driver.find_element(
    By.XPATH, "/html/body/div/div/div/div[1]/div/div/div[2]/div/news/cards")

for news in news_cards:
    if "Article" in news:
        print(f"{news} was generated")
    else:
        print(f"{news} was failed to generate")
        
# Close the web driver
driver.quit()

print("Test Completed Successfully")
