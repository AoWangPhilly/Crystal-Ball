from selenium import webdriver
from selenium.webdriver.common.by import By

# Set up the web driver
driver = webdriver.Chrome()

# Navigate to the web page with the table
url = "https://crystal-ball.herokuapp.com/dash/news"

driver.get(url)
driver.implicitly_wait(3)

# Find the cards
cards = driver.find_elements(By.XPATH, "/html/body/div/div/div/div[1]/div")

# Check the elements in each card
has_valid_elements = True
for card in cards:
    children = card.find_elements(By.XPATH, ".//*")
    if len(children) != 7:
        print("Card does not have all valid elements")
        has_valid_elements = False

if has_valid_elements:
    print("All cards have the required elements")


# Close the web driver
driver.quit()

print("Test Completed Successfully")
