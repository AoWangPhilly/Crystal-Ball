from selenium import webdriver
from selenium.webdriver.common.by import By

# Set up the web driver
driver = webdriver.Chrome()

# Navigate to the web page with the table
url = "https://crystal-ball.herokuapp.com/dash/trending"

driver.get(url)
driver.implicitly_wait(3)

# Find the table
table = driver.find_element(
    By.XPATH, "/html/body/div/div/div/div[1]/div/div/div[2]/div/div[2]/div[2]/table/tbody")

# Find all the table cells headers
header_cells = table.find_elements(By.XPATH, "//tr/th/div/span")

valid_headers = ["Name", "Price", "Change",
                 "% Change", "Volume", "Market Cap", "PE Ratio"]

for cell in header_cells:
    if cell.text not in valid_headers:
        print(cell.text + " is not a valid header")
if valid_headers:
    print("All headers are valid")

# Loop through the cells to check if any are empty
cells = table.find_elements(By.XPATH, "//tr")
empty_cells = []
for cell in cells:
    if not cell.text.strip():
        empty_cells.append(cell)

# Output the results
if len(empty_cells) == 0:
    print("There are no empty cells in the table.")
else:
    print("There are", len(empty_cells), "empty cells in the table")
    for cell in empty_cells:
        print("Row:", cell.get_attribute("data-row"),
              "Column:", cell.get_attribute("data-col"))

# Loop through the cells to check if color of pill is correct
invalid_colors = False
for cell in cells:
    pills = cell.find_elements(By.CLASS_NAME, "rounded-pill")
    pill_class = ""
    for pill in pills:
        pill_class = pill.get_attribute("class")
    cellTextList = cell.text.split("\n")
    for text in cellTextList:
        badge_color = ""
        if "%" in text and "Change" not in text:
            percent = float(text.strip('%'))
            if percent >= 1:
                badge_color = "bg-success"
            elif percent <= -1:
                badge_color = "bg-danger"
            else:
                badge_color = "bg-warning"
        if badge_color not in pill_class:
            print("Colors do not match")
            invalid_colors = True
            
if not invalid_colors:
    print("All colors match to their respective range")

# Close the web driver
driver.quit()

print("Test Completed Successfully")
