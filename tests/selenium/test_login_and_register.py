import random
import string
from selenium import webdriver
from selenium.webdriver.common.by import By


# Generate a random name
letters = string.ascii_lowercase
name = ''.join(random.choice(letters) for i in range(10))

# Generate a random email
email = ''.join(random.choice(letters) for i in range(8))
email += '@example.com'

# Generate a random password
password = ''.join(random.choice(string.ascii_letters + string.digits)
                   for i in range(10))

# Set up the web driver
driver = webdriver.Chrome()
driver.implicitly_wait(3)

# Navigate to the registration page
driver.get("https://crystal-ball.herokuapp.com/dash/register")

# Find the name, email, and password fields and enter the generated values
name_field = driver.find_element(By.ID, "name")
name_field.send_keys(name)

email_field = driver.find_element(By.ID, "email")
email_field.send_keys(email)

password_field = driver.find_element(By.ID, "password")
password_field.send_keys(password)

# Click the register button
register_button = driver.find_element(By.ID, "submit")
register_button.click()

# Close the web driver
driver.quit()
