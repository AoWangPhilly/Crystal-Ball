import unittest
import datetime

import numpy as np
import pandas as pd

from src.monte_carlo.model import MonteCarloModel
from src.monte_carlo.wellness_calculator import WellnessCalculator
from src.stock_data.aggregator import StockDataAggregator


class TestWellnessCalculator(unittest.TestCase):

    def CreateWellnessCalc(self):
        agg = StockDataAggregator(stock_symbols="AAPL")
        stock_data = agg.get_historical_stock_price()
        wellness = WellnessCalculator(stock_data=stock_data)
        return wellness

    def test_accuracy_greater_than_zero(self):
        wellness = self.CreateWellnessCalc()
        accuracy = wellness.calculate_accuracy(datetime.datetime.now() - datetime.timedelta(days=252))
        self.assertGreater(accuracy, 0)


if __name__ == "__main__":
    unittest.main()












