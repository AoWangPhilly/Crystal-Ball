import datetime
import unittest

import numpy as np
import yfinance as yf
import pandas as pd
from src.monte_carlo.model import (
    generate_predicted_prices,
    show_historical_and_predicted_price,
    graph_monte_carlo_simulations,
    graph_predicted_price_distribution,
    graph_returns_distribution,
)
from src.stock_data.aggregator import StockDataAggregator


class TestModel(unittest.TestCase):
    # def CreateModel(self):
    #     np.random.seed(0)
    #     agg = StockDataAggregator(stock_symbols="AAPL")
    #     stock_data = agg.get_historical_stock_price()
    #     #model = MonteCarloModel(stock_data=stock_data, number_of_days_to_predict=252)
    #     generate_predicted_prices(stock_data)
    #     return model

    def get_final_predicted_stock_price(self, ticker="AAPL"):
        np.random.seed(0)
        # agg = StockDataAggregator(stock_symbols=ticker)
        # stock_data = agg.get_historical_stock_price()
        data = yf.download(
            tickers=[ticker, "^GSPC"], start=datetime.date(2022, 5, 7), end=datetime.date(2023, 5, 7)
        )
        predict_for_date = datetime.date(2024, 5, 7)
        adjusted_close = data["Adj Close"]
        predicted_prices = generate_predicted_prices(
            data=adjusted_close,
            end=predict_for_date,
            trials=100,
        )
        means = predicted_prices.mean(axis=1)
        price = means.iloc[-1]
        return price

    def test_price_prediction_greater_than_zero(self):  # test will fail if AAPL is predicted to hit 0.
        # model = self.CreateModel()
        price = self.get_final_predicted_stock_price()  # model.predict_stock_price()
        self.assertGreater(price, 0, "Price of AAPL predicted to be 0!")

    def test_price_prediction_greater_than_1(self):  # test will fail if AAPL is predicted to hit $1.
        # model = self.CreateModel()
        price = self.get_final_predicted_stock_price()  # model.predict_stock_price()
        self.assertGreater(price, 1, "Price of AAPL predicted to be 1!")

    # def test_set_stock_data(self):
    #     #model = self.CreateModel()
    #     agg = StockDataAggregator(stock_symbols="TSLA")
    #     stock_data = agg.get_historical_stock_price()
    #     model.set_stock_data(stock_data)
    #     pd.testing.assert_frame_equal(model.stock_data, stock_data)

    # def test_closing_prices_greater_than_zero(self):
    #     #model = self.CreateModel()
    #     model.predict_stock_price()
    #     for i in model.closing_prices:
    #         self.assertGreater(i, 0)
    def test_set_stock_price_produces_different_price(self):  # test will fail if aapl stock price = amc stock price
        # aapl_model = self.CreateModel()
        # amc_model = self.CreateModel()
        # agg = StockDataAggregator(stock_symbols="AMC")
        # stock_data = agg.get_historical_stock_price()
        # amc_model.set_stock_data(stock_data)
        aapl_price = self.get_final_predicted_stock_price("AAPL")  # aapl_model.predict_stock_price()
        amc_price = self.get_final_predicted_stock_price("AMC")  # amc_model.predict_stock_price()
        self.assertNotEqual(aapl_price, amc_price, "Price of AAPL and AMC predicted to be the same!")


if __name__ == "__main__":
    unittest.main()
