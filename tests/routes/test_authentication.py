import time

from fastapi.testclient import TestClient
from pages.utils import BASE_URL
from app import app

client = TestClient(app)


def test_put_without_login():
    email = "johndoe@example.com"
    new_account = {
        "first_name": "John",
        "password": "johndoe123",
        "is_admin": True
    }
    response = client.put(f"{BASE_URL}accounts/{email}", json=new_account)
    assert response.status_code == 401
    assert response.json()["detail"] == "Not authenticated"


def test_put_without_admin_permissions():
    email = "oliviaw@example.com"
    password = "test"
    new_account = {
        "first_name": "John-edit",
        "password": "test1234567",
        "is_admin": True
    }
    data = {"username": email, "password": password}
    creds = client.post(f"{BASE_URL}login", data=data)
    bearer_token = creds.json().get("access_token")
    response = client.put(
        f"{BASE_URL}accounts/{email}",
        json=new_account,
        headers={"Authorization": f"Bearer {bearer_token}"}
    )
    assert response.status_code == 403
    assert response.json()["detail"] == "Need admin permissions to update"

    client.get(
        f"{BASE_URL}logout",
        headers={"Authorization": f"Bearer {bearer_token}"}
    )


def test_put_with_admin_permission():
    email = "janedoe@example.com"
    password = "test"
    edit_email = "oliviaw@example.com"
    new_account = {
        "first_name": "Oliva-edit",
        "password": "oliviadoe123",
        "is_admin": False
    }
    data = {"username": email, "password": password}
    creds = client.post(f"{BASE_URL}login", data=data)
    bearer_token = creds.json().get("access_token")

    response = client.put(
        f"{BASE_URL}accounts/{edit_email}",
        json=new_account,
        headers={"Authorization": f"Bearer {bearer_token}"}
    )
    new_name = client.get(f"{BASE_URL}accounts/{edit_email}")

    assert response.status_code == 204
    assert new_name.json()["first_name"] == new_account["first_name"]

    client.get(
        f"{BASE_URL}logout",
        headers={"Authorization": f"Bearer {bearer_token}"}
    )


def test_delete_without_admin_permissions():
    email = "oliviaw@example.com"
    password = "oliviadoe123"

    delete_email = "janedoe@example.com"
    data = {"username": email, "password": password}
    creds = client.post(f"{BASE_URL}login", data=data)
    bearer_token = creds.json().get("access_token")

    response = client.delete(
        f"{BASE_URL}accounts/{delete_email}",
        headers={"Authorization": f"Bearer {bearer_token}"}
    )

    assert response.status_code == 403

    client.get(
        f"{BASE_URL}logout",
        headers={"Authorization": f"Bearer {bearer_token}"}
    )


def test_delete_with_admin_permissions():
    email = "janedoe@example.com"
    password = "test"

    delete_email = "johndoe@example.com"
    data = {"username": email, "password": password}
    creds = client.post(f"{BASE_URL}login", data=data)
    bearer_token = creds.json().get("access_token")

    resp = client.delete(
        f"{BASE_URL}accounts/{delete_email}",
        headers={"Authorization": f"Bearer {bearer_token}"}
    )
    time.sleep(1)
    new_name = client.get(f"{BASE_URL}accounts/{delete_email}")

    assert new_name.status_code == 404

    client.get(
        f"{BASE_URL}logout",
        headers={"Authorization": f"Bearer {bearer_token}"}
    )
