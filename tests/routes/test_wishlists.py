from fastapi.testclient import TestClient
from pages.utils import BASE_URL
from app import app
import src.models as models
from src.database import engine, create_db, get_db_context
from sqlalchemy_utils import drop_database
from src.database import SQLALCHEMY_DATABASE_URL

client = TestClient(app)

def test_get_wishlists():
    response = client.get(f"{BASE_URL}wishlist")
    print(f"{BASE_URL}wishlist")
    assert response.status_code == 200
    assert len(response.json()) == 2

def test_invalid_stock_ticker():
    new_wishlist = {
        "email": "johndoe@example.com",
        "name": "John 3",
        "stock_symbols": ["AAPL", "GOOOG"]
    }
    response = client.post(f"{BASE_URL}wishlist/", json=new_wishlist)
    assert response.status_code == 422
    assert response.json()["detail"] == "The following stock ticker symbols are invalid: GOOOG"


def test_multiple_invalid_stock_tickers():
    new_wishlist = {
        "email": "johndoe@example.com",
        "name": "John 3",
        "stock_symbols": ["APPL", "GOOOG"]
    }
    response = client.post(f"{BASE_URL}wishlist/", json=new_wishlist)
    assert response.status_code == 422
    assert response.json()["detail"] == "The following stock ticker symbols are invalid: APPL, GOOOG"

def test_duplicate_wishlist_name():
    new_wishlist = {
        "email": "johndoe@example.com",
        "name": "John 1",
        "stock_symbols": ["AAPL", "GOOGL"]
    }
    response = client.post(f"{BASE_URL}wishlist/", json=new_wishlist)
    assert response.status_code == 422
    assert response.json()["detail"] == "johndoe@example.com already has a wishlist called John 1"

def test_invalid_wishlist_name():
    new_wishlist = {
        "email": "johndoe@example.com",
        "name": "John 1^",
        "stock_symbols": ["AAPL", "GOOGL"]
    }
    response = client.post(f"{BASE_URL}wishlist/", json=new_wishlist)
    assert response.status_code == 422
    assert response.json()["detail"] == "Wishlist name should not contain invalid special characters: !@#$%^&*"