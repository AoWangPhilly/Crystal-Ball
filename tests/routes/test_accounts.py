from fastapi.testclient import TestClient
from pages.utils import BASE_URL
from app import app

client = TestClient(app)


def test_get_accounts():
    response = client.get(f"{BASE_URL}accounts")
    assert response.status_code == 200
    assert len(response.json()) == 18


def test_get_account_by_nonexistent_email():
    email: str = "aow@example.com"
    response = client.get(f"{BASE_URL}accounts/{email}")
    assert response.status_code == 404


def test_get_account_by_existing_email():
    email: str = "johndoe@example.com"
    response = client.get(f"{BASE_URL}accounts/{email}")
    assert response.status_code == 200
    assert response.json()["email"] == email


def test_get_account_by_email_after_post():
    new_account = {
        "email": "aodoe@example.com",
        "first_name": "AoW",
        "password": "aodoe123",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 201
    account = client.get(f"{BASE_URL}accounts/{new_account['email']}")
    assert account.json()["email"] == new_account["email"]


def test_post_with_existing_email():
    new_account = {
        "email": "johndoe@example.com",
        "first_name": "John",
        "password": "Johndoe123",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 400
    assert response.json()["detail"] == "johndoe@example.com is taken"


def test_post_with_existing_case_insensitive_email():
    new_account = {
        "email": "JOHNDOE@example.com",
        "first_name": "John",
        "password": "Johndoe123",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 400
    assert response.json()["detail"] == "johndoe@example.com is taken"


def test_post_with_invalid_email():
    new_account = {
        "email": "Hello",
        "first_name": "John",
        "password": "Johndoe123",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "value is not a valid email address"


def test_post_with_empty_name():
    new_account = {
        "email": "aow@example.com",
        "first_name": "",
        "password": "aowang123",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "first name must not be empty"


def test_post_with_short_name():
    new_account = {
        "email": "aow@example.com",
        "first_name": "Ao",
        "password": "aowang123",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "first name must be at least 3 characters"


def test_post_with_long_name():
    new_account = {
        "email": "aow@example.com",
        "first_name": "".join(["a" for _ in range(51)]),
        "password": "aowang123",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "first name must be at most 50 characters"


def test_post_with_empty_password():
    new_account = {
        "email": "aow@example.com",
        "first_name": "AoW",
        "password": "",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "password must not be empty"


def test_post_with_short_password():
    new_account = {
        "email": "aow@example.com",
        "first_name": "AoW",
        "password": "a",
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "password must be at least 8 characters"


def test_post_with_long_password():
    new_account = {
        "email": "aow@example.com",
        "first_name": "AoW",
        "password": "".join(["a" for _ in range(101)]),
    }
    response = client.post(f"{BASE_URL}accounts/", json=new_account)
    assert response.status_code == 422
    assert response.json()["detail"][0]["msg"] == "password must be at most 100 characters"
