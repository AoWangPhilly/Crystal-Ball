import unittest
from src.stock_data.stock_info import StockInfo

class TestStockInfo(unittest.TestCase):
    def setUp(self):
        self.stock = StockInfo("MSFT")
    
    def test_get_info(self):
        info = self.stock.get_info()
        self.assertIsNotNone(info)
        self.assertIn("regularMarketPrice", info)
    
    def test_get_history(self):
        history = self.stock.get_history(period="2d", interval="5m")
        self.assertIsNotNone(history)
        self.assertGreater(len(history), 0)
    
    def test_get_dividends(self):
        dividends = self.stock.get_dividends()
        self.assertIsNotNone(dividends)
        self.assertGreater(len(dividends), 0)
    
    def test_get_splits(self):
        splits = self.stock.get_splits()
        self.assertIsNotNone(splits)
        self.assertGreater(len(splits), 0)
    
    def test_get_recommendations(self):
        recommendations = self.stock.get_recommendations()
        self.assertIsNotNone(recommendations)
        self.assertGreater(len(recommendations), 0)
    
    def test_get_earnings(self):
        earnings = self.stock.get_earnings()
        self.assertIsNotNone(earnings)
        self.assertGreater(len(earnings), 0)

    def test_get_live_price(self):
        price = self.stock.get_live_price()
        self.assertIsNotNone(price)
        self.assertGreater(price, 0)

    def test_get_valuation_stats(self):
        valuation_stats = self.stock.get_valuation_stats()
        self.assertIsNotNone(valuation_stats)
        self.assertGreater(len(valuation_stats), 0)

    def test_get_balance_sheet(self):
        balance_sheet = self.stock.get_balance_sheet()
        self.assertIsNotNone(balance_sheet)
        self.assertTrue(isinstance(balance_sheet, dict))
        
    def test_get_income_statement(self):
        income_statement = self.stock.get_income_statement()
        self.assertIsNotNone(income_statement)
        self.assertTrue(isinstance(income_statement, dict))
        
    def test_get_cash_flow_statement(self):
        cash_flow_statement = self.stock.get_cash_flow_statement()
        self.assertIsNotNone(cash_flow_statement)
        self.assertTrue(isinstance(cash_flow_statement, dict))

    def test_get_info_table(self):
        table = self.stock.get_info_table()
        self.assertIsNotNone(table)
        self.assertGreater(len(table), 0)

    def test_get_history_table(self):
        table = self.stock.get_history_table(period="5d", interval="15m")
        self.assertIsNotNone(table)
        self.assertGreater(len(table), 0)

    def test_get_dividends_table(self):
        table = self.stock.get_dividends_table()
        self.assertIsNotNone(table)
        self.assertGreater(len(table), 0)

    def test_get_splits_table(self):
        table = self.stock.get_splits_table()
        self.assertIsNotNone(table)
        self.assertGreater(len(table), 0)

    def test_get_recommendations_table(self):
        table = self.stock.get_recommendations_table()
        self.assertIsNotNone(table)
        self.assertGreater(len(table), 0)

    def test_get_earnings_table(self):
        table = self.stock.get_earnings_table()
        self.assertIsNotNone(table)
        self.assertGreater(len(table), 0)

    def test_get_valuation_stats_table(self):
        table = self.stock.get_valuation_stats_table()
        self.assertIsNotNone(table)
        self.assertGreater(len(table), 0)

    def test_get_balance_sheet_table(self):
        table = self.stock.get_balance_sheet_table()
        self.assertIsNotNone(table)
        self.assertGreater(len(table), 0)

if __name__ == '__main__':
    unittest.main()