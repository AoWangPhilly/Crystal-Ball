import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.models import Account, Wishlist, TrendingArticle, RecommendedArticle, PredictorModel, Base


class TestModels(unittest.TestCase):
    def setUp(self):
        self.engine = create_engine('sqlite:///:memory:')
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def test_account_model(self):
        # Test inserting a record into the 'accounts' table
        account = Account(email='test@example.com', first_name='Test', password='password')
        self.session.add(account)
        self.session.commit()

        # Test retrieving a record from the 'accounts' table
        result = self.session.query(Account).first()
        self.assertEqual(result.email, 'test@example.com')
        self.assertEqual(result.first_name, 'Test')
        self.assertEqual(result.password, 'password')
        self.assertFalse(result.is_admin)

    def test_wishlist_model(self):
        # Test inserting a record into the 'wishlists' table
        account = Account(email='test@example.com', first_name='Test', password='password')
        self.session.add(account)
        self.session.commit()

        wishlist = Wishlist(email='test@example.com', name='Test Wishlist', stock_symbols=['AAPL', 'GOOG'])
        self.session.add(wishlist)
        self.session.commit()

        # Test retrieving a record from the 'wishlists' table
        result = self.session.query(Wishlist).first()
        self.assertEqual(result.email, 'test@example.com')
        self.assertEqual(result.name, 'Test Wishlist')
        self.assertListEqual(result.stock_symbols, ['AAPL', 'GOOG'])

    def test_trending_article_model(self):
        # Test inserting a record into the 'trending_articles' table
        article = TrendingArticle(stock_symbols=['AAPL'], article_url='https://www.example.com/article1',
                                  thumbnail_url='https://www.example.com/thumbnail1')
        self.session.add(article)
        self.session.commit()

        # Test retrieving a record from the 'trending_articles' table
        result = self.session.query(TrendingArticle).first()
        self.assertListEqual(result.stock_symbols, ['AAPL'])
        self.assertEqual(result.article_url, 'https://www.example.com/article1')
        self.assertEqual(result.thumbnail_url, 'https://www.example.com/thumbnail1')

    def test_recommended_article_model(self):
        # Test inserting a record into the "recommended_articles" table
        email = "test_user@test.com"
        stock_symbols = ["AAPL", "GOOG", "TSLA"]
        published_at = datetime.now()
        article_url = "https://test.com/test_article"
        thumbnail_url = "https://test.com/test_thumbnail.png"
        sentiment_result = 0.5

        recommended_article = RecommendedArticle(email=email, stock_symbols=stock_symbols,
                                                 published_at=published_at, article_url=article_url,
                                                 thumbnail_url=thumbnail_url, sentiment_result=sentiment_result)

        self.session.add(recommended_article)
        self.session.commit()

        # Test querying a record from the "recommended_articles" table
        result = self.session.query(RecommendedArticle).filter_by(email=email).first()

        self.assertIsNotNone(result)
        self.assertEqual(result.email, email)
        self.assertEqual(result.stock_symbols, stock_symbols)
        self.assertEqual(result.published_at, published_at)
        self.assertEqual(result.article_url, article_url)
        self.assertEqual(result.thumbnail_url, thumbnail_url)
        self.assertEqual(result.sentiment_result, sentiment_result)

        # Test updating a record in the "recommended_articles" table
        new_sentiment_result = 0.7
        result.sentiment_result = new_sentiment_result

        self.session.commit()

        updated_result = self.session.query(RecommendedArticle).filter_by(email=email).first()
        self.assertEqual(updated_result.sentiment_result, new_sentiment_result)

        # Test deleting a record from the "recommended_articles" table
        self.session.delete(updated_result)
        self.session.commit()

        deleted_result = self.session.query(RecommendedArticle).filter_by(email=email).first()
        self.assertIsNone(deleted_result)

    def test_predictor_models(self):
        # test predictor_models creation
        email = 'user1@email.com'
        model_id = 1
        parameter = {"param1": "value1", "param2": "value2"}
        output = ["output1", "output2"]
        duration = 10.5
        accuracy = 0.75
        predictor_model = PredictorModel(
            email=email,
            model_id=model_id,
            parameter=parameter,
            output=output,
            duration=duration,
            accuracy=accuracy
        )
        self.session.add(predictor_model)
        self.session.commit()

        # test predictor_models retrieval
        retrieved_predictor_model = self.session.query(
            PredictorModel).filter_by(email=email).first()
        self.assertEqual(retrieved_predictor_model.model_id, model_id)
        self.assertEqual(retrieved_predictor_model.parameter, parameter)
        self.assertEqual(retrieved_predictor_model.output, output)
        self.assertEqual(retrieved_predictor_model.duration, duration)
        self.assertEqual(retrieved_predictor_model.accuracy, accuracy)

        # test predictor_models deletion
        self.session.delete(predictor_model)
        self.session.commit()
        deleted_predictor_model = self.session.query(
            PredictorModel).filter_by(email=email).first()
        self.assertIsNone(deleted_predictor_model)
