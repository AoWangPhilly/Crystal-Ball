import unittest
from src.config import settings
from src.classes.recommended_articles.recommended import NewsRecommender
from src.classes.wishlist.wishlist import Wishlist

username = settings.database_username
password = settings.database_password
hostname = settings.database_hostname
port = settings.database_port
db_name = settings.database_name
api_key = settings.news_api_key


class TestNewsRecommender(unittest.TestCase):

    def setUp(self):
        self.news_recommender = NewsRecommender(api_key=api_key)
        self.wishlist = Wishlist()
        self.wishlist.add_ticker('AAPL')

    def test_get_top_headlines_with_wishlist(self):
        response = self.news_recommender.get_top_headlines(wishlist=self.wishlist)
        self.assertEqual(response['status'], 'ok')
        articles = response['articles']
        tickers = self.wishlist.tickers
        for ticker in tickers:
            contains_ticker = False
            for article in articles:
                if ticker in article['title'] or ticker in article['description']:
                    contains_ticker = True
            self.assertTrue(contains_ticker)

    def test_preprocess_json(self):
        data = {
            'status': 'ok',
            'articles': [
                {
                    'source': {'name': 'CNN'},
                    'author': 'John Smith',
                    'title': 'Apple unveils new iPhone',
                    'description': 'Apple unveils new iPhone with 5G technology.',
                    'publishedAt': '2022-01-01T12:00:00Z',
                    'content': 'Apple has unveiled its latest iPhone with 5G technology...',
                    'url': 'https://www.cnn.com/2022/01/01/tech/apple-iphone-5g/index.html',
                    'urlToImage': 'https://cdn.cnn.com/cnnnext/dam/assets/220101125938-apple-iphone-5g.jpg'
                }
            ]
        }
        df = self.news_recommender.preprocess_json(data)
        self.assertEqual(len(df), 1)
        self.assertEqual(df['title'][0], 'Apple unveils new iPhone')

    def test_recommend_with_wishlist(self):
        df = self.news_recommender.recommend(wishlist=self.wishlist)
        tickers = self.wishlist.tickers
        contains_ticker = False
        for index, row in df.iterrows():
            # Ensure that at least one of the stock tickers is in the article
            for ticker in tickers:
                if ticker in row['title'] or ticker in row['description']:
                    contains_ticker = True
                    break
            if contains_ticker:
                break
        self.assertTrue(contains_ticker)

    def test_preprocess_json_empty(self):
        data = {
            'status': 'ok',
            'articles': []
        }
        df = self.news_recommender.preprocess_json(data)
        self.assertEqual(len(df), 0)

    def test_preprocess_json_missing_fields(self):
        data = {
            'status': 'ok',
            'articles': [
                {
                    'source': {'name': 'CNN'},
                    'author': None,
                    'title': 'Apple unveils new iPhone',
                    'description': 'Apple unveils new iPhone with 5G technology.',
                    'publishedAt': '2022-01-01T12:00:00Z',
                    'content': None,
                    'url': 'https://www.cnn.com/2022/01/01/tech/apple-iphone-5g/index.html',
                    'urlToImage': 'https://cdn.cnn.com/cnnnext/dam/assets/220101125938-apple-iphone-5g.jpg'
                }
            ]
        }
        df = self.news_recommender.preprocess_json(data)
        self.assertEqual(len(df), 0)


if __name__ == '__main__':
    unittest.main()
