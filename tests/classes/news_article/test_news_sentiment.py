import unittest
from src.classes.news_article.news_sentiment import NewsSentiment
from src.config import settings 

api_key = settings.news_api_key

class TestNewsSentiment(unittest.TestCase):
    def setUp(self):
        self.news_sentiment = NewsSentiment(news_api_key= api_key)

    def test_analyze_sentiment(self):
        text = "This is a positive text."
        sentiment = self.news_sentiment.analyze_sentiment(text)
        self.assertGreater(sentiment, 0.0)

        text = "This is a negative text."
        sentiment = self.news_sentiment.analyze_sentiment(text)
        self.assertLess(sentiment, 0.0)

        text = "This is a neutral text."
        sentiment = self.news_sentiment.analyze_sentiment(text)
        self.assertEqual(sentiment, 0.0)

    def test_categorize_sentiment(self):
        sentiment = 0.1
        category = self.news_sentiment.categorize_sentiment(sentiment)
        self.assertEqual(category, "Positive")

        sentiment = -0.1
        category = self.news_sentiment.categorize_sentiment(sentiment)
        self.assertEqual(category, "Negative")

        sentiment = 0.0
        category = self.news_sentiment.categorize_sentiment(sentiment)
        self.assertEqual(category, "Neutral")

if __name__ == "__main__":
    unittest.main()
