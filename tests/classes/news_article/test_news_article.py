import datetime
import unittest
import requests_mock
from unittest.mock import patch, mock_open
from src.classes.news_article.news_article import NewsAPI
import json
import pandas as pd
import requests

from src.config import settings

username = settings.database_username
password = settings.database_password
hostname = settings.database_hostname
port = settings.database_port
db_name = settings.database_name


class TestNewsAPI(unittest.TestCase):
    def test_init(self):
        api_key = settings.news_api_key
        news_api = NewsAPI(api_key)
        assert news_api.api_key == api_key

    def test_get_top_headlines(self):
        api_key = settings.news_api_key
        news_api = NewsAPI(api_key)
        url = f'https://newsapi.org/v2/top-headlines?country=us&apiKey={api_key}'
        response = requests.get(url)
        assert response.status_code == 200
        assert response.json() == news_api.get_top_headlines()

    
    def test_get_top_headlines_country(self):
        api_key = settings.news_api_key
        news_api = NewsAPI(api_key)
        url = f'https://newsapi.org/v2/top-headlines?country=us&apiKey={api_key}'
        response = requests.get(url)
        assert response.status_code == 200
        assert news_api.get_top_headlines(country="gb")

    def test_get_top_headlines_category(self):
        api_key = settings.news_api_key
        news_api = NewsAPI(api_key)
        url = f'https://newsapi.org/v2/top-headlines?country=us&apiKey={api_key}'
        response = requests.get(url)
        assert response.status_code == 200
        assert news_api.get_top_headlines(category="business")

    def test_preprocess_json(self):
        newsapi = NewsAPI(None)
        # Sample JSON data
        json_data = {
            "articles": [
                {
                    "source": {"name": "CNN"},
                    "author": "John Doe",
                    "title": "Article 1",
                    "description": "Description of Article 1",
                    "publishedAt": "2022-01-01T12:00:00Z",
                    "content": "Content of Article 1",
                    "url": "https://www.cnn.com/article1",
                    "urlToImage": "https://www.cnn.com/article1/image.jpg"
                },
                {
                    "source": {"name": "BBC"},
                    "author": "Jane Doe",
                    "title": "Article 2",
                    "description": "Description of Article 2",
                    "publishedAt": "2022-01-02T12:00:00Z",
                    "content": "Content of Article 2",
                    "url": "https://www.bbc.com/article2",
                    "urlToImage": "https://www.bbc.com/article2/image.jpg"
                }
            ]
        }

        processed_data = newsapi.preprocess_json(json_data)
        assert isinstance(processed_data, pd.DataFrame)

        assert len(processed_data) == 2

        assert processed_data.iloc[0].to_dict() == {
            "source": "CNN",
            "author": "John Doe",
            "title": "Article 1",
            "description": "Description of Article 1",
            "publishedAt": "2022-01-01T12:00:00Z",
            "content": "Content of Article 1",
            "url": "https://www.cnn.com/article1",
            "urlToImage": "https://www.cnn.com/article1/image.jpg"}

        assert processed_data.iloc[1].to_dict() == {
            "source": "BBC",
            "author": "Jane Doe",
            "title": "Article 2",
            "description": "Description of Article 2",
            "publishedAt": "2022-01-02T12:00:00Z",
            "content": "Content of Article 2",
            "url": "https://www.bbc.com/article2",
            "urlToImage": "https://www.bbc.com/article2/image.jpg"}

    def test_get_headlines(self):
        api_key = settings.news_api_key
        news_api = NewsAPI(api_key)

        # Test retrieving latest news articles
        latest_news = news_api.get_headlines(keywords="stock market")
        assert latest_news["status"] == "ok"

        # Test retrieving news articles from the last week
        from_date = (datetime.datetime.now() - datetime.timedelta(days=7)).strftime("%Y-%m-%d")
        to_date = datetime.datetime.now().strftime("%Y-%m-%d")
        last_week_news = news_api.get_headlines(keywords="stock market", from_date=from_date, to_date=to_date)
        assert last_week_news["status"] == "ok"

    def test_get_headlines_time_range(self):
        api_key = settings.news_api_key
        news_api = NewsAPI(api_key)

        # Test retrieving news articles for a specific time range
        from_date = "2022-03-01"
        to_date = "2022-03-15"
        news = news_api.get_headlines(keywords="tech", from_date=from_date, to_date=to_date)
        assert news["status"] == "ok"

    def test_get_headlines_from_source(self):
        api_key = settings.news_api_key
        news_api = NewsAPI(api_key)

        # Test retrieving news articles from a specific source
        source = "bbc-news"
        news = news_api.get_headlines(keywords="tech", sources=[source])
        assert news["status"] == "ok"
        assert all(article["source"]["id"] == source for article in news["articles"])

    def test_get_headlines_by_category(self):
        api_key = settings.news_api_key
        news_api = NewsAPI(api_key)

        # Test retrieving news articles by category
        category = "technology"
        news = news_api.get_headlines(category=category)
        assert news["status"] == "ok"
        assert all(article["category"].lower() == category for article in news["articles"])

if __name__ == '__main__':
    unittest.main()
