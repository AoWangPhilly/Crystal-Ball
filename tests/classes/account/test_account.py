import os
from datetime import datetime
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src import models, schemas
from src.classes.account.account import Account

# set up test database
TEST_DATABASE_URL = "sqlite:///./test.db"

@pytest.fixture(scope="module")
def db():
    engine = create_engine(TEST_DATABASE_URL, connect_args={"check_same_thread": False})
    TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = TestingSessionLocal()
    models.Base.metadata.create_all(bind=engine)

    yield session

    session.close()
    models.Base.metadata.drop_all(bind=engine)
    os.remove("test.db")

def test_create_account(db):
    account_data = {"email": "test@example.com", "password": "password"}
    account = Account.create_account(account_data)
    assert account.email == "test@example.com"
    assert account.password != "password"
    db_account = db.query(models.Account).filter(models.Account.email == "test@example.com").first()
    assert db_account is not None
    assert db_account.email == "test@example.com"

def test_create_account_with_existing_email(db):
    existing_account = models.Account(email="test@example.com", password="password")
    db.add(existing_account)
    db.commit()

    account_data = {"email": "test@example.com", "password": "password"}
    with pytest.raises(Exception):
        Account.create_account(account_data)

def test_delete_account(db):
    account = models.Account(email="test@example.com", password="password")
    db.add(account)
    db.commit()

    Account.delete_account("test@example.com")
    deleted_account = db.query(models.Account).filter(models.Account.email == "test@example.com").first()
    assert deleted_account is None

def test_delete_nonexistent_account(db):
    Account.delete_account("test@example.com")

def test_get_accounts(db):
    account1 = models.Account(email="test1@example.com", password="password")
    account2 = models.Account(email="test2@example.com", password="password")
    db.add(account1)
    db.add(account2)
    db.commit()

    accounts = Account.get_accounts()
    assert len(accounts) == 2
    assert accounts[0].email == "test1@example.com"
    assert accounts[1].email == "test2@example.com"

def test_print_all_accounts(capsys):
    account1 = models.Account(email="test1@example.com", password="password")
    account2 = models.Account(email="test2@example.com", password="password")
    with capsys.disabled():
        db.add(account1)
        db.add(account2)
        db.commit()
        Account.print_all_accounts()
    captured = capsys.readouterr()
    assert "test1@example.com" in captured.out
    assert "test2@example.com" in captured.out