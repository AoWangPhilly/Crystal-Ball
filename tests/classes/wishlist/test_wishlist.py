import unittest
from src.classes.wishlist.wishlist import Wishlist

class TestWishlist(unittest.TestCase):
    def setUp(self):
        self.wishlist = Wishlist()
        self.ticker = "AAPL"
        self.name = "My Wishlist"

    def test_set_name(self):
        self.wishlist.set_name(self.name)
        self.assertEqual(self.wishlist.name, self.name)

    def test_add_ticker(self):
        self.wishlist.add_ticker(self.ticker)
        self.assertIn(self.ticker, self.wishlist.tickers)

    def test_remove_ticker(self):
        self.wishlist.add_ticker(self.ticker)
        self.wishlist.remove_ticker(self.ticker)
        self.assertNotIn(self.ticker, self.wishlist.tickers)

if __name__ == '__main__':
    unittest.main()
