import numpy as np
import yfinance as yf
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import norm
import pandas as pd
from src.stock_data.aggregator import StockDataAggregator


def main():
    # Get that stock data!!
    agg = StockDataAggregator(stock_symbols="GOOG")
    df = agg.get_historical_stock_price()["Adj Close"]
    print(df)
    
    # Shows the historical stock price
    fig = plt.figure(figsize=(15, 6))    
    plt.plot(df.index, df.values)
    plt.savefig("prototypes/stock-price.png")
    
    # Calculate returns + plot distribution
    fig = plt.figure(figsize=(15, 6))

    log_returns = np.log(1 + df.pct_change())
    sns.distplot(log_returns.iloc[1:])
    plt.xlabel("Daily Return")
    plt.ylabel("Frequency")
    plt.savefig("prototypes/log-return.png")
    
    mean = log_returns.mean()
    var = log_returns.var()
    stdev = log_returns.std()
    drift = mean - 0.5 * var
    print(drift)
    print(stdev)
    days = 252
    trials = 100_000
    Z = norm.ppf(np.random.rand(days, trials)) 

    daily_returns = np.exp(drift + stdev * Z)
    
    price_paths = np.zeros_like(daily_returns)
    
    price_paths[0] = df.iloc[-1]
    for t in range(1, days):
        price_paths[t] = price_paths[t-1]*daily_returns[t]
    print(price_paths)
    fig = plt.figure(figsize=(15, 6))

    plt.plot(pd.DataFrame(price_paths).iloc[:,:10])
    plt.savefig("prototypes/simulations.png")
    
    fig = plt.figure(figsize=(15, 6))
    sns.distplot(pd.DataFrame(price_paths).iloc[-1])
    plt.xlabel(f"Price after {days} days")
    plt.savefig("prototypes/distribution.png")
    
    
if __name__ == "__main__":
    main()

