# Crystal-Ball

### Product Description
Crystal Ball is an application that predicts which company in a specific industry would be a good investment, using performance and growth indicators. The application allows the user to explore within a predefined industry with select competitors and analyze financial statements and other data sources to select the strongest performer. Crystal Ball is intended for a broad audience, from the casual Robinhood investor to the ambitious day trader, who are interested in the stock market. The need for the application is widespread, as more and more people are interested in their capital growth and are often clueless about which stocks to buy.

### Team Members
* Hung Do - *Developer*
* Justina Doan - *Developer*
* Ryan Hoffecker - *Developer*
* Justin Nguyen - *Developer/Product Owner*
* Amanjyot Singh - *Developer*
* Ao Wang - *Developer*
* Sarah Yang - *Developer*

### Stakeholders - *The Vanguard Group*
* Xiangyu Chen - *Technical Lead*
* George Buozis - *Senior IT Manager*
* Conor Ryan - *Senior Investment Product Manager*
* Basil Nation - *Product Owner*
