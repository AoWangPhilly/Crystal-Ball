import dash
import dash_bootstrap_components as dbc
import flask
import requests
from dash_extensions.enrich import (
    Output,
    DashProxy,
    Input,
    State,
    MultiplexerTransform,
    html,
    dcc,
    ALL,
)
import dash_mantine_components as dmc

from src.config import settings
from pages.utils import BASE_URL
from src.models import Account
from src.database import get_db_context, r

investor_tabs = ["News", "Stock", "Trending", "Model", "Wishlist", "Logout", "Profile"]
public_tabs = ["Register", "News", "Stock", "Trending", "Login"]
admin_tabs = [
    "News",
    "Stock",
    "Trending",
    "Model",
    "Wishlist",
    "Admin",
    "Logout",
    "Profile",
]


def get_columns_given_roles(is_logged_in: bool, account_id: str):
    with get_db_context() as db:
        if not is_logged_in or account_id is None:
            return public_tabs

        account: Account = db.query(Account).get(account_id)
        return admin_tabs if account.is_admin else investor_tabs


def create_dash_app(requests_pathname_prefix: str = None) -> dash.Dash:
    CRYSTALBALL_LOGO = "assets/images/crystalball-logo.png"

    external_stylesheets = [
        "https://fonts.googleapis.com/css?family=Inter",
        dbc.themes.PULSE,
        dbc.icons.FONT_AWESOME,
        dbc.icons.BOOTSTRAP,
    ]
    server = flask.Flask(__name__)
    server.secret_key = settings.secret_key

    app = DashProxy(
        __name__,
        title="Crystal Ball",
        server=server,
        use_pages=True,
        external_stylesheets=external_stylesheets,
        requests_pathname_prefix=requests_pathname_prefix,
        transforms=[MultiplexerTransform()],
        suppress_callback_exceptions=True,
    )
    app.scripts.config.serve_locally = False

    home_link = (
        dbc.Row(
            [
                dbc.Col(dmc.Avatar(src=CRYSTALBALL_LOGO)),
                dbc.Col(dbc.NavbarBrand("Crystal Ball", className="ms-2")),
            ],
            align="center",
            className="g-0",
        ),
    )

    navbar = dbc.NavbarSimple(
        style={"height": "4em"},
        id="navbar",
        brand=home_link,
        brand_href=requests_pathname_prefix,
        color="#6941C6",
        dark=True,
        children=[
            dbc.NavItem(
                id={"type": "tabs", "index": page["name"]},
                children=dbc.NavLink(
                    page["name"],
                    id=page["name"],
                    href=page["relative_path"],
                    style={
                        "display": "block",
                        "margin": "auto",
                        "text-align": "center",
                        "color": "white",
                    },
                ),
            )
            for page in dash.page_registry.values()
        ],
    )

    app.layout = html.Div(
        [
            navbar,
            dcc.Store(id="account-id", storage_type="session"),
            dash.page_container,
            dcc.Location(id="url"),
        ]
    )

    @app.callback(
        Output("url", "pathname"),
        Output("account-id", "data"),
        Input("Logout", "n_clicks"),
        State("account-id", "data"),
        State("url", "pathname"),
        prevent_initial_call=True,
    )
    def logout_account(n_clicks, account_id, pathname):
        if r.json().get(account_id, "is-logged-in") and n_clicks:
            print(f"Logged out!! Farewell {account_id}!")
            requests.get(
                f"{BASE_URL}logout",
                headers={
                    "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}"
                },
            )

            r.json().set(account_id, "$.is-logged-in", False)
            return "/dash", None
        return pathname, account_id

    @app.callback(
        Output("navbar", "children"),
        State("account-id", "data"),
        Input({"type": "tabs", "index": ALL}, "children"),
    )
    def create_tabs(account_id, tabs):
        tabs_shown = get_columns_given_roles(
            account_id=account_id,
            is_logged_in=r.json().get(account_id, ".is-logged-in")
            if account_id
            else False,
        )
        output_tabs = []
        for tab in tabs:
            name = tab["props"]["id"]
            tab["props"]["style"]["display"] = "block" if name in tabs_shown else "none"
            print(f"{account_id=}")
            if name == "Profile" and account_id:
                if account_id:
                    src_path = (
                        f"{r.get(f'{account_id}-profile-pic').decode()}"
                        if r.get(f"{account_id}-profile-pic")
                        .decode()
                        .startswith("data")
                        else f"data:image/png;base64,{r.get(f'{account_id}-profile-pic').decode()}"
                    )
                    tab["props"]["children"] = dmc.Avatar(
                        src=src_path, id="navbar-profile-pic"
                    )

            if name in tabs_shown:
                tab["props"]["style"] |= {"margin-left": "20px"}
                output_tabs.append(tab)

        output_tabs = sorted(
            output_tabs, key=lambda tab: tabs_shown.index(tab["props"]["id"])
        )
        return output_tabs

    return app
