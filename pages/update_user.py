import dash
import dash_bootstrap_components as dbc
from dash_extensions.enrich import html, callback, Output, Input, State, dcc
import requests
from pages.utils import BASE_URL
from src.database import get_db_context, r
from src.models import Account

dash.register_page(__name__, path_template="/update/account/<id>")


def get_account(id: str) -> Account:
    with get_db_context() as db:
        account: Account = db.query(Account).get(id)
    return account


def layout(id=None):
    return html.Div(
        className="page-center",
        children=[
            dbc.Form(
                id="update-form",
                children=[
                    dbc.Col(
                        children=[
                            dbc.Row(
                                class_name="auth-row",
                                children=[
                                    html.Div(
                                        className="auth-row auth-header auth-common",
                                        children=["Update Information"],
                                    ),
                                    html.Div(
                                        className="auth-common auth-field-label",
                                        children=[html.Label("Name")],
                                    ),
                                    dbc.Input(
                                        className="auth-field auth-common",
                                        type="text",
                                        id="update-name",
                                        placeholder="Update name...",
                                        value=get_account(id).first_name if id else "",
                                    ),
                                ],
                            )
                        ]
                    ),
                    dbc.Col(
                        children=[
                            dbc.Row(
                                class_name="auth-row",
                                children=[
                                    html.Div(
                                        className="auth-common auth-field-label",
                                        children=[html.Label("Email")],
                                    ),
                                    dbc.Input(
                                        className="auth-field auth-common",
                                        id="update-email",
                                        placeholder="Update email...",
                                        value=get_account(id).email if id else "",
                                    ),
                                ],
                            )
                        ]
                    ),
                    dbc.Col(
                        children=[
                            dbc.Row(
                                class_name="auth-row",
                                children=[
                                    html.Div(
                                        className="auth-common auth-field-label",
                                        children=[html.Label("Password")],
                                    ),
                                    dbc.Input(
                                        className="auth-field auth-common",
                                        type="password",
                                        id="update-password",
                                        placeholder="Update password...",
                                    ),
                                ],
                            )
                        ]
                    ),
                    dbc.Col(
                        children=[
                            dbc.Row(
                                class_name="auth-row",
                                children=[
                                    dbc.Checklist(
                                        options=[
                                            {"label": "Admin Privileges", "value": 1},
                                        ],
                                        value=[1]
                                        if id and get_account(id).is_admin
                                        else [],
                                        id="admin-toggle",
                                        switch=True,
                                        style={
                                            "border-radius": "10px",
                                        },
                                    ),
                                ],
                            )
                        ]
                    ),
                    dbc.Alert(id="updated-output-message", style={"display": "none"}),
                    dbc.Col(
                        children=[
                            dbc.Row(
                                className="auth-row",
                                children=[
                                    dbc.Button(
                                        className="common-btn shadow",
                                        children="Update",
                                        id="update-btn",
                                        style={
                                            "width": "360px",
                                            "height": "44px",
                                            "lineHeight": "10px",
                                        },
                                    ),
                                ],
                            )
                        ]
                    ),
                    dbc.Col(
                        children=[
                            dbc.Row(
                                className="auth-row",
                                children=[
                                    dbc.Button(
                                        className="common-btn",
                                        children="Back",
                                        style={
                                            "width": "360px",
                                            "height": "44px",
                                            "lineHeight": "30px",
                                            "border-radius": "10px",
                                        },
                                        href="/dash/admin",
                                    ),
                                ],
                            )
                        ]
                    ),
                    dcc.Store(id="target-id", data={"target-id": id}),
                ],
            ),
            html.Div(
                children=[
                    dbc.Container(
                        html.H1("User does not have access to the Update page")
                    )
                ],
                style={"display": "none"},
                id="unauthorized-update",
            ),
        ],
    )


@callback(
    Output("updated-output-message", "children"),
    Output("updated-output-message", "style"),
    Output("updated-output-message", "color"),
    Output("url", "pathname"),
    Input("update-btn", "n_clicks"),
    State("update-name", "value"),
    State("update-password", "value"),
    State("update-email", "value"),
    State("admin-toggle", "value"),
    State("account-id", "data"),
    State("target-id", "data"),
)
def update_account(
    n_clicks,
    name: str,
    password: str,
    email: str,
    is_admin: str,
    account_id_info: str,
    target_id: str,
):
    account_id = target_id["target-id"]
    if not n_clicks:
        return "", {"display": "none"}, "danger", f"/dash/update/account/{account_id}"
    updated_info = {
        "first_name": name,
        "email": email,
        "password": password,
        "is_admin": bool(is_admin),
    }
    resp = requests.put(
        f"{BASE_URL}accounts/{account_id}",
        json=updated_info,
        headers={
            "Authorization": f"Bearer {r.json().get(account_id_info, 'bearer-token')}"
        },
    )
    if resp.ok:
        return "Updated successfully!!", {"display": "block"}, "success", "dash/admin"

    detail = resp.json()["detail"]
    if isinstance(detail, str):
        error_message = detail
    else:
        error_message = " and ".join(
            [f"{error['msg'].capitalize()}" for error in detail]
        )

    return (
        error_message,
        {"display": "block"},
        "danger",
        f"/dash/update/account/{account_id}",
    )


@callback(
    Output("unauthorized-update", "style"),
    Output("update-form", "style"),
    [Input("account-id", "data")],
)
def update_auth(account_id):
    try:
        if r.json().get(account_id, "is-logged-in"):
            return {"display": "none"}, {"display": "block"}
    except:
        return {"display": "block"}, {"display": "none"}
