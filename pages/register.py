from typing import Optional
import dash
import dash_bootstrap_components as dbc
import requests
from dash_extensions.enrich import html, Input, Output, callback, State

from pages.utils import BASE_URL
from src.database import get_db_context
from src.models import Account
from src.database import r

dash.register_page(__name__, path="/register")

alert = dbc.Alert(
    id="register-output-message", className="mt-4", style={"display": "none"}
)

name_input = dbc.Row(
    class_name="auth-row",
    children=[
        html.Div(
            className="auth-row auth-header auth-common",
            children=["Sign up"],
        ),
        html.Div(
            className="auth-common auth-field-label",
            children=[html.Label("Name")],
        ),
        dbc.Input(
            className="auth-field auth-common",
            type="text",
            id="name",
            placeholder="Enter your name...",
            required=True,
        ),
    ],
)

email_input = dbc.Row(
    class_name="auth-row",
    children=[
        html.Div(
            className="auth-common auth-field-label",
            children=[html.Label("Email")],
        ),
        dbc.Input(
            className="auth-field auth-common",
            type="email",
            id="email",
            placeholder="Enter your email...",
            required=True,
        ),
    ],
)

password_input = dbc.Row(
    class_name="auth-row",
    children=[
        html.Div(
            className="auth-common auth-field-label",
            children=[html.Label("Password")],
        ),
        dbc.Input(
            className="auth-field auth-common",
            type="password",
            id="password",
            placeholder="Create a password...",
            required=True,
        ),
    ],
)

register_button = dbc.Button(
    className="common-btn shadow",
    children="Create Account",
    id="submit",
    style={
        "width": "360px",
        "height": "44px",
        "lineHeight": "10px",
    },
)


redirect_login_url = html.P(
    className="text-center p-2",
    children=[
        "Already have an account? ",
        html.A("Log in", href="/dash/login"),
    ],
)

layout = html.Div(
    className="page-center",
    children=[
        dbc.Form(
            [
                dbc.Col(name_input),
                dbc.Col(email_input),
                dbc.Col(password_input),
                html.Div(
                    className="text-center",
                    children=[register_button],
                ),
            ]
        ),
        redirect_login_url,
        alert,
    ],
)


@callback(
    Output("register-output-message", "children"),
    Output("register-output-message", "style"),
    Output("register-output-message", "color"),
    Output("url", "pathname"),
    Output("account-id", "data"),
    State("name", "value"),
    State("email", "value"),
    State("password", "value"),
    Input("submit", "n_clicks"),
)
def register_account(name: str, email: str, password: str, n_clicks: Optional[int]):
    if not n_clicks or not name or not email or not password:
        return ("", {"display": "none"}, "danger", "/dash/register", None)
    print(f"{email=}")
    # Send post request to register the user
    data = {"first_name": name, "email": email, "password": password, "is_admin": False}
    resp = requests.post(f"{BASE_URL}signup/", json=data)

    # If the request is successful, save the bearer token in redis
    if resp.ok:
        bearer_token = resp.json().get("access_token")
        with get_db_context() as db:
            account = db.query(Account).filter(Account.email == email).first()

        # Save the account id and bearer token in redis
        r.json().set(
            account.id,
            "$",
            {
                "bearer-token": bearer_token,
                "is-logged-in": True,
            },
        )

        return (
            "Registered successfully!!",
            {"display": "block"},
            "success",
            "/dash",
            account.id,
        )

    print(f"{resp.text=}")
    detail = resp.json()["detail"]
    print(detail)
    error_message = detail if isinstance(detail, str) else detail[0]["msg"]
    return (error_message, {"display": "block"}, "danger", "/dash/register", None)
