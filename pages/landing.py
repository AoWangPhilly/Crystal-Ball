import dash
import dash_bootstrap_components as dbc
from dash_extensions.enrich import html, dcc, Output, Input, State, callback

from src.database import get_db_context
from src.models import Account

dash.register_page(__name__, path="/")

not_logged_in_component = [
    html.Div(
        className="text-center",
        children=[
            dbc.Button(
                className="common-btn shadow",
                children="Sign up",
                href="/dash/register",
                style={"width": "122px", "height": "60px"},
            ),
        ],
    ),
    html.Div(
        className="d-flex justify-content-center",
        children=[
            html.P(className="d-inline p-1", children="Have an account?"),
            dcc.Link(
                className="d-inline p-1",
                children=f"Log in",
                href="/dash/login",
            ),
        ],
    ),
]

layout = html.Div(
    children=[
        html.P(
            className="landing-header",
            children="Make Decisions with Predictions",
        ),
        html.P(
            className="landing-subheader",
            children="Powerful modeling predictions based on key financial indicators to help you make investment decisions",
        ),
        html.Div(id="user-message"),
        html.Div(
            className="d-flex justify-content-center",
            children=[
                html.Img(
                    className="landing-mockup",
                    src=r"assets/images/landing-image.jpg",
                    alt="image",
                    style={"width": "700px", "height": "auto"},
                )
            ],
        ),
    ]
)


@callback(
    Output("user-message", "children"),
    Input("account-id", "data"),
)
def displayMessage(account_id):
    if account_id:
        with get_db_context() as db:
            account = db.query(Account).filter(Account.id == account_id).first()
            if account:
                return html.H3(
                    f"Welcome, {account.first_name}",
                    className="d-flex justify-content-center",
                )
            else:
                return not_logged_in_component
    return not_logged_in_component
