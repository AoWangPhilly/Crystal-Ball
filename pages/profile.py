from typing import Optional
import dash
import dash_bootstrap_components as dbc
from dash_extensions.enrich import html, dcc, callback, Output, Input, State
from pages.utils import resize_profile_picture
import requests

from src.database import get_db_context
from src.database import r
from src.models import Account
from pages.utils import BASE_URL
from src.utils import verify

dash.register_page(__name__, path="/profile")

name_and_email_form = html.Div(
    [
        html.Div(
            [
                dbc.Label("First Name"),
                dbc.Input(
                    id="new-first-name",
                    type="text",
                    placeholder="Enter your name",
                    class_name="mb-3",
                    style={"border-radius": "10px"},
                ),
            ]
        ),
        html.Div(
            [
                dbc.Label("Email"),
                dbc.Input(
                    id="new-email",
                    type="text",
                    placeholder="Enter your email address",
                    style={"border-radius": "10px"},
                    class_name="mb-3",
                ),
            ]
        ),
        dbc.Button(
            "Save Changes",
            color="primary",
            className="mr-1 btn",
            style={"float": "right", "border-radius": "10px"},
            id="name-and-email-submit",
        ),
    ]
)
name_and_email_card = html.Div(
    [
        html.H3("Personal Information"),
        html.P("Update your personal details here"),
        dbc.Card(
            [dbc.CardBody([name_and_email_form])],
            style={"border-radius": "10px"},
        ),
    ],
    className="mb-4",
)

change_password_form = html.Div(
    [
        html.Div(
            [
                dbc.Label("Current Password"),
                dbc.Input(
                    id="current-password",
                    type="password",
                    placeholder="Current Password",
                    class_name="mb-3",
                    style={"border-radius": "10px"},
                ),
            ]
        ),
        html.Div(
            [
                dbc.Label("New Password"),
                dbc.Input(
                    id="new-password",
                    type="password",
                    placeholder="New Password",
                    style={"border-radius": "10px"},
                    class_name="mb-3",
                ),
            ]
        ),
        html.Div(
            [
                dbc.Label("Confirm New Password"),
                dbc.Input(
                    id="confirm-new-password",
                    type="password",
                    placeholder="Confirm New Password",
                    style={"border-radius": "10px"},
                    class_name="mb-3",
                ),
            ]
        ),
        dbc.Button(
            "Save Changes",
            color="primary",
            className="mr-1 btn",
            style={"float": "right", "border-radius": "10px"},
            id="change-password-submit",
        ),
    ]
)
change_password_card = html.Div(
    [
        html.H3(
            "Change Password",
        ),
        dbc.Card(
            [dbc.CardBody([change_password_form])],
            style={"border-radius": "10px"},
        ),
    ],
    className="mb-3 ",
)
image_update = html.Div(
    [
        html.H3(
            "Change Profile Picture",
        ),
        dbc.Card(
            style={"border-radius": "10px"},
            className="mb-3 ",
            children=[
                dbc.CardBody(
                    children=[
                        html.Img(
                            id="profile-pic",
                            style={
                                "display": "block",
                                "margin": "auto",
                                "border-radius": "50%",
                            },
                        ),
                        dcc.Upload(
                            id="upload-image",
                            children=html.Div(
                                ["Drag and Drop or ", html.A("Select Files")]
                            ),
                            className="mt-3",
                            style={
                                "width": "100%",
                                "height": "60px",
                                "lineHeight": "60px",
                                "borderStyle": "dashed",
                                "borderRadius": "5px",
                                "textAlign": "center",
                            },
                        ),
                    ]
                ),
            ],
        ),
    ]
)

email_and_name_alert = dbc.Alert(
    id="email-and-name-alert",
    style={"display": "none"},
    className="mt-4",
    dismissable=True,
)


password_alert = dbc.Alert(
    id="password-alert",
    style={"display": "none"},
    className="mt-4",
    dismissable=True,
)


layout = dbc.Container(
    html.Div(
        children=[
            html.Div(
                id="profile-page",
                children=[
                    image_update,
                    name_and_email_card,
                    email_and_name_alert,
                    change_password_card,
                    password_alert,
                ],
            ),
            html.Div(
                children=[
                    dbc.Container(html.H1("User is not logged in to view Profile page"))
                ],
                style={"display": "none"},
                id="unauthorized-profile-page",
            ),
        ],
        className="mt-3",
        style={"width": "40%", "margin": "auto"},
    )
)


@callback(
    Output("unauthorized-profile-page", "style"),
    Output("profile-page", "style"),
    [Input("account-id", "data")],
)
def update_auth(account_id):
    try:
        if r.json().get(account_id, "is-logged-in"):
            return {"display": "none"}, {"display": "block"}
    except:
        return {"display": "block"}, {"display": "none"}


@callback(
    Output("profile-pic", "src"),
    Input("account-id", "data"),
)
def update_profile_pic(account_id):
    try:
        return r.get(f"{account_id}-profile-pic")
    except:
        return ""


@callback(
    Output("profile-pic", "src"),
    Output("navbar-profile-pic", "src"),
    State("account-id", "data"),
    Input("upload-image", "contents"),
)
def upload_profile_pic(account_id, image_content):
    if image_content is not None:
        image_content = resize_profile_picture(
            base64_img=image_content, width=200, height=200
        )
        r.set(f"{account_id}-profile-pic", image_content)
        return image_content, image_content
    original_image = r.get(f"{account_id}-profile-pic").decode()
    return original_image, original_image


@callback(
    Output("output", "children"),
    Input("name-and-email-submit", "n_clicks"),
    prevent_initial_callback=False,
)
def update_output(n_clicks):
    if n_clicks is None:
        return "Button has not been clicked yet"
    else:
        return f"Button has been clicked {n_clicks} times"


@callback(
    [
        Output("new-first-name", "value"),
        Output("new-email", "value"),
        Output("email-and-name-alert", "children"),
        Output("email-and-name-alert", "style"),
        Output("email-and-name-alert", "color"),
        Output("email-and-name-alert", "is_open"),
    ],
    [
        State("account-id", "data"),
        State("new-first-name", "value"),
        State("new-email", "value"),
    ],
    [Input("name-and-email-submit", "n_clicks")],
    prevent_initial_callback=True,
)
def update_email_and_name_form(
    account_id: str, new_name: str, new_email: str, n_clicks: Optional[int]
):
    with get_db_context() as db:
        account: Account = db.query(Account).get(account_id)
    if n_clicks is None:
        return (
            account.first_name,
            account.email,
            "",
            {"display": "none"},
            "danger",
            False,
        )
    else:
        name_response = requests.put(
            f"{BASE_URL}accounts/api/profile/name",
            json={"first_name": new_name},
            headers={
                "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}",
                "Content-Type": "application/json",
                "accept": "*/*",
            },
        )

        if not name_response.ok:
            return (
                account.first_name,
                account.email,
                name_response.json()["detail"],
                {"display": "block"},
                "danger",
                True,
            )

        email_response = requests.put(
            f"{BASE_URL}accounts/api/profile/email",
            json={"email": new_email},
            headers={
                "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}",
                "Content-Type": "application/json",
                "accept": "*/*",
            },
        )

        if not email_response.ok:
            return (
                new_name,
                account.email,
                email_response.json()["detail"],
                {"display": "block"},
                "danger",
                True,
            )

        return (
            new_name,
            new_email,
            "Name and email are successfully changed",
            {"display": "block"},
            "success",
            True,
        )


@callback(
    Output("password-alert", "children"),
    Output("password-alert", "style"),
    Output("password-alert", "color"),
    Output("password-alert", "is_open"),
    State("account-id", "data"),
    State("current-password", "value"),
    State("new-password", "value"),
    State("confirm-new-password", "value"),
    Input("change-password-submit", "n_clicks"),
)
def update_password_form(
    account_id: str,
    current_password: str,
    new_password: str,
    confirm_new_password: str,
    n_clicks: Optional[int],
):
    if n_clicks is None:
        return (
            "",
            {"display": "none"},
            "danger",
            False,
        )

    resp = requests.put(
        f"{BASE_URL}accounts/api/profile/password",
        json={
            "old_password": current_password,
            "new_password": new_password,
            "confirm_new_password": confirm_new_password,
        },
        headers={
            "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}",
            "Content-Type": "application/json",
            "accept": "*/*",
        },
    )
    output_json = resp.json()
    if not resp.ok:
        if "detail" in output_json and isinstance(output_json["detail"], list):
            return (
                output_json["detail"][0]["msg"],
                {"display": "block"},
                "danger",
                True,
            )
        elif "details" in resp.json():
            return output_json["details"], {"display": "block"}, "danger", True
        else:
            return output_json["detail"], {"display": "block"}, "danger", True
    return "Password is successfully changed", {"display": "block"}, "success", True
