from typing import List
from datetime import datetime
import json
import math

import dash
import dash_bootstrap_components as dbc
import requests
from dash_extensions.enrich import html, callback, Output, Input, State, dcc, ALL
import dash_mantine_components as dmc
from dash_iconify import DashIconify

from pages.utils import BASE_URL
from src.models import Wishlist
from src.database import r
from src.stock_data.logo import get_logo

dash.register_page(__name__, path="/wishlist")


def get_date_added(time):
    timestamp = time[0:10]
    timeStr = datetime.strptime(timestamp, "%Y-%m-%d")
    return timeStr.strftime("%B %d, %Y")


def create_wishlist_card(wishlist) -> html.A:
    """
    Creates a wishlist card
    :param wishlist (Wishlist): the wishlist to create a card for
    :return (html.A): the card
    """

    return dbc.Card(
        className="zoom",
        style={
            "margin-top": "10px",
            "borderRadius": "10px",
        },
        children=[
            dbc.CardBody(
                [
                    dbc.Row(
                        [
                            dbc.Col(
                                html.A(
                                    children=[
                                        html.H4(wishlist["name"], id="card-title"),
                                        html.P(
                                            f"Created {get_date_added(wishlist['timestamp'])}",
                                            id="card-description",
                                        ),
                                    ],
                                    href=f"/dash/wishlist/{wishlist['id']}",
                                    style={"text-decoration": "none", "color": "black"},
                                ),
                                width=3,
                            ),
                            dbc.Col(
                                [
                                    dmc.AvatarGroup(
                                        children=[
                                            dmc.Avatar(
                                                src=get_logo(ticker_symbol),
                                                radius="xl",
                                                size="lg",
                                            )
                                            for ticker_symbol in wishlist[
                                                "stock_symbols"
                                            ]
                                        ],
                                    ),
                                ]
                            ),
                            dbc.Col(
                                html.Div(
                                    [
                                        dbc.Button(
                                            DashIconify(
                                                icon="iwwa:delete",
                                                width=30,
                                                height=30,
                                                className="zoom",
                                            ),
                                            id={
                                                "type": "dynamic-delete",
                                                "index": wishlist["id"],
                                            },
                                            className="mt-3 mr-2 ml-2",
                                            style={
                                                "borderRadius": "10px",
                                                "float": "right",
                                            },
                                        ),
                                    ],
                                    style={
                                        "float": "right",
                                    },
                                )
                            ),
                        ]
                    )
                ]
            )
        ],
    )


def format_wishlist_grid(wishlists: List[Wishlist]):
    output = [create_wishlist_card(wishlist=wishlist) for wishlist in wishlists]

    return output


create_wishlist_form = html.Div(
    children=[
        dbc.Row(
            [
                dbc.Col(
                    dbc.Input(
                        className="auth-field auth-common",
                        type="text",
                        id="wishlist-name",
                        placeholder="Enter wishlist name...",
                        required=True,
                    ),
                    width=4,
                ),
                dbc.Col(
                    dbc.Button(
                        "Create",
                        id="wishlist-create-button",
                        style={"border-radius": "10px"},
                        className="mt-2",
                    )
                ),
            ]
        ),
        dbc.Alert(
            id="wishlist-create-message",
            style={"display": "none", "borderRadius": "10px", "width": "75%"},
            dismissable=True,
            className="mt-2",
        ),
    ]
)

pagination = dbc.Pagination(
    id="pagination",
    max_value=5,
    class_name="d-flex justify-content-center mt-3",
    fully_expanded=False,
    style={"display": "none"},
)

empty_response_header = html.H3(
    "Hmm...Maybe you should create a wishlist?",
    style={"color": "lightgrey", "text-align": "center", "display": "none"},
    className="mt-3",
    id="empty-response-header",
)

empty_response_image = html.Img(
    src="assets/images/missing-wishlist.jpeg",
    style={"width": "50%", "height": "50%", "borderRadius": "10px", "display": "none"},
    className="mt-3 center-image",
    id="empty-response-image",
)

layout = html.Div(
    [
        dbc.Container(
            id="wishlist-page",
            children=[
                dcc.Loading(
                    [
                        html.H1(children="All Wishlists"),
                        create_wishlist_form,
                        html.Div(
                            id="wishlist-grid",
                            className="justify-content-center",
                            style={"display": "none"},
                        ),
                        empty_response_image,
                        pagination,
                        empty_response_header,
                    ],
                    type="dot",
                )
            ],
        ),
        html.Div(
            children=[
                dbc.Container(html.H1("User is not logged in to view Wishlist page"))
            ],
            style={"display": "none"},
            id="unauthorized-wishlist",
        ),
    ]
)


@callback(
    Output("unauthorized-wishlist", "style"),
    Output("wishlist-page", "style"),
    [Input("account-id", "data")],
)
def update_auth(account_id):
    try:
        if r.json().get(account_id, "is-logged-in"):
            return {"display": "none"}, {"display": "block"}
    except:
        return {"display": "block"}, {"display": "none"}


@callback(
    Output("wishlist-grid", "children"),
    Output("pagination", "style"),
    Output("empty-response-header", "style"),
    Output("empty-response-image", "style"),
    Output("wishlist-grid", "style"),
    Output("wishlist-create-message", "style"),
    Output("pagination", "max_value"),
    State("account-id", "data"),
    State("wishlist-create-message", "style"),
    Input({"type": "dynamic-delete", "index": ALL}, "n_clicks"),
    Input("pagination", "active_page"),
)
def delete_wishlist(account_id, alert, delete_clicks, page):
    wishlist_id = dash.callback_context.triggered[0]["prop_id"].rsplit(".", 1)[0]
    get_wishlists_req = requests.get(f"{BASE_URL}api/wishlist/{account_id}")
    wishlists = get_wishlists_req.json()
    articles_per_page = 5
    if not page:
        page = 1

    if any(delete_clicks):
        if "index" in wishlist_id:
            delete_chart = json.loads(wishlist_id)["index"]
            requests.delete(
                f"{BASE_URL}api/wishlist/remove/{delete_chart}",
                headers={
                    "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}"
                },
            )
            wishlists = list(filter(lambda x: x["id"] != delete_chart, wishlists))
            wishlists.sort(key=lambda x: x["timestamp"], reverse=True)
            output = format_wishlist_grid(wishlists=wishlists)
            return (
                output[(page - 1) * articles_per_page : page * articles_per_page],
                {"display": "block"} if output else {"display": "none"},
                {"color": "lightgrey", "text-align": "center", "display": "none"}
                if output
                else {"color": "lightgrey", "text-align": "center", "display": "block"},
                {
                    "width": "50%",
                    "height": "50%",
                    "borderRadius": "10px",
                    "display": "none",
                }
                if output
                else {
                    "width": "50%",
                    "height": "50%",
                    "borderRadius": "10px",
                    "display": "block",
                },
                {"display": "block"} if output else {"display": "none"},
                {"display": "none"},
                math.ceil(len(output) / articles_per_page),
            )
    wishlists.sort(key=lambda x: x["timestamp"], reverse=True)
    output = format_wishlist_grid(wishlists=wishlists)
    return (
        output[(page - 1) * articles_per_page : page * articles_per_page],
        {"display": "block"} if output else {"display": "none"},
        {"color": "lightgrey", "text-align": "center", "display": "none"}
        if output
        else {"color": "lightgrey", "text-align": "center", "display": "block"},
        {
            "width": "50%",
            "height": "50%",
            "borderRadius": "10px",
            "display": "none",
        }
        if output
        else {
            "width": "50%",
            "height": "50%",
            "borderRadius": "10px",
            "display": "block",
        },
        {"display": "block"} if output else {"display": "none"},
        alert.get("style") if alert else {"display": "none"},
        math.ceil(len(output) / articles_per_page),
    )


@callback(
    Output("wishlist-grid", "children"),
    Output("wishlist-create-message", "children"),
    Output("wishlist-create-message", "style"),
    Output("wishlist-create-message", "color"),
    Output("wishlist-create-message", "is_open"),
    Output("pagination", "style"),
    Output("empty-response-header", "style"),
    Output("empty-response-image", "style"),
    Output("wishlist-grid", "style"),
    Output("pagination", "max_value"),
    State("wishlist-name", "value"),
    State("account-id", "data"),
    Input("wishlist-create-button", "n_clicks"),
    Input("pagination", "active_page"),
)
def create_wishlist(wishlist_name, account_id, create_clicks, page):
    print(f"{page=}")
    get_wishlists_req = requests.get(f"{BASE_URL}api/wishlist/{account_id}")
    wishlists = get_wishlists_req.json()
    wishlists.sort(key=lambda x: x["timestamp"], reverse=True)
    output = format_wishlist_grid(wishlists=wishlists)
    articles_per_page = 5
    if not page:
        page = 1

    if create_clicks:
        create_wishlist_req = requests.post(
            f"{BASE_URL}api/wishlist/",
            json={"name": wishlist_name},
            headers={
                "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}"
            },
        )
        if create_wishlist_req.ok:
            wishlists.append(create_wishlist_req.json())
            wishlists.sort(key=lambda x: x["timestamp"], reverse=True)
            output = format_wishlist_grid(wishlists=wishlists)
            print(f"{len(output)}")
            print(f"{math.ceil(len(output) / articles_per_page)=}")
            print(f"{(page - 1) * articles_per_page=}")
            print(f"{page * articles_per_page=}")
            return (
                output[(page - 1) * articles_per_page : page * articles_per_page],
                "The wishlist has been successfully added!",
                {"display": "block"},
                "success",
                True,
                {"display": "block"} if output else {"display": "none"},
                {"color": "lightgrey", "text-align": "center", "display": "none"},
                {
                    "width": "50%",
                    "height": "50%",
                    "borderRadius": "10px",
                    "display": "none",
                },
                {"display": "block"},
                math.ceil(len(output) / articles_per_page),
            )
        else:
            error = create_wishlist_req.json()["detail"][0]["msg"]
            print(f"{len(output)}")
            print(f"{math.ceil(len(output) / articles_per_page)=}")
            print(f"{(page - 1) * articles_per_page=}")
            print(f"{page * articles_per_page=}")
            return (
                output[(page - 1) * articles_per_page : page * articles_per_page],
                f"{error.capitalize()}. Please try again.",
                {"display": "block"},
                "danger",
                True,
                {"display": "block"} if output else {"display": "none"},
                {"color": "lightgrey", "text-align": "center", "display": "none"}
                if output
                else {"color": "lightgrey", "text-align": "center", "display": "block"},
                {
                    "width": "50%",
                    "height": "50%",
                    "borderRadius": "10px",
                    "display": "none",
                }
                if output
                else {
                    "width": "50%",
                    "height": "50%",
                    "borderRadius": "10px",
                    "display": "block",
                },
                {"display": "block"} if output else {"display": "none"},
                math.ceil(len(output) / articles_per_page),
            )
    print(f"{len(output)}")
    print(f"{math.ceil(len(output) / articles_per_page)=}")
    print(f"{(page - 1) * articles_per_page=}")
    print(f"{page * articles_per_page=}")
    return (
        output[(page - 1) * articles_per_page : page * articles_per_page],
        "",
        {"display": "none"},
        "danger",
        False,
        {"display": "block"} if output else {"display": "none"},
        {"color": "lightgrey", "text-align": "center", "display": "none"}
        if output
        else {"color": "lightgrey", "text-align": "center", "display": "block"},
        {
            "width": "50%",
            "height": "50%",
            "borderRadius": "10px",
            "display": "none",
        }
        if output
        else {
            "width": "50%",
            "height": "50%",
            "borderRadius": "10px",
            "display": "block",
        },
        {"display": "block"} if output else {"display": "none"},
        math.ceil(len(output) / articles_per_page),
    )
