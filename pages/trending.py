import warnings

import dash
from dash_extensions.enrich import html, dash_table
import dash_bootstrap_components as dbc
import pandas as pd

from src.stock_data.tickers import find_ticker_name
from src.stock_data.logo import get_logo

warnings.simplefilter(action="ignore")

dash.register_page(__name__, path="/trending")

df = pd.read_html("https://finance.yahoo.com/most-active")[0]

df = df[
    [
        "Symbol",
        "Name",
        "Price (Intraday)",
        "Change",
        "% Change",
        "Volume",
        "Market Cap",
        "PE Ratio (TTM)",
    ]
]

df.rename(
    columns={"PE Ratio (TTM)": "PE Ratio", "Price (Intraday)": "Price"}, inplace=True
)


def create_name_link(row):
    href = f'stock?symbol={find_ticker_name(row["Symbol"])}'
    logos = get_logo(row["Symbol"])
    img = f"<img src='{logos}' width='50' height='50' style='margin-right: 10px; border-radius: 50%'>"
    return f'<div class="m-3"><div style="font-size: 20px" class="table-label"><a href="{href}">{img}</a><a href="{href}">{row["Symbol"]}</a></div><div class="table-sublabel">{row["Name"]}</div></div>'


# Generate Name column with Name + Symbol
name_symbol_list = [create_name_link(row) for _, row in df.iterrows()]

df.drop(columns="Symbol", inplace=True)
df.fillna("N/A", inplace=True)
formatted_columns = []
for column in df.columns:
    column_dict = {}
    column_dict["id"] = column
    column_dict["name"] = column
    if column in ("Name", "Change", "% Change"):
        column_dict["presentation"] = "markdown"
    formatted_columns.append(column_dict)

df["Name"] = name_symbol_list

# Generate colored columns
colored_returns = []
colored_returns_percent = []
for index, row in df.iterrows():
    row["% Change"] = round(float(row["% Change"].rstrip("%")), 2)
    row["Change"] = "{:.2f}".format(round(row["Change"], 2))
    if row["% Change"] >= 1:
        badge_color = "bg-success"
    elif row["% Change"] <= -1:
        badge_color = "bg-danger"
    else:
        badge_color = "bg-warning"

    colored_returns.append(
        f'<p style="font-size: 25px" class="m-4 d-flex justify-content-center"><span class="badge rounded-pill {badge_color}">'
        + f"{row['Change']}"
        + "</span></p>"
    )

    colored_returns_percent.append(
        f'<p style="font-size: 25px" class="m-4 d-flex justify-content-center"><span class="badge rounded-pill {badge_color}">'
        + f"{'{:.2f}'.format(row['% Change'])}%"
        + "</span></p>"
    )

df["Change"] = colored_returns
df["% Change"] = colored_returns_percent

layout = dbc.Container(
    children=[
        html.H1("Trending Stocks"),
        html.Div(
            className="d-flex justify-content-center",
            children=[
                dash_table.DataTable(
                    data=df.to_dict("records"),
                    filter_action="native",
                    columns=formatted_columns,
                    markdown_options={"html": True},
                    style_as_list_view=True,
                    page_size=10,
                    page_action="native",
                    style_header={
                        "backgroundColor": "#F9FAFB",
                        "border": "1px solid #EAECF0",
                        "color": "#667085",
                        "font-size": "16px",
                        "padding": "10px",
                    },
                    style_cell={
                        "font-family": "Inter",
                        "font-size": "20px",
                        "textAlign": "center",
                        "height": "50px",
                        "padding": "10px",
                    },
                    style_table={
                        "width": "1216px",
                        "border": "1px solid #EAECF0",
                        "box-shadow": "0px 1px 3px rgba(16, 24, 40, 0.1), 0px 1px 2px rgba(16, 24, 40, 0.06)",
                        "border-radius": "8px",
                    },
                    style_cell_conditional=[
                        {"if": {"column_id": "Company"}, "textAlign": "left"},
                        {"if": {"column_id": "Volume"}, "textAlign": "right"},
                        {"if": {"column_id": "Market Cap"}, "textAlign": "right"},
                        {"if": {"column_id": "Price"}, "textAlign": "right"},
                    ],
                ),
            ],
        ),
    ]
)
