from typing import Optional
import dash
import dash_bootstrap_components as dbc
import requests
from dash_extensions.enrich import Output, Input, State, callback, html

from pages.utils import BASE_URL
from src.database import get_db_context, r
from src.models import Account

dash.register_page(__name__, path="/login")

layout = html.Div(
    className="page-center",
    children=[
        dbc.Form(
            [
                dbc.Col(
                    children=[
                        dbc.Row(
                            class_name="auth-row",
                            children=[
                                html.Div(
                                    className="auth-row auth-header auth-common",
                                    children=["Log in"],
                                ),
                                html.Div(
                                    className="auth-common auth-field-label",
                                    children=[html.Label("Email")],
                                ),
                                dbc.Input(
                                    className="auth-field auth-common",
                                    type="email",
                                    id="email-login",
                                    placeholder="Email",
                                    required=True,
                                ),
                            ],
                        )
                    ]
                ),
                dbc.Col(
                    children=[
                        dbc.Row(
                            class_name="auth-row",
                            children=[
                                html.Div(
                                    className="auth-common auth-field-label",
                                    children=[html.Label("Password")],
                                ),
                                dbc.Input(
                                    className="auth-field auth-common",
                                    type="password",
                                    id="password-login",
                                    placeholder="Password",
                                    required=True,
                                ),
                            ],
                        )
                    ]
                ),
                html.Div(
                    className="text-center",
                    children=[
                        dbc.Button(
                            className="common-btn shadow",
                            children="Sign in",
                            id="submit-login",
                            style={
                                "width": "360px",
                                "height": "44px",
                                "lineHeight": "10px",
                            },
                        ),
                    ],
                ),
            ]
        ),
        html.P(
            className="text-center p-2",
            children=[
                "Don't have an account? ",
                html.A("Register", href="/dash/register"),
            ],
        ),
        dbc.Alert(id="output-login", style={"display": "none"}, className="mt-4"),
    ],
)


@callback(
    Output("output-login", "children"),
    Output("output-login", "style"),
    Output("output-login", "color"),
    Output("url", "pathname"),
    Output("account-id", "data"),
    State("email-login", "value"),
    State("password-login", "value"),
    Input("submit-login", "n_clicks"),
    prevent_initial_call=True,
)
def account_login(email: str, password: str, n_clicks: Optional[int]):
    if not n_clicks or not email or not password:
        return ("", {"display": "none"}, "danger", "/dash/login", None)
    data = {"username": email, "password": password}
    resp = requests.post(f"{BASE_URL}login", data=data)
    bearer_token = resp.json().get("access_token")

    with get_db_context() as db:
        account = db.query(Account).filter(Account.email == email).first()

    if resp.ok:
        if not r.exists(account.id):
            r.json().set(
                account.id, "$", {"bearer-token": bearer_token, "is-logged-in": True}
            )
        r.json().set(account.id, "is-logged-in", True)
        r.json().set(account.id, "bearer-token", bearer_token)
        print(f"Logged in successfully!! Welcome {account.id}!")
        return (
            "Logged in successfully!!",
            {"display": "block"},
            "success",
            "/dash",
            account.id,
        )

    return (
        resp.json().get("detail"),
        {"display": "block"},
        "danger",
        "/dash/login",
        None,
    )
