import datetime
import math

import cufflinks as cf
import dash
import dash_bootstrap_components as dbc
import plotly.express as px
import requests
from dash.exceptions import PreventUpdate
from dash_extensions.enrich import Input, Output, State, callback, dcc, html
from numerize import numerize
from dash_iconify import DashIconify
import dash_mantine_components as dmc
from dash import no_update

from pages.utils import BASE_URL, search_stocks
from src.classes.news_article.news_article import NewsAPI
from src.config import settings
from src.database import r
from src.stock_data.aggregator import StockDataAggregator
from src.stock_data.stock_info import StockInfo
from src.stock_data.tickers import get_ticker_name_and_symbol
from src.stock_data.logo import get_logo

dash.register_page(__name__, path="/stock")

cf.set_config_file(sharing="public", theme="white", offline=True)

ticker_options_df = get_ticker_name_and_symbol()

news_api = NewsAPI(settings.news_api_key)


def layout(symbol="AAPL: Apple Inc. Common Stock"):
    search_bar = html.Div(
        children=[
            dbc.Label("Ticker symbol: "),
            dcc.Dropdown(
                placeholder=symbol,
                id="ticker_dropdown",
                value=symbol,
            ),
        ],
    )

    start_date_input = html.Div(
        [
            html.Div(
                dbc.Label(
                    children=[
                        "Start date :",
                    ]
                )
            ),
            dcc.DatePickerSingle(
                id="start-date",
                month_format="MMM Do, YYYY",
                placeholder="MMM Do, YY",
                date=datetime.datetime.now().date() - datetime.timedelta(weeks=52),
            ),
        ]
    )

    end_date_input = html.Div(
        [
            html.Div(
                dbc.Label(
                    children=[
                        "End date :",
                    ]
                )
            ),
            dcc.DatePickerSingle(
                id="end-date",
                month_format="MMM Do, YYYY",
                placeholder="MMM Do, YY",
                date=datetime.datetime.now().date(),
            ),
        ]
    )

    run_button = dbc.Button(
        "Run",
        id="submit-button",
        color="primary",
        style={
            "display": "flex",
            "justify-content": "center",
            "align-items": "center",
            "height": "50%",
            "width": "50%",
        },
        className="common-btn shadow mt-4",
    )

    add_stock_button = dbc.Button(
        DashIconify(icon="zondicons:add-outline", width=30),
        id="add-to-wishlist",
        color="primary",
        style={"display": "none", "borderRadius": "10px"},
    )

    return html.Div(
        children=[
            dbc.Container(
                children=[
                    dbc.Col(
                        dbc.Row(
                            [
                                dbc.Col(search_bar, width=6),
                                dbc.Col(start_date_input),
                                dbc.Col(end_date_input),
                                dbc.Col(run_button),
                            ]
                        ),
                        className="mt-3",
                    ),
                    html.Div(
                        id="stock_info_content",
                        className="invisible",
                        children=[
                            html.Div(
                                children=[
                                    dbc.Row(
                                        [
                                            dbc.Col(
                                                html.H2(
                                                    id="company_name",
                                                ),
                                                width=3,
                                            ),
                                            dbc.Col(
                                                dmc.Avatar(
                                                    id="company-logo",
                                                ),
                                                width=1,
                                            ),
                                            dbc.Col(add_stock_button, width=3),
                                        ]
                                    ),
                                    dbc.Modal(
                                        [
                                            dbc.ModalHeader(
                                                dbc.ModalTitle(
                                                    "Select a wishlist to add this stock to"
                                                )
                                            ),
                                            dbc.ModalBody(
                                                dbc.Select(
                                                    id="select",
                                                    placeholder="Select a wishlist",
                                                )
                                            ),
                                            dbc.ModalFooter(
                                                dbc.Button(
                                                    "Add",
                                                    id="add-stock",
                                                    className="ms-auto",
                                                    n_clicks=0,
                                                )
                                            ),
                                        ],
                                        id="modal",
                                        is_open=False,
                                    ),
                                ],
                            ),
                            html.Div(
                                id="wishlist-status",
                                style={"display": "none"},
                            ),
                            html.Span(
                                className="pt-2 d-flex",
                                children=[
                                    html.H1(id="company_live_price"),
                                    dbc.Alert(
                                        id="company_return_percent_alert",
                                        className="ms-3 p-1",
                                        children=[
                                            html.H5(
                                                id="company_return_percent",
                                                className="m-1",
                                            )
                                        ],
                                        style={
                                            "borderRadius": "10px",
                                        },
                                    ),
                                    html.H5(id="company_return", className="ms-2 mt-2"),
                                ],
                            ),
                            html.H6(id="market_close_time", className="pb-3"),
                            dbc.Tabs(
                                id="period_tabs",
                                active_tab="1Y",
                                children=[
                                    dbc.Tab(
                                        tab_id="1M",
                                        label="1M",
                                        active_tab_class_name="active-tab",
                                        label_class_name="inactive-tab",
                                    ),
                                    dbc.Tab(
                                        tab_id="6M",
                                        label="6M",
                                        active_tab_class_name="active-tab",
                                        label_class_name="inactive-tab",
                                    ),
                                    dbc.Tab(
                                        tab_id="1Y",
                                        label="1Y",
                                        active_tab_class_name="active-tab",
                                        label_class_name="inactive-tab",
                                    ),
                                    dbc.Tab(
                                        tab_id="5Y",
                                        label="5Y",
                                        active_tab_class_name="active-tab",
                                        label_class_name="inactive-tab",
                                    ),
                                ],
                            ),
                            html.Div(
                                className="d-flex flex-row",
                                children=[
                                    html.Div(
                                        className="w-75",
                                        children=[
                                            dcc.Loading(
                                                dcc.Graph(id="graph", className="pe-2"),
                                                type="dot",
                                            ),
                                            dcc.Tooltip(id="graph-tooltip-5"),
                                        ],
                                    ),
                                    html.Div(
                                        id="key_stats_div",
                                        className="w-25 stock_key_stat_box",
                                        children=[
                                            html.H3("Key Statistics"),
                                            html.Div(className="stock_key_stat_row"),
                                        ],
                                    ),
                                ],
                            ),
                            html.Div(id="recommendation_accordion_div"),
                            html.Div(
                                id="company_news",
                                className="news-card-holder d-flex justify-content-center",
                            ),
                            dbc.Pagination(
                                id="pagination",
                                max_value=5,
                                class_name="d-flex justify-content-center mt-3",
                                fully_expanded=False,
                            ),
                        ],
                    ),
                ]
            ),
        ],
        style={"font-family": "Inter"},
    )


def generate_recommendation_accordion(symbol: str):
    stock_info_obj = StockInfo(symbol)
    recommendation_dfs = stock_info_obj.get_recommendations()
    earnings_estimate_df = recommendation_dfs["Earnings Estimate"]
    revenue_estimate_df = recommendation_dfs["Revenue Estimate"]
    earnings_history_df = recommendation_dfs["Earnings History"]

    # Dropping Number of Analysts and Year Ago EPS
    earnings_estimate_df = earnings_estimate_df.drop([0, 4])

    earnings_estimate_df.columns = [
        "Earnings Estimate",
        "2023 Q1",
        "2023 Q2",
        "2023 EOY",
        "2024 EOY",
    ]

    earnings_estimate_df = earnings_estimate_df.reindex([2, 1, 3])

    earnings_estimate_df_melted = earnings_estimate_df.melt(
        id_vars=["Earnings Estimate"], var_name="Quarter", value_name="EPS"
    )

    # Dropping Number of Analysts and Year Ago EPS
    earnings_history_df = earnings_history_df.drop([2, 3])

    earnings_history_df.columns = [
        "Earnings History",
        "2022 Q1",
        "2022 Q2",
        "2022 Q3",
        "2022 Q4",
    ]

    earnings_history_df_melted = earnings_history_df.melt(
        id_vars=["Earnings History"], var_name="Quarter", value_name="EPS"
    )

    earnings_estimate_fig = px.histogram(
        earnings_estimate_df_melted,
        x="Quarter",
        y="EPS",
        color="Earnings Estimate",
        barmode="group",
    )

    earnings_estimate_fig.update_traces(hoverinfo="none", hovertemplate=None)

    earnings_estimate_fig.update_layout(
        template="plotly_white",
        title="Earnings Estimate",
        showlegend=True,
        xaxis_title="Quarter",
        yaxis_title="Earnings Per Share (EPS)",
    )

    earnings_history_fig = px.histogram(
        earnings_history_df_melted,
        x="Quarter",
        y="EPS",
        color="Earnings History",
        barmode="group",
    )

    earnings_history_fig.update_traces(hoverinfo="none", hovertemplate=None)


    earnings_history_fig.update_layout(
        template="plotly_white",
        title="Earnings History",
        showlegend=True,
        xaxis_title="Quarter",
        yaxis_title="Earnings Per Share (EPS)",
    )

    earnings_estimate_accordian = dbc.AccordionItem(
        title="Earnings Estimates",
        children=[dcc.Graph(id="earnings_estimate", figure=earnings_estimate_fig), dcc.Tooltip(id="graph-tooltip-estimate")],
    )

    earnings_history_accordian = dbc.AccordionItem(
        title="Earnings History",
        children=[dcc.Graph(id="earnings_history", figure=earnings_history_fig), dcc.Tooltip(id="graph-tooltip-history")],
    )

    revenue_estimate_df.columns = ["", "2023 Q1", "2023 Q2", "2023 EOY", "2024 EOY"]

    revenue_estimate_accordian = dbc.AccordionItem(
        title="Revenue Estimates",
        children=[
            dash.dash_table.DataTable(
                data=revenue_estimate_df.to_dict("records"),
                columns=[{"name": i, "id": i} for i in revenue_estimate_df.columns],
                style_header={
                    "backgroundColor": "#F9FAFB",
                    "border": "1px solid #EAECF0",
                    "color": "#667085",
                    "font-size": "16px",
                    "padding": "10px",
                },
                style_cell={
                    "font-family": "Inter",
                    "font-size": "16px",
                    "textAlign": "center",
                    "height": "40px",
                    "padding": "10px",
                },
                style_table={
                    "border": "1px solid #EAECF0",
                    "box-shadow": "0px 1px 3px rgba(16, 24, 40, 0.1), 0px 1px 2px rgba(16, 24, 40, 0.06)",
                    "border-radius": "8px",
                    "overflowX": "auto",
                },
            )
        ],
    )

    return dbc.Accordion(
        start_collapsed=True,
        children=[
            earnings_estimate_accordian,
            earnings_history_accordian,
            revenue_estimate_accordian,
        ],
    )


def generate_news_cards(articles):
    if articles.empty:
        return (
            html.Img(
                src="assets/images/no_articles_found.jpeg",
                style={
                    "width": "auto",
                    "height": "700px",
                },
            ),
        )
    card_list = []
    for _, row in articles.iterrows():
        card = dbc.Card(
            class_name="news-card w-100",
            children=[
                dbc.CardBody(
                    children=[
                        html.A(
                            href=row["url"],
                            children=[
                                html.H4(
                                    row["description"],
                                    className="card-title news-card-title",
                                ),
                            ],
                        ),
                        html.H6(datetime.datetime.strptime(row["publishedAt"][0:10], "%Y-%m-%d").strftime("%B %d, %Y"), className="card-subtitle pb-1"),
                        html.H6(row["source"], className="card-subtitle"),
                        html.P(
                            children=[row["content"][:200]],
                            className="news-card-text",
                        ),
                        dbc.CardImg(
                            class_name="news-card-image", src=row["urlToImage"]
                        ),
                        html.P(
                            children=[row["sentiment_category"]],
                            style={
                                "color": "green"
                                if row["sentiment_category"]
                                == "Article Sentiment: Overwhelmingly Positive"
                                or row["sentiment_category"]
                                == "Article Sentiment: Positive"
                                else (
                                    "orange"
                                    if row["sentiment_category"]
                                    == "Article Sentiment: Neutral"
                                    else "red"
                                )
                            },
                        ),
                    ]
                ),
            ],
        )
        card_list.append(card)

    return card_list


def generate_key_stats(stock_info_dict: dict):
    children = [html.H3("Key Statistics")]
    stats = {
        "Previous Close": "regularMarketPreviousClose",
        "Day Range": "regularMarketDayRange",
        "Year Range": "fiftyTwoWeekRange",
        "Market Cap": "marketCap",
        "Avg Volume": "averageDailyVolume3Month",
        "P/E Ratio": "forwardPE",
        "Dividend Yield": "trailingAnnualDividendYield",
    }
    for key, value in stats.items():
        prop_value = stock_info_dict[value]
        if key == "Previous Close":
            prop_value = "${:.2f}".format(prop_value)
        elif key in ["Day Range", "Year Range"]:
            pass
        elif key in ["Market Cap", "Avg Volume"]:
            pass
        else:
            pass
        children.append(
            html.Div(
                className="stock_key_stat_row",
                children=[
                    html.Span(className="fw-light", children=[key]),
                    html.Span(className="fw-bold", children=[prop_value]),
                ],
            )
        )
    return children


@callback(
    Output("company_return_percent_alert", "color"),
    Output("company_return", "style"),
    Input("company_return_percent", "children"),
)
def update_return_alert_color(return_percent: str):
    if return_percent[0] != "-":
        return "success", {"color": "#13b955"}
    else:
        return "danger", {"color": "#fc3939"}


@callback(
    Output("ticker_dropdown", "options"), Input("ticker_dropdown", "search_value")
)
def update_options(search_value):
    if not search_value:
        raise PreventUpdate
    return search_stocks(
        search_value=search_value,
        ticker_options_df=ticker_options_df,
    )


@callback(
    Output("company_news", "children"),
    Output("pagination", "max_value"),
    State("ticker_dropdown", "value"),
    Input("pagination", "active_page"),
    Input("submit-button", "n_clicks"),
)
def display_news(ticker_dropdown, page, n_clicks):
    if ticker_dropdown:
        symbol = ticker_dropdown.split(":")[0]

        # Generate news articles based on company
        headlines = news_api.get_headlines(keywords=symbol)
        articles = news_api.preprocess_json(headlines).head(10)
        news_articles = generate_news_cards(articles)

        number_of_articles = len(news_articles)
        articles_per_page = 3
        max_value = math.ceil(number_of_articles / articles_per_page)

        if not page:
            page = 1

        return (
            news_articles[(page - 1) * articles_per_page : page * articles_per_page],
            max_value,
        )
    else:
        raise PreventUpdate


@callback(
    Output("stock_info_content", "className"),
    Output("graph", "figure"),
    Output("company_name", "children"),
    Output("company_live_price", "children"),
    Output("company_return_percent", "children"),
    Output("company_return", "children"),
    Output("market_close_time", "children"),
    Output("key_stats_div", "children"),
    Output("recommendation_accordion_div", "children"),
    Output("company-logo", "src"),
    State("start-date", "date"),
    State("end-date", "date"),
    State("ticker_dropdown", "value"),
    Input("submit-button", "n_clicks"),
    Input("period_tabs", "active_tab"),
)
def display_candle(
    start_date, end_date, ticker_dropdown: str, n_clicks: int, active_tab
):
    print(start_date, end_date, ticker_dropdown, n_clicks)
    if ticker_dropdown:
        # Historical Price Figure
        # start = datetime.datetime.now() - datetime.timedelta(days=365 * 3)
        symbol, company_name = (
            ticker_dropdown.split(":")[0],
            ticker_dropdown.split(": ")[1],
        )

        if active_tab == "1M":
            start_date = datetime.datetime.now().date() - datetime.timedelta(weeks=4)
        elif active_tab == "6M":
            start_date = (
                datetime.datetime.now().date() - datetime.timedelta(weeks=4) * 6
            )
        elif active_tab == "1Y":
            start_date = datetime.datetime.now().date() - datetime.timedelta(weeks=52)
        elif active_tab == "5Y":
            start_date = (
                datetime.datetime.now().date() - datetime.timedelta(weeks=52) * 5
            )

        print(f"{symbol=}", f"{company_name=}")
        print(f"{start_date=}", f"{end_date=}")
        agg = StockDataAggregator(
            stock_symbols=symbol, start_time=start_date, end_time=end_date
        )
        df = agg.get_historical_stock_price()
        qf = cf.QuantFig(df, name="")
        qf.add_volume()
        fig = qf.iplot(asFigure=True)

        fig.update_traces(hoverinfo="none")

        fig.data[1].increasing.fillcolor = "#3D9970"
        fig.data[1].increasing.line.color = "#3D9970"
        fig.data[1].decreasing.fillcolor = "#FF4136"
        fig.data[1].decreasing.line.color = "#FF4136"

        fig.data[0]["marker"].pop("line")
        fig.data[0]["marker"]["color"] = tuple(
            [
                color.replace("#808080", "#FF4136")
                for color in fig.data[0]["marker"]["color"]
            ]
        )
        fig.data[0]["marker"]["color"] = tuple(
            [
                color.replace("#17BECF", "#3D9970")
                for color in fig.data[0]["marker"]["color"]
            ]
        )

        fig.update_layout(dict(showlegend=False))

        # Get individual stock information
        stock_info_obj = StockInfo(symbol)
        stock_info_dict = {
            "AAPL": {
                "livePrice": 175.43,
                "regularMarketChange": 2.44,
                "regularMarketChangePercent": 1.41,
                "regularMarketTime": "May 26 7:59:31",
                "regularMarketPreviousClose": 172.99,
                "regularMarketDayRange": "$173.11 - $175.77",
                "fiftyTwoWeekRange": "$124.17 - $176.39",
                "marketCap": "2.76T USD",
                "averageDailyVolume3Month": "54.02M",
                "forwardPE": "29.81",
                "trailingAnnualDividendYield": "0.55%",
            },
            "TGT": {
                "livePrice": 138.93,
                "regularMarketChange": 1.82,
                "regularMarketChangePercent": 1.29,
                "regularMarketTime": "May 26 7:59:31",
                "regularMarketPreviousClose": 140.75,
                "regularMarketDayRange": "$137.21 - $140.48",
                "fiftyTwoWeekRange": "$137.16 - $183.89",
                "marketCap": "64.12B USD",
                "averageDailyVolume3Month": "3.54M",
                "forwardPE": "23.63",
                "trailingAnnualDividendYield": "3.11%",
            }
        }
        if symbol == "AAPL":
            stock_info_dict = stock_info_dict["AAPL"]
        else:
            stock_info_dict = stock_info_dict["TGT"]
        company_live_price = "${:.2f}".format(stock_info_dict["livePrice"])
        reccomendation_accordion = generate_recommendation_accordion(symbol)

        if stock_info_dict is None:
            company_return = "N/A"
            company_return_percent = "N/A"
            market_close_time = "N/A"
            key_stats = "The key statistics are not available at this time due to a Yahoo Finance connection error."
        else:
            if marketChange := stock_info_dict.get("regularMarketChange"):
                company_return = "{:.2f}".format(marketChange)

            company_return_percent = "{:.2f}%".format(
                stock_info_dict["regularMarketChangePercent"]
            )

            market_close_time = (
                "Updated: " + stock_info_dict["regularMarketTime"] + " EST"
            )

            key_stats = generate_key_stats(stock_info_dict)
        return (
            "visible",
            fig,
            company_name,
            company_live_price,
            company_return_percent,
            company_return,
            market_close_time,
            key_stats,
            reccomendation_accordion,
            get_logo(symbol),
        )
    else:
        raise PreventUpdate


@callback(
    Output("add-to-wishlist", "style"),
    Input("account-id", "data"),
)
def update_wishlist_feature(account_id):
    return (
        {"display": "block", "borderRadius": "10px"}
        if account_id
        else {"display": "none"}
    )


@callback(
    Output("wishlist-status", "children"),
    Output("wishlist-status", "style"),
    Output("wishlist-status", "color"),
    Output("wishlist-status", "is_open"),
    State("account-id", "data"),
    State("ticker_dropdown", "value"),
    State("select", "value"),
    Input("add-stock", "n_clicks"),
)
def add_stock_to_wishlist(account_id, ticker_symbol, wishlist, n_clicks):
    try:
        ticker_symbol = ticker_symbol.split(":")[0]

        if n_clicks > 0:
            if r.json().get(account_id, "is-logged-in"):
                resp = requests.put(
                    f"{BASE_URL}api/wishlist/symbol",
                    json={"wishlist_id": wishlist, "ticker_symbol": ticker_symbol},
                    headers={
                        "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}"
                    },
                )
                if resp.ok:
                    return (
                        dbc.Alert(
                            f"Stock {ticker_symbol} added to wishlist",
                            color="success",
                            dismissable=True,
                            className="mt-2",
                        ),
                        {"display": "block"},
                        "success",
                        True,
                    )
                else:
                    return (
                        dbc.Alert(
                            str(resp.json()["detail"]),
                            color="danger",
                            dismissable=True,
                            className="mt-2",
                        ),
                        {"display": "block"},
                        "danger",
                        True,
                    )
        return "", {"display": "none"}, "danger", False

    except:
        ticker_symbol = ticker_symbol.split(":")[0]

        if n_clicks > 0:
            if r.json().get(account_id, "is-logged-in"):
                resp = requests.put(
                    f"{BASE_URL}wishlist/symbol",
                    json={"wishlist_id": wishlist, "ticker_symbol": ticker_symbol},
                    headers={
                        "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}"
                    },
                )
                if resp.ok:
                    return (
                        f"Stock {ticker_symbol} added to wishlist",
                        {"display": "block"},
                        "success",
                        True,
                    )
                else:
                    return (
                        str(resp.json()["detail"]),
                        {"display": "block"},
                        "danger",
                        True,
                    )
        return "", {"display": "none"}, "danger", False


@callback(
    Output("modal", "is_open"),
    [Input("add-to-wishlist", "n_clicks"), Input("add-stock", "n_clicks")],
    [State("modal", "is_open")],
)
def toggle_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open
    return is_open


@callback(
    Output("select", "options"),
    Input("account-id", "data"),
)
def update_select_options(account_id: str):
    if r.json().get(account_id, "is-logged-in"):
        results = requests.get(f"{BASE_URL}api/wishlist/{account_id}").json()
        return [{"label": result["name"], "value": result["id"]} for result in results]
    else:
        return []


@callback(
    Output("graph-tooltip-5", "show"),
    Output("graph-tooltip-5", "bbox"),
    Output("graph-tooltip-5", "children"),
    Input("graph", "hoverData"),
)
def display_hover(hoverData):
    if hoverData is None:
        return False, no_update, no_update

    # Only shows the first point:
    pt = hoverData["points"][0]
    bbox = pt["bbox"]
    timeStr = datetime.datetime.strptime(pt["x"], "%Y-%m-%d")
    time = timeStr.strftime("%B %d, %Y")

    children = [
        html.Div(
            [
                html.B(f"USD ${pt['close']:.2f}"),
                html.P(
                    [
                        f"{time}",
                        html.Br(),
                        f"High: {pt['high']:.2f}",
                        html.Br(),
                        f"Low: {pt['low']:.2f}",
                    ],
                    style={"color": "gray"},
                ),
            ],
            style={"height": "100px"},
        )
    ]

    return True, bbox, children

@callback(
    Output("graph-tooltip-estimate", "show"),
    Output("graph-tooltip-estimate", "bbox"),
    Output("graph-tooltip-estimate", "children"),
    Input("earnings_estimate", "hoverData"),
)
def display_hover(hoverData):
    if hoverData is None:
        return False, no_update, no_update

    # Only shows the first point:
    pt = hoverData["points"][0]
    bbox = pt["bbox"]

    children = [
        html.Div(
            [   html.P(
                    [
                        f"Sum of EPS: {pt['y']}",
                    ],
                    style={"color": "black"},
                ),
            ],
            style={"height": "30px"},
        )
    ]

    return True, bbox, children

@callback(
    Output("graph-tooltip-history", "show"),
    Output("graph-tooltip-history", "bbox"),
    Output("graph-tooltip-history", "children"),
    Input("earnings_history", "hoverData"),
)
def display_hover(hoverData):
    if hoverData is None:
        return False, no_update, no_update

    # Only shows the first point:
    pt = hoverData["points"][0]
    bbox = pt["bbox"]

    children = [
        html.Div(
            [   html.P(
                    [
                        f"Sum of EPS: {pt['y']}",
                    ],
                    style={"color": "black"},
                ),
            ],
            style={"height": "30px"},
        )
    ]

    return True, bbox, children