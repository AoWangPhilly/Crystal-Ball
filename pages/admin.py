import json
from typing import List, Dict

import dash
import dash_bootstrap_components as dbc
import requests
from dash_extensions.enrich import html, callback, Output, Input, State, ALL

from src.models import Account
from pages.utils import BASE_URL
from src.database import get_db_context, r

dash.register_page(__name__, path="/admin")


def process_account_data() -> List[Dict]:
    """
    Processes the account entity grabbing only the fields we need, like email, role, and name
    :return (List[Dict[str, str]]): a list of the account info
    """
    accounts: List[Account] = requests.get(f"{BASE_URL}accounts/").json()
    output: List[Dict[str, str]] = [
        {
            "id": account["id"],
            "email": account["email"],
            "is_admin": account["is_admin"],
            "name": account["first_name"],
        }
        for account in accounts
    ]
    return output


#
def create_tablerow(account: Dict) -> html.Tr:
    """
    Creates table rows in the admin page
    :param account: an account dictionary that has email, role, and name fields
    :return (html.Tr): a dash html table row containing the account info
    """

    # Delete button that contains the account email, so it knows which
    # account to delete from DB

    # The reset button to change the user's account information (name, email, password, etc)
    update_button = dbc.Button(
        "Update Information",
        id={"type": "dynamic-update", "index": account["id"]},
        outline=True,
        color="primary",
        className="me-1",
        href=f"/dash/update/account/{account['id']}",
        style={
            "border-radius": "10px",
        },
    )

    delete_button = dbc.Button(
        "Delete",
        id={"type": "dynamic-delete", "index": account["id"]},
        outline=True,
        color="danger",
        className="me-1",
        style={
           "border-radius": "10px",
        },
    )
    return html.Tr(
        [
            html.Td(
                dbc.Row(
                    [
                        dbc.Col(
                            html.Img(
                                src=r.get(f"{account['id']}-profile-pic").decode(),
                                height="40px",
                                style={
                                    "display": "block",
                                    "margin": "auto",
                                    "border-radius": "50%",
                                },
                            ),
                            className="m-3",
                            width="auto",
                        ),
                        dbc.Col(
                            children=[
                                html.Div(account["name"]),
                                html.Div(account["email"]),
                            ],
                        ),
                    ],
                    className="g-0 ms-auto flex-nowrap mt-3 mt-md-0",
                    align="center",
                )
            ),
            html.Td(
                dbc.Badge(
                    "Admin",
                    color="warning",
                    style={
                        "border-radius": "10px",
                    },
                )
                if account["is_admin"]
                else dbc.Badge("Investor", color="primary"),
                style={"vertical-align": "middle"},
            ),
            html.Td(
                dbc.Row(
                    [
                        dbc.Col(
                            update_button,
                            className="m-3",
                            width="auto",
                        ),
                        dbc.Col(delete_button),
                    ],
                    className="g-0 ms-auto flex-nowrap mt-3 mt-md-0",
                    align="center",
                ),
                style={"vertical-align": "middle"},
            ),
        ]
    )


def populate_accounts(accounts) -> List[html.Tr]:
    """
    Create all table rows for the table body
    :return (List[html.Tr]): the table body content
    """
    table_body_contents = [create_tablerow(account) for account in accounts]
    return table_body_contents


layout = html.Div(
    [
        html.Div(
            children=[
                dbc.Container(
                    dbc.Table(
                        [
                            html.Thead(
                                html.Tr(
                                    [
                                        html.Th("Users"),
                                        html.Th("Role"),
                                        html.Th(""),
                                    ]
                                )
                            ),
                            html.Tbody(id="accounts"),
                        ]
                    )
                )
            ],
            id="admin-content",
        ),
        html.Div(
            children=[dbc.Container(html.H1(id="admin-auth-message"))],
            style={"display": "none"},
            id="unauthorized-admin",
        ),
    ]
)


@callback(
    Output("unauthorized-admin", "style"),
    Output("admin-content", "style"),
    Output("admin-auth-message", "children"),
    [Input("account-id", "data")],
)
def update_auth(account_id):
    try:
        if r.json().get(account_id, "is-logged-in"):
            with get_db_context() as db:
                account: Account = db.query(Account).get(account_id)
                is_admin = account.is_admin
                if is_admin:
                    return {"display": "none"}, {"display": "block"}, ""
                else:
                    return (
                        {"display": "block"},
                        {"display": "none"},
                        "Investor does not have administrative privileges",
                    )
    except:
        return (
            {"display": "block"},
            {"display": "none"},
            "User is not logged in to view Admin page",
        )


@callback(
    Output("accounts", "children"),
    Input({"type": "dynamic-delete", "index": ALL}, "n_clicks"),
    State("accounts", "children"),
    State("account-id", "data"),
)
def update_and_display_accounts(n_clicks, children, account_id):
    # Update account table
    children = populate_accounts(process_account_data())

    if r.json().get(account_id, "is-logged-in"):
        # Choose which table to delete
        input_id = dash.callback_context.triggered[0]["prop_id"].rsplit(".", 1)[0]
        if "index" in input_id:
            delete_chart = json.loads(input_id)["index"]
            requests.delete(
                f"{BASE_URL}accounts/{delete_chart}",
                headers={
                    "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}"
                },
            )
            children = populate_accounts(process_account_data())

    return children
