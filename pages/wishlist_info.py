from pprint import pprint
import json

import dash
import dash_bootstrap_components as dbc
from dash_extensions.enrich import Input, Output, callback, dcc, html, State, ALL
import requests
import dash_mantine_components as dmc
from dash_iconify import DashIconify

from pages.utils import search_stocks, BASE_URL
from src.stock_data.stock_info import StockInfo
from src.stock_data.tickers import get_ticker_name_and_symbol
from dash.exceptions import PreventUpdate
from datetime import datetime
from src.database import r
from src.stock_data.logo import get_logo
from src.stock_data.tickers import find_ticker_name


dash.register_page(__name__, path_template="/wishlist/<id>")


ticker_options_df = get_ticker_name_and_symbol()

search_bar = html.Div(
    [
        html.Div(
            children=[
                dcc.Dropdown(
                    id="wishlist_dropdown",
                    placeholder="Add ticker...",
                    optionHeight=80,
                ),
            ]
        )
    ]
)


@callback(
    Output("wishlist_dropdown", "options"), Input("wishlist_dropdown", "search_value")
)
def update_options(search_value):
    if not search_value:
        raise PreventUpdate
    return search_stocks(
        search_value=search_value,
        ticker_options_df=ticker_options_df,
    )


def get_wishlist_name(wishlist_id):
    resp = requests.get(
        f"{BASE_URL}api/wishlist/find?wishlist_id={wishlist_id}",
    )
    print(f"{resp.text=}")
    if resp.ok:
        return resp.json()[0]["name"]

    return ""


def get_price_percentage(symbol):
    stock_info_obj = StockInfo(symbol)
    stock_info_dict = stock_info_obj.get_info()
    if stock_info_dict is None:
        return "N/A"
    company_return_percent = "{:.2f}%".format(
        stock_info_dict["regularMarketChangePercent"]
    )
    return company_return_percent


def get_price_color(return_percent: str):
    if return_percent[0] != "-":
        return "#13b955"
    else:
        return "#fc3939"


def get_date_added(id):
    resp = requests.get(
        f"{BASE_URL}api/wishlist/find?wishlist_id={id}",
    )
    if resp.ok:
        timestamp = resp.json()[0]["timestamp"][0:10]
        timeStr = datetime.strptime(timestamp, "%Y-%m-%d")
        return timeStr.strftime("%B %d, %Y")

    return ""


def create_wishlist_table(stock_symbols):
    table_header = [
        html.Thead(
            html.Tr(
                [
                    html.Th("Ticker Symbol"),
                    # html.Th("Daily Price Change"),
                    html.Th("Date Added"),
                    html.Th("Remove"),
                ]
            ),
            className="mt-3",
        )
    ]

    table_body = [
        html.Tbody(
            children=[
                html.Tr(
                    [
                        html.Td(
                            children=[
                                dbc.Row(
                                    [
                                        dbc.Col(
                                            dmc.Avatar(src=get_logo(symbol=symbol)),
                                            width=2,
                                        ),
                                        dbc.Col(
                                            html.A(
                                                symbol,
                                                href=f"/dash/stock?symbol={find_ticker_name(symbol)}",
                                            )
                                        ),
                                    ]
                                )
                            ]
                        ),
                        # html.Td(
                        #     get_price_percentage(symbol=symbol),
                        #     style={
                        #         "color": get_price_color(
                        #             get_price_percentage(symbol=symbol)
                        #         )
                        #     },
                        # ),
                        html.Td(get_date_added(wishlist_id)),
                        html.Td(
                            dbc.Row(
                                [
                                    dbc.Col(
                                        dbc.Button(
                                            DashIconify(
                                                icon="iwwa:delete",
                                                width=30,
                                                height=30,
                                                className="zoom",
                                            ),
                                            id={
                                                "type": "dynamic-delete",
                                                "index": symbol,
                                            },
                                            style={
                                                "borderRadius": "10px",
                                            },
                                        )
                                    )
                                ],
                                className="g-0 ms-auto flex-nowrap mt-3 mt-md-0",
                                align="center",
                            ),
                            style={"vertical-align": "middle"},
                        ),
                    ]
                )
                for symbol in stock_symbols
            ]
        )
    ]
    table = dbc.Table(table_header + table_body, bordered=True)
    return table


@callback(
    Output("wishlist-table", "children"),
    Output("wishlist-alert", "children"),
    Output("wishlist-alert", "style"),
    Output("wishlist-alert", "color"),
    Output("wishlist-alert", "is_open"),
    State("account-id", "data"),
    Input("wishlist_dropdown", "value"),
)
def populate_table(account_id, ticker_dropdown):
    resp = requests.get(
        f"{BASE_URL}api/wishlist/find?wishlist_id={wishlist_id}",
    )
    stock_symbols = resp.json()[0]["stock_symbols"]

    if ticker_dropdown:
        ticker_symbol = ticker_dropdown.split(":")[0]
        add_stock_to_wishlist = requests.put(
            f"{BASE_URL}api/wishlist/symbol",
            json={
                "ticker_symbol": ticker_symbol,
                "wishlist_id": wishlist_id,
            },
            headers={
                "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}"
            },
        )

        if not add_stock_to_wishlist.ok:
            return (
                create_wishlist_table(stock_symbols=stock_symbols),
                add_stock_to_wishlist.json()["detail"],
                {"display": "block"},
                "danger",
                True,
            )
        else:
            stock_symbols.append(ticker_symbol)
            return (
                create_wishlist_table(stock_symbols=stock_symbols),
                f"{ticker_symbol} added in wishlist",
                {"display": "block"},
                "success",
                True,
            )

    return (
        create_wishlist_table(stock_symbols=stock_symbols),
        "",
        {"display": "none"},
        "danger",
        False,
    )


@callback(
    Output("wishlist-table", "children"),
    State("account-id", "data"),
    Input({"type": "dynamic-delete", "index": ALL}, "n_clicks"),
)
def remove_stock_from_wishlist(account_id, n_clicks):
    stock_id = dash.callback_context.triggered[0]["prop_id"].rsplit(".", 1)[0]

    resp = requests.get(
        f"{BASE_URL}api/wishlist/find?wishlist_id={wishlist_id}",
    )
    stock_symbols = resp.json()[0]["stock_symbols"]

    if any(n_clicks):
        if "index" in stock_id:
            delete_chart = json.loads(stock_id)["index"]
            requests.delete(
                f"{BASE_URL}api/wishlist/symbol",
                json={
                    "ticker_symbol": delete_chart,
                    "wishlist_id": wishlist_id,
                },
                headers={
                    "Authorization": f"Bearer {r.json().get(account_id, 'bearer-token')}"
                },
            )
            stock_symbols.remove(delete_chart)

            return create_wishlist_table(stock_symbols=stock_symbols)
    return create_wishlist_table(stock_symbols=stock_symbols)


def layout(id=None):
    global wishlist_id
    wishlist_id = id

    alert = dbc.Alert(
        id="wishlist-alert",
        style={"display": "none"},
        className="mt-4",
        dismissable=True,
    )

    return dbc.Container(
        children=[
            html.H1(f"Wishlist {get_wishlist_name(id)}"),
            dbc.Alert(
                "Let's get started! Initially the wishlist is blank, click the dropdown and type the stock symbol you wish to add to the list.",
                color="info",
                id="wishlist-info",
                dismissable=True,
            ),
            search_bar,
            alert,
            dcc.Loading(
                [
                    html.Div(id="wishlist-table", className="mt-4"),
                ],
                type="dot",
            ),
        ]
    )
