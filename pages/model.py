from datetime import datetime, timedelta
from typing import Dict, List

import dash
import dash_bootstrap_components as dbc
import dash_daq as daq
from dash_extensions.enrich import html, dcc, callback, Output, Input, State, dash_table

from dash.exceptions import PreventUpdate
import yfinance as yf
import pandas as pd

from pages.utils import search_stocks
from src.stock_data.tickers import get_ticker_name_and_symbol
from src.monte_carlo.model import (
    generate_predicted_prices,
    show_historical_and_predicted_price,
    graph_monte_carlo_simulations,
    graph_predicted_price_distribution,
    graph_returns_distribution,
)
from src.database import cache_dataframe, get_cached_dataframe, r
from src.monte_carlo.metrics import (
    calculate_stock_metrics,
    interpret_beta,
    interpret_sharpe_ratio,
)

dash.register_page(__name__, path="/model")

ticker_options_df = get_ticker_name_and_symbol()

ticker_symbol_input = [
    dbc.Label("Ticker symbol: "),
    dcc.Dropdown(
        placeholder="GOOG: Alphabet Inc. Class C Capital Stock",
        id="ticker-symbol",
        value="GOOG: Alphabet Inc. Class C Capital Stock",
    ),
]

start_date_input = [
    html.Div(
        dbc.Label(
            children=[
                "Start date ",
                html.I(
                    className="bi bi-info-circle-fill me-2",
                    id="start-time-target",
                ),
                ":",
            ]
        )
    ),
    dcc.DatePickerSingle(
        id="start-date",
        month_format="MMM Do, YYYY",
        placeholder="MMM Do, YY",
        date=datetime.now().date() - timedelta(days=365),
    ),
]

end_date_input = [
    html.Div(
        dbc.Label(
            children=[
                "End date ",
                html.I(
                    className="bi bi-info-circle-fill me-2",
                    id="end-time-target",
                ),
                ":",
            ]
        )
    ),
    dcc.DatePickerSingle(
        id="end-date",
        month_format="MMM Do, YYYY",
        placeholder="MMM Do, YY",
        date=datetime.now().date(),
    ),
]

days_to_predict_input = [
    html.Div(
        dbc.Label(
            children=[
                "Predict for ",
                html.I(
                    className="bi bi-info-circle-fill me-2",
                    id="predict-for-date-target",
                ),
                ":",
            ]
        )
    ),
    dcc.DatePickerSingle(
        id="day-to-predict-date",
        month_format="MMM Do, YYYY",
        placeholder="MMM Do, YY",
        date=datetime.now().date() + timedelta(days=252),
    ),
]

number_of_simulations_input = [
    dbc.Label(
        children=[
            "Simulations ",
            html.I(
                className="bi bi-info-circle-fill me-2",
                id="number-of-simulations-target",
            ),
            ":",
        ]
    ),
    daq.NumericInput(
        id="simulations",
        min=0,
        max=100_000,
        value=1000,
        size=120,
    ),
]

run_simulation_button = [
    dbc.Button(
        className="common-btn shadow mt-3",
        children="Run",
        id="submit-model",
        style={
            "display": "flex",
            "justify-content": "center",
            "align-items": "center",
            "height": "50%",
            "width": "50%",
        },
    ),
]


controls = dbc.Form(
    [
        dbc.Col(
            dbc.Row(
                [
                    dbc.Col(html.H3("Simulation Parameters")),
                    dbc.Col(ticker_symbol_input),
                    dbc.Col(start_date_input),
                    dbc.Col(end_date_input),
                    dbc.Col(days_to_predict_input),
                    dbc.Col(number_of_simulations_input),
                    dbc.Col(run_simulation_button),
                ],
                className="mt-3",
            )
        ),
        dbc.Tooltip(
            "Select a start date of the stock you want to predict. "
            "For example, when selecting AAPL stock with a start date of 01/01/10 "
            "the model will take data from the start date and onwards.",
            target="start-time-target",
        ),
        dbc.Tooltip(
            "Select an end date of the stock you want to predict. "
            "For example, when selecting AAPL stock with a end date of 01/01/10 "
            "the model will take data since the stock's IPO to the end date (inclusive).",
            target="end-time-target",
        ),
        dbc.Tooltip(
            "Select a date to predict the stock price for. Only pick dates after the end date.",
            target="predict-for-date-target",
        ),
        dbc.Tooltip(
            "For the model, the more simulations there are, the more "
            "accurate the predicted price. However, the duration takes longer.",
            target="number-of-simulations-target",
        ),
    ]
)


def human_format(x):
    for unit in ["", "K", "M", "B", "T"]:
        if abs(x) < 1000.0:
            return f"{x:.2f}{unit}"
        x /= 1000.0

    return f"{x:.2f}{'T'}"


def create_card(title: str, content: str, id_name: str) -> dbc.Card:
    return dbc.Card(
        dbc.CardBody(
            [
                html.P(title, className="card-title", id=id_name),
                html.H3(content, className="card-text"),
            ]
        ),
        className="mb-3",
        style={"border-radius": "10px"},
    )


def generate_metric_cards(metrics: Dict[str, float]) -> List[dbc.Card]:
    last_price_card = create_card(
        "Last Price", f"${metrics['last_price']:.2f}", id_name="last-price-target"
    )

    return_card = create_card(
        "Return", f"{metrics['returns']:.2f}%", id_name="return-target"
    )

    expected_price_card = create_card(
        "Expected Price",
        f"${metrics['expected_return']:.2f}",
        id_name="expected-price-target",
    )

    beta_card = create_card("Beta", f"{metrics['beta']:.2f}", id_name="beta-target")

    capm_card = create_card("CAPM", f"{metrics['capm']:.2f}%", id_name="capm-target")

    sharpe_ratio_card = create_card(
        "Sharpe Ratio", f"{metrics['sharpe_ratio']:.2f}", id_name="sharpe-ratio-target"
    )

    return [
        html.H3("Metrics"),
        html.P(
            "The metrics show potential risk and return of an investment, "
            "helping investors make informed decisions about whether to "
            "invest in a particular asset or portfolio of assets.",
            style={
                "font-size": "16px",
                "font-weight": "lighter",
            },
        ),
        dbc.Row(
            [
                dbc.Col([last_price_card, expected_price_card, capm_card]),
                dbc.Col([return_card, beta_card, sharpe_ratio_card]),
            ],
            className="g-3",
        ),
        dbc.Tooltip(
            "The last price of the stock on the end date.", target="last-price-target"
        ),
        dbc.Tooltip(
            "The change in price from the end date and the predicted date.",
            target="return-target",
        ),
        dbc.Tooltip(
            "The expected price of the stock on the predicted date.",
            target="expected-price-target",
        ),
        dbc.Tooltip(
            "The beta measures the market risk that cannot be avoided through diversification",
            target="beta-target",
        ),
        dbc.Tooltip(
            "The CAPM calculates the expected return of a security adjusted to the risk taken",
            target="capm-target",
        ),
        dbc.Tooltip(
            "The Sharpe Ratio measures the performance of a security compared to a risk-free asset",
            target="sharpe-ratio-target",
        ),
    ]


def generate_analysis(account_id: str) -> html.Div:
    params = r.json().get(account_id, "model-params")
    metrics = r.json().get(account_id, "metrics")
    last_price = metrics["last_price"]
    expected_price = metrics["expected_return"]
    ticker_symbol = params["ticker_symbol"].split(":")[0]
    start_date = datetime.strptime(params["start_date"], "%Y-%m-%d").strftime(
        "%m/%d/%Y"
    )
    end_date = datetime.strptime(params["end_date"], "%Y-%m-%d").strftime("%m/%d/%Y")
    predict_date = datetime.strptime(params["predict_for_date"], "%Y-%m-%d").strftime(
        "%m/%d/%Y"
    )
    number_of_simulations = params["number_of_simulations"]

    param_content = (
        f"The model's input takes in the {ticker_symbol} stock's historical stock price between "
        f"{start_date} to {end_date} from Yahoo Finance. The last actual price "
        f"of the stock was ${last_price:,.2f}. After running {number_of_simulations} simulations,"
        f"the model predicts the stock's price on {predict_date} to be ${expected_price:,.2f}. "
        f"Therefore, the model predicts the stock to have a return of {metrics['returns']:.2f}%."
    )

    metric_content = (
        f"{interpret_sharpe_ratio(metrics['sharpe_ratio'])} "
        f"{interpret_beta(metrics['beta'])} "
        f"After the calculations, the CAPM is {metrics['capm']:.2f}%."
    )

    return [
        html.P(
            param_content,
            style={
                "font-size": "16px",
                "font-weight": "lighter",
            },
            className="mb-3",
        ),
        html.P(
            metric_content,
            style={
                "font-size": "16px",
                "font-weight": "lighter",
            },
        ),
    ]


layout = html.Div(
    className="small",
    children=[
        dbc.Container(
            fluid="sm",
            id="model-content",
            children=[
                dbc.Row(controls),
                dbc.Row(dbc.Col(id="param-content", width=10)),
                dbc.Row(
                    children=[
                        dbc.Col(
                            dbc.Card(
                                dbc.CardBody(
                                    [
                                        dcc.Loading(
                                            dcc.Graph(
                                                id="historical-and-predicted-price-figure",
                                            ),
                                            type="dot",
                                        )
                                    ]
                                )
                            ),
                        ),
                        dbc.Col(id="metric-card-content", width=3),
                    ],
                ),
                dbc.Row([html.H3("Analysis"), dbc.Col(id="metric-content", width=10)]),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    children=[
                                        html.H3("Historical Stock Data"),
                                        html.P(
                                            "Below is the data used by the Monte Carlo Simulation Model. "
                                            "To get all the stock data, click the download button to export a CSV file.",
                                            style={
                                                "font-size": "16px",
                                                "font-weight": "lighter",
                                            },
                                        ),
                                    ],
                                ),
                                dbc.Col(
                                    width=2,
                                    children=[
                                        dbc.Button(
                                            "Download",
                                            id="download-btn-historical-data",
                                            style={
                                                "display": "flex",
                                                "justify-content": "center",
                                                "align-items": "center",
                                                "height": "50%",
                                                "float": "right",
                                            },
                                            class_name="common-btn shadow mt-3",
                                        ),
                                        dcc.Download(id="download-historical-data"),
                                    ],
                                ),
                            ]
                        )
                    ],
                ),
                dbc.Row(
                    [
                        html.Div(id="historical-stock-data-table"),
                    ]
                ),
                dbc.Row(
                    className="mt-3",
                    children=[
                        dbc.Col(
                            dbc.Card(
                                dbc.CardBody(
                                    dcc.Loading(
                                        dcc.Graph(id="simulations-figure"), type="dot"
                                    )
                                )
                            )
                        ),
                        html.Div(
                            [
                                dbc.Row(
                                    [
                                        dbc.Col(
                                            class_name="mt-3",
                                            children=[
                                                html.H3("Monte Carlo Simulation Data"),
                                                html.P(
                                                    "Below is the data used to generate the graph. "
                                                    "To get all runs of the simulation, click the download button to export a CSV file.",
                                                    style={
                                                        "font-size": "16px",
                                                        "font-weight": "lighter",
                                                    },
                                                ),
                                            ],
                                        ),
                                        dbc.Col(
                                            width=2,
                                            children=[
                                                dbc.Button(
                                                    "Download",
                                                    id="download-btn-simulation-data",
                                                    style={
                                                        "display": "flex",
                                                        "justify-content": "center",
                                                        "align-items": "center",
                                                        "height": "50%",
                                                        "float": "right",
                                                    },
                                                    class_name="common-btn shadow mt-3",
                                                ),
                                                dcc.Download(
                                                    id="download-simulation-data"
                                                ),
                                            ],
                                        ),
                                    ]
                                )
                            ],
                        ),
                        html.Div(id="simulation-data-table"),
                    ],
                ),
                dbc.Row(
                    className="mt-3",
                    children=[
                        dbc.Col(
                            dbc.Card(
                                dbc.CardBody(
                                    dcc.Loading(
                                        dcc.Graph(
                                            id="predicted-price-distribution-figure"
                                        ),
                                        type="dot",
                                    )
                                )
                            )
                        ),
                        dbc.Col(
                            dbc.Card(
                                dbc.CardBody(
                                    dcc.Loading(
                                        dcc.Graph(id="log-returns-figure"), type="dot"
                                    )
                                )
                            )
                        ),
                    ],
                ),
            ],
        ),
        html.Div(
            children=[
                dbc.Container(html.H1("User is not logged in to view Model page"))
            ],
            style={"display": "none"},
            id="unauthorized-model",
        ),
    ],
)


@callback(Output("ticker-symbol", "options"), Input("ticker-symbol", "search_value"))
def update_options(search_value):
    if not search_value:
        raise PreventUpdate
    return search_stocks(
        search_value=search_value,
        ticker_options_df=ticker_options_df,
    )


@callback(
    Output("unauthorized-model", "style"),
    Output("model-content", "style"),
    [Input("account-id", "data")],
)
def update_auth(account_id):
    try:
        if r.json().get(account_id, "is-logged-in"):
            return {"display": "none"}, {"display": "block"}
    except:
        return {"display": "block"}, {"display": "none"}


@callback(
    Output("download-historical-data", "data"),
    State("account-id", "data"),
    Input("download-btn-historical-data", "n_clicks"),
    prevent_initial_call=True,
)
def download_historical_data(account_id, n_clicks):
    if r.exists(f"{account_id}-historical-stock-price-data"):
        df = get_cached_dataframe(
            account_id=account_id, key="formatted-historical-stock-price"
        )
    else:
        df = pd.DataFrame()
    return dcc.send_data_frame(df.to_csv, "historical-stock-price.csv")


@callback(
    Output("download-simulation-data", "data"),
    State("account-id", "data"),
    Input("download-btn-simulation-data", "n_clicks"),
    prevent_initial_call=True,
)
def download_simulation_data(account_id, n_clicks):
    if r.exists(f"{account_id}-predicted-stock-price"):
        df = get_cached_dataframe(account_id=account_id, key="predicted-stock-price")
    else:
        df = pd.DataFrame()
    return dcc.send_data_frame(df.to_csv, "predicted-stock-price.csv")


@callback(
    Output("historical-and-predicted-price-figure", "figure"),
    Output("simulations-figure", "figure"),
    Output("log-returns-figure", "figure"),
    Output("predicted-price-distribution-figure", "figure"),
    Output("metric-card-content", "children"),
    Output("simulation-data-table", "children"),
    Output("historical-stock-data-table", "children"),
    Output("ticker-symbol", "value"),
    Output("ticker-symbol", "placeholder"),
    Output("start-date", "date"),
    Output("end-date", "date"),
    Output("day-to-predict-date", "date"),
    Output("simulations", "value"),
    Output("param-content", "children"),
    Output("metric-content", "children"),
    State("ticker-symbol", "value"),
    State("start-date", "date"),
    State("end-date", "date"),
    State("day-to-predict-date", "date"),
    State("simulations", "value"),
    State("account-id", "data"),
    Input("submit-model", "n_clicks"),
    # prevent_initial_call=True,
)
def run_simulation(
    ticker_symbol: str,
    start_date: str,
    end_date: str,
    predict_for_date: str,
    number_of_simulations: int,
    account_id: str,
    n_clicks: int,
):
    model_params = {
        "ticker_symbol": ticker_symbol,
        "start_date": start_date,
        "end_date": end_date,
        "predict_for_date": predict_for_date,
        "number_of_simulations": number_of_simulations,
    }

    ticker_symbol = ticker_symbol.split(":")[0]

    changed_id = [p["prop_id"] for p in dash.callback_context.triggered][0]

    model_param_does_not_exist = False
    model_param_updated = False
    try:
        model_param_updated = r.json().get(account_id, "model-params") != model_params
    except:
        model_param_does_not_exist = True

    if model_param_does_not_exist or (
        "submit-model" in changed_id and model_param_updated
    ):
        data = yf.download(
            tickers=[ticker_symbol, "^GSPC"], start=start_date, end=end_date
        )
        adjusted_close = data["Adj Close"]
        predicted_prices = generate_predicted_prices(
            data=adjusted_close,
            end=predict_for_date,
            trials=number_of_simulations,
        )

        columns = data.columns.map(lambda col: col[1] != "^GSPC")
        formatted_historical_stock_price = data[data.columns[columns]]
        formatted_historical_stock_price.columns = (
            formatted_historical_stock_price.columns.get_level_values(0)
        )
        formatted_historical_stock_price = (
            formatted_historical_stock_price.dropna().reset_index()
        )

        formatted_historical_stock_price.Date = (
            formatted_historical_stock_price.Date.dt.date
        )

        formatted_historical_stock_price["Volume"] = formatted_historical_stock_price[
            "Volume"
        ].apply(human_format)
        float_cols = formatted_historical_stock_price.select_dtypes(
            include=["float"]
        ).columns

        # apply a mapping function to the floating-point columns
        formatted_historical_stock_price[float_cols] = (
            formatted_historical_stock_price[float_cols]
            .round(2)
            .applymap(lambda x: f"${x:,.2f}")
        )

        metrics = calculate_stock_metrics(
            data=adjusted_close, predicted_price=predicted_prices
        )

        formatted_predicted_prices = (
            predicted_prices.copy(deep=True)
            .iloc[:, :10]
            .copy(deep=True)
            .select_dtypes(include="float")
            .round(2)
            .applymap(lambda x: f"${x:,.2f}")
            .reset_index()
            .rename(columns={"index": "Date"})
        )

        r.json().set(account_id, "model-params", model_params)

        cache_dataframe(
            account_id=account_id, key="historical-stock-price-data", df=data
        )
        cache_dataframe(
            account_id=account_id, key="predicted-stock-price", df=predicted_prices
        )
        cache_dataframe(
            account_id=account_id,
            key="formatted-historical-stock-price",
            df=formatted_historical_stock_price,
        )
        r.json().set(account_id, "metrics", metrics)
        cache_dataframe(
            account_id=account_id,
            key="formatted-predicted-prices",
            df=formatted_predicted_prices,
        )

    else:
        print("cache reached!!")
        model_params = r.json().get(account_id, "model-params")
        data = get_cached_dataframe(
            account_id=account_id, key="historical-stock-price-data"
        )
        adjusted_close = data["Adj Close"]
        predicted_prices = get_cached_dataframe(
            account_id=account_id, key="predicted-stock-price"
        )
        formatted_historical_stock_price = get_cached_dataframe(
            account_id=account_id, key="formatted-historical-stock-price"
        )
        metrics = r.json().get(account_id, "metrics")
        formatted_predicted_prices = get_cached_dataframe(
            account_id=account_id, key="formatted-predicted-prices"
        )

    historical_and_predicted_price_fig = show_historical_and_predicted_price(
        data=adjusted_close,
        predicted_price=predicted_prices,
    )

    simulation_fig = graph_monte_carlo_simulations(predicted_prices=predicted_prices)
    returns_fig = graph_returns_distribution(stock_price=adjusted_close)
    predicted_price_fig = graph_predicted_price_distribution(
        predicted_prices=predicted_prices
    )

    metric_cards = generate_metric_cards(metrics=metrics)

    simulation_data_table = dash_table.DataTable(
        formatted_predicted_prices.to_dict("records"),
        columns=[{"name": i, "id": i} for i in formatted_predicted_prices.columns],
        markdown_options={"html": True},
        style_as_list_view=True,
        page_size=5,
        page_action="native",
        style_header={
            "backgroundColor": "#F9FAFB",
            "border": "1px solid #EAECF0",
            "color": "#667085",
            "font-size": "16px",
            "padding": "10px",
        },
        style_cell={
            "font-family": "Inter",
            "font-size": "16px",
            "textAlign": "center",
            "height": "40px",
            "padding": "10px",
        },
        style_table={
            "border": "1px solid #EAECF0",
            "box-shadow": "0px 1px 3px rgba(16, 24, 40, 0.1), 0px 1px 2px rgba(16, 24, 40, 0.06)",
            "border-radius": "8px",
            "overflowX": "auto",
        },
    )

    historical_stock_data_table = dash_table.DataTable(
        formatted_historical_stock_price.to_dict("records"),
        columns=[
            {"name": i, "id": i} for i in formatted_historical_stock_price.columns
        ],
        markdown_options={"html": True},
        style_as_list_view=True,
        page_size=5,
        page_action="native",
        style_header={
            "backgroundColor": "#F9FAFB",
            "border": "1px solid #EAECF0",
            "color": "#667085",
            "font-size": "16px",
            "padding": "10px",
        },
        style_cell={
            "font-family": "Inter",
            "font-size": "16px",
            "textAlign": "center",
            "height": "40px",
            "padding": "10px",
        },
        style_table={
            "border": "1px solid #EAECF0",
            "box-shadow": "0px 1px 3px rgba(16, 24, 40, 0.1), 0px 1px 2px rgba(16, 24, 40, 0.06)",
            "border-radius": "8px",
            "overflowX": "auto",
        },
    )

    param_content, metric_content = generate_analysis(account_id=account_id)
    return (
        historical_and_predicted_price_fig,
        simulation_fig,
        returns_fig,
        predicted_price_fig,
        metric_cards,
        simulation_data_table,
        historical_stock_data_table,
        model_params["ticker_symbol"],
        model_params["ticker_symbol"],
        model_params["start_date"],
        model_params["end_date"],
        model_params["predict_for_date"],
        model_params["number_of_simulations"],
        param_content,
        metric_content,
    )
