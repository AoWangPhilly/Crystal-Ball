import os
from typing import List
from PIL import Image
import io
import base64

import pandas as pd

from src.config import settings

PORT = int(os.environ.get("PORT", 17995)) if settings.heroku_on else 8000
HOST = "127.0.0.1"
BASE_URL = f"http://{HOST}:{PORT}/"


def search_stocks(
    search_value: str,
    ticker_options_df: pd.DataFrame,
) -> List[str]:
    dropdown_options = ticker_options_df["Symbol_Name"].to_list()
    search_value = search_value.upper()
    # Search elements that contain search_value
    specific_symbol_df = ticker_options_df.query("Symbol == @search_value")
    # Put specific symbol search on top of results
    if not specific_symbol_df.empty:
        specific_symbol = (
            specific_symbol_df["Symbol"] + ": " + specific_symbol_df["Name"]
        ).values[0]
        dropdown_options.remove(specific_symbol)
        dropdown_options.insert(0, specific_symbol)

    # Limit Search Results to 10
    return dropdown_options[:10]


def resize_profile_picture(base64_img, width, height):
    """
    Resize an SVG image from base64-encoded string.

    Args:
        base64_img (str): Base64-encoded SVG image.
        width (int): New width for the resized image.
        height (int): New height for the resized image.

    Returns:
        str: Base64-encoded string of the resized image.
    """
    # Decode the base64 SVG string into bytes
    img_bytes = base64.b64decode(base64_img.split(",")[1])

    # Create an in-memory IO stream to read the SVG bytes
    img_io = io.BytesIO(img_bytes)

    # Open the SVG image using Pillow
    image = Image.open(img_io)

    # Resize the image
    image = image.resize((width, height))

    # Save the resized image to a new in-memory IO stream
    resized_io = io.BytesIO()
    image.save(resized_io, format="PNG")

    # Get the resized image bytes and encode them to base64
    resized_image_bytes = resized_io.getvalue()
    resized_image_base64 = base64.b64encode(resized_image_bytes).decode("ascii")

    return "data:image/png;base64,{}".format(resized_image_base64)
