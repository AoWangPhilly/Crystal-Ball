import math
import datetime
from typing import List, Dict

from src.classes.news_article.news_article import NewsAPI
from src.config import settings

import dash
import dash_bootstrap_components as dbc
from dash_extensions.enrich import Output, Input, State, callback, html

dash.register_page(__name__, path="/news")

news_api = NewsAPI(settings.news_api_key)


def generate_news_cards(articles: List[Dict[str, str]]) -> List[dbc.Card]:
    card_list = []
    for _, row in articles.iterrows():
        card = dbc.Card(
            class_name="news-card",
            children=[
                dbc.CardBody(
                    children=[
                        html.A(
                            href=row["url"],
                            children=[
                                html.H4(
                                    row["description"],
                                    className="card-title news-card-title",
                                ),
                            ],
                        ),
                        html.H6(
                            datetime.datetime.strptime(
                                row["publishedAt"][0:10], "%Y-%m-%d"
                            ).strftime("%B %d, %Y"),
                            className="card-subtitle pb-1",
                        ),
                        html.H6(row["source"], className="card-subtitle"),
                        html.P(
                            children=[row["content"][:200]],
                            className="news-card-text",
                        ),
                        dbc.CardImg(
                            class_name="news-card-image", src=row["urlToImage"]
                        ),
                        html.P(
                            children=[row["sentiment_category"]],
                            style={
                                "color": "green"
                                if row["sentiment_category"]
                                == "Article Sentiment: Overwhelmingly Positive"
                                or row["sentiment_category"]
                                == "Article Sentiment: Positive"
                                else (
                                    "orange"
                                    if row["sentiment_category"]
                                    == "Article Sentiment: Neutral"
                                    else "red"
                                )
                            },
                        ),
                    ]
                ),
            ],
        )
        card_list.append(card)
    return card_list


@callback(
    Output("news_card_div", "children"),
    Output("pagination", "max_value"),
    Input("search_input", "n_submit"),
    State("search_input", "value"),
    Input("news_tabs", "active_tab"),
    Input("pagination", "active_page"),
    prevent_initial_callback=True,
)
def news_search(n_submit, search, active_tab, page):
    if not search:
        search = "stock market"

    # Check which button was clicked and set the appropriate time range
    if active_tab == "latest_news_tab":
        to_date = datetime.datetime.now().strftime("%Y-%m-%d")
        from_date = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime(
            "%Y-%m-%d"
        )
    else:
        to_date = datetime.datetime.now().strftime("%Y-%m-%d")
        from_date = (datetime.datetime.now() - datetime.timedelta(days=7)).strftime(
            "%Y-%m-%d"
        )

    headlines = news_api.get_headlines(
        keywords=search, from_date=from_date, to_date=to_date
    )
    articles = news_api.preprocess_json(headlines)

    if articles.empty:
        return (
            html.Img(
                src="assets/images/no_articles_found.jpeg",
                style={
                    "width": "auto",
                    "height": "700px",
                },
            ),
            0,
        )
    articles = generate_news_cards(articles)
    articles_per_page = 5
    max_value = math.ceil(len(articles) / articles_per_page)
    if not page:
        page = 1

    return (
        articles[(page - 1) * articles_per_page : page * articles_per_page],
        max_value,
    )


layout = html.Div(
    children=[
        html.Div(
            className="d-flex justify-content-center my-3",
            children=[
                dbc.Tabs(
                    id="news_tabs",
                    active_tab="latest_news_tab",
                    children=[
                        dbc.Tab(label="Daily News", tab_id="latest_news_tab"),
                        dbc.Tab(label="Weekly News", tab_id="last_week_news_tab"),
                    ],
                    style={"width": "60%"},
                ),
            ],
        ),
        html.Div(
            className="d-flex justify-content-center mt-2 mb-2",
            children=[
                dbc.Input(
                    id="search_input",
                    placeholder="Search for News..",
                    type="text",
                    style={"width": "60%", "border-radius": "10px"},
                ),
            ],
        ),
        dbc.Spinner(
            [
                html.Div(
                    id="news_card_div",
                    className="news-card-holder d-flex justify-content-center",
                ),
                dbc.Pagination(
                    id="pagination",
                    max_value=5,
                    class_name="d-flex justify-content-center mt-3",
                    fully_expanded=False,
                ),
            ],
            color="#8742f5",
        ),
    ]
)
