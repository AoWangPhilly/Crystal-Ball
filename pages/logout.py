import dash
import dash_bootstrap_components as dbc
from dash import html

dash.register_page(__name__, path='/logout')

layout = html.Div(
    dbc.Container(
        html.H1("You've been logged out")
    )
)
