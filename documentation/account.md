# Account Class Documentation

The Account class is a class for managing user accounts in a database. It has the following properties:

- email: The email of the user.
- password: The hashed password of the user.
- db_session: The database session.

It also has the following methods:

## __init__
The constructor which sets up the class with an email, password, and db_session.

## __hash_password
A private function that hashes the password using the SHA-256 algorithm.

## register
A function to register an account. It checks if the email already exists and if the password meets certain criteria. If the account does not exist, it adds the email and hashed password to the database.

## change_password
A function to change the password for an account. It updates the password in the database and sends a confirmation email.

## get_account_details
A function to retrieve the account details for a user.

Example Usage:

```python
# creating a new Account object
db_session = ... # setup database session
account = Account("user@example.com", "secret_password", db_session)

# registering a new account
account.register()

# changing the password for an existing account
account.change_password("new_secret_password")

# retrieving the account details
account_details = account.get_account_details()
print(account_details)
