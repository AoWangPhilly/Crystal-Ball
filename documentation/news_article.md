## NewsAPI

A class to retrieve news data from the NewsAPI, preprocess the data, and write it to a SQL database.

### Parameters
- `api_key` (str): The API key to access the NewsAPI.

### Methods

#### get_top_headlines

Retrieves the top headlines from the NewsAPI based on the specified parameters.

##### Parameters
- `category` (str, optional): The category of the headlines.
- `sources` (list, optional): The sources of the headlines.
- `keywords` (str, optional): The keywords to search for in the headlines.

##### Returns
- JSON data of the top headlines from the NewsAPI.

#### preprocess_json

Preprocesses the JSON data from the NewsAPI to extract the relevant information and convert it to a pandas DataFrame.

##### Parameters
- `json_data` (dict): The JSON data from the NewsAPI.

##### Returns
- The preprocessed data in the form of a pandas DataFrame.

#### write_to_db

Writes the data in the DataFrame to a specified SQL table.

##### Parameters
- `df` (pandas.DataFrame): The DataFrame containing the data to be written to the SQL table.
- `table_name` (str): The name of the SQL table to write the data to.
- `db_username` (str): The username for the SQL database.
- `db_password` (str): The password for the SQL database.
- `db_hostname` (str): The hostname for the SQL database.
- `db_port` (int): The port number for the SQL database.
- `db_name` (str): The name of the SQL database.
