# NewsSentiment Class

The `NewsSentiment` class is used to analyze the sentiment of news articles fetched from the NewsAPI.

## Properties
- `news_api`: an instance of the `NewsAPI` class used to fetch news articles from the NewsAPI
- `sentiment_analyzer`: an instance of the `SentimentIntensityAnalyzer` class from the `nltk.sentiment` module used to analyze the sentiment of news articles
- `results`: a list of dictionaries containing the title, description, and sentiment category of processed news articles

## Methods
- `analyze_sentiment(text)`: analyzes the sentiment of a given text using the `SentimentIntensityAnalyzer` instance
- `categorize_sentiment(sentiment)`: categorizes the sentiment of a given sentiment score into "Positive", "Negative", or "Neutral"
- `process_articles(articles)`: processes a list of `Article` objects from the NewsAPI by analyzing their sentiment and categorizing it, and adds the title, description, and sentiment category to the `results` list

### Example
```python
news_api_key = os.getenv("NEWS_API_KEY")
news_api = NewsAPI(news_api_key)
news_articles = news_api.get_headlines()
sentiment_analyzer = NewsSentiment(news_api_key)

sentiment_analyzer.process_articles(news_articles)

for result in sentiment_analyzer.results:
    print(result)
