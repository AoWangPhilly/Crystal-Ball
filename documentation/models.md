# Models

This script defines multiple SQLAlchemy models for the database.

## Account

The `Account` model represents an account with the following fields:

- `email`: Email of the user, primary key and cannot be null.
- `first_name`: First name of the user, cannot be null.
- `password`: Password of the user, cannot be null.
- `created_at`: Timestamp of when the account was created, cannot be null and is set to the current time by default.
- `is_admin`: A flag indicating whether the user is an admin or not, cannot be null and is set to False by default.

## Wishlist

The `Wishlist` model represents a list of stock symbols saved by a user with the following fields:

- `id`: Unique identifier for the wishlist, primary key, cannot be null and is auto-incremented.
- `email`: Email of the user, foreign key referencing the `email` field in the `Account` model, cannot be null.
- `name`: Name of the wishlist, cannot be null.
- `stock_symbols`: List of stock symbols in the wishlist, cannot be null and is an empty list by default.
- `timestamp`: Timestamp of when the wishlist was created, cannot be null and is set to the current time by default.

## TrendingArticle

The `TrendingArticle` model represents articles that are trending for specific stocks with the following fields:

- `id`: Unique identifier for the article, primary key, cannot be null and is auto-incremented.
- `stock_symbols`: List of stock symbols the article is trending for, cannot be null.
- `published_at`: Timestamp of when the article was published, cannot be null and is set to the current time by default.
- `article_url`: URL of the article, cannot be null.
- `thumbnail_url`: URL of the thumbnail image of the article, cannot be null.
- `timestamp`: Timestamp of when the article was created, cannot be null and is set to the current time by default.

## RecommendedArticle

The `RecommendedArticle` model represents articles recommended to a user with the following fields:

- `email`: Email of the user, foreign key referencing the `email` field in the `Account` model, primary key and cannot be null.
- `stock_symbols`: List of stock symbols the article is related to, cannot be null.
- `published_at`: Timestamp of when the article was published, cannot be null and is set to the current time by default.
- `article_url`: URL of the article, cannot be null.
- `thumbnail_url`: URL of the thumbnail image of the article, cannot be null.
- `sentiment_result`: Sentiment result of the article, cannot be null.

## PredictorModel
The `PredictorModel` class is used to represent a trained predictor model, as well as its associated metadata. It has the following fields:

- `email`: the email address of the user who created the model. This is a foreign key referencing the `email` field in the `Account` table, and is set with a `ForeignKey` constraint that specifies that the referenced `Account` should be deleted if this `PredictorModel` is deleted. The `email` field is also the primary key of this table.
- `model_id`: a unique identifier for the predictor model.
- `timestamp`: a timestamp indicating when the model was created. This is set to the current time by default using a `server_default` constraint with a value of `text("now()")`.
- `parameter`: the parameters used to train the model, stored as a JSON object.
- `output`: the output of the predictor model, stored as an array of strings.
- `duration`: the duration (in seconds) that the model took to train.
- `accuracy`: the accuracy of the model, expressed as a float.
