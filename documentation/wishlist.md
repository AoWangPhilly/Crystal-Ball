# Wishlist Class

The `Wishlist` class is used to manage a list of tickers.

## Properties
- `tickers`: a list of tickers

## Methods
- `add_ticker(ticker)`: adds a ticker to the `tickers` list
- `remove_ticker(ticker)`: removes a ticker from the `tickers` list

### Example
```python
wishlist = Wishlist()
wishlist.add_ticker("AAPL")
wishlist.add_ticker("AMZN")

print(wishlist.tickers)  # ["AAPL", "AMZN"]

wishlist.remove_ticker("AAPL")

print(wishlist.tickers)  # ["AMZN"]
