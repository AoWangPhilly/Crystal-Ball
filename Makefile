include .env
export

run:
	uvicorn app:app --host=127.0.0.1 --port=8000 --reload

drop_db:
	python3 -m src.database drop

create_db:
	python3 -m src.database build

populate_db:
	PGPASSWORD=$(database_password) psql -U $(database_username) -d $(database_name) -a -f queries/setup.sql

setup_db:
	python3 -m src.database drop
	python3 -m src.database build

pip:
	pip3 install -r requirements.txt

test:
	@sed -i '' 's/^testing=.*/testing=True/' .env
	make setup_db
	python3 -m pytest tests/routes/
	@sed -i '' 's/^testing=.*/testing=False/' .env
