import logging
import os
import sys
import requests
import pandas as pd
from sqlalchemy import create_engine
import datetime
from src import models, schemas
from src.database import get_db
from src.models import TrendingArticle
from src.config import settings 
from textblob import TextBlob

restricted_domains = [
    "bloomberg.com",
    "cnbc.com",
    "yahoo.com",
    "biztoc.com",
    "businessinsider.com",
    "foxbusiness.com",
    "fortune.com",
    "marketwatch.com",
    "independent.co.uk",
    "independent.ie",
    "cnn.com",
    "usatoday.com",
    "wsj.com",
]

class NewsAPI:
    def __init__(self, api_key):
        self.api_key = api_key
        self.base_url = "https://newsapi.org/v2"
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create a log folder if it doesn't exist
        log_folder = "src/classes/news_article/logs"
        if not os.path.exists(log_folder):
            os.makedirs(log_folder)

        current_time = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        log_file = f"news_article_{current_time}.log"
        logging.basicConfig(filename=os.path.join(log_folder, log_file), level=logging.INFO,
                            format='%(asctime)s:%(levelname)s:%(message)s')

        logging.info(f"Initialized NewsAPI class at {current_time}")
        
        


    def get_top_headlines(self, category=None, sources=None, keywords=None, country="us"):
        """
        Retrieves the top headlines from the NewsAPI based on the specified parameters.
        :param category: The category of the headlines.
        :param sources: The sources of the headlines.
        :param keywords: The keywords to search for in the headlines.
        :return: JSON data of the top headlines from the NewsAPI.
        """

        endpoint = "/top-headlines"
        params = {
            "apiKey": self.api_key
        }
        if category:
            params["category"] = category
        if sources:
            params["sources"] = ",".join(sources)
        if keywords:
            params["q"] = keywords
        if country:
            params["country"] = country
        url = self.base_url + endpoint

        try:
            response = requests.get(url, params=params)
            response.raise_for_status()
            data = response.json()
            self.logger.info(f"Successfully retrieved top headlines for category={category}, sources={sources}, keywords={keywords}, country={country}")
            return data
        except requests.exceptions.HTTPError as error:
            self.logger.error(f"Error retrieving top headlines: {error}")
            sys.exit(1)

    def get_headlines(self, keywords=None, from_date=None, to_date=None):
        """
        Retrieves the top headlines from the NewsAPI based on the specified parameters.
        :param keywords: The keywords to search for in the headlines.
        :param from_date: The start date of the time range for the news articles.
        :param to_date: The end date of the time range for the news articles.
        :return: JSON data of the top headlines from the NewsAPI.
        """

        endpoint = "/everything"
        params = {
            "apiKey": self.api_key,
            "language": "en",
            "domains": ",".join(restricted_domains),
            "sortBy": "relevancy",
        }
        if keywords:
            params["q"] = keywords
        if from_date:
            params["from"] = from_date
        if to_date:
            params["to"] = to_date

        url = self.base_url + endpoint

        try:
            response = requests.get(url, params=params)
            response.raise_for_status()
            data = response.json()
            self.logger.info(f"Successfully retrieved headlines for  keywords={keywords}")
            return data
        except requests.exceptions.HTTPError as error:
            self.logger.error(f"Error retrieving top headlines: {error}")
            sys.exit(1)


    def preprocess_json(self, json_data):
        """
        Preprocesses the JSON data from the NewsAPI to extract the relevant information and convert it to a pandas DataFrame.
        :param json_data: The JSON data from the NewsAPI.
        :return: The preprocessed data in the form of a pandas DataFrame.
        """
        try:
            # Extract the articles from the JSON data
            articles = json_data["articles"]
            self.logger.info(f"Extracted articles from JSON data: {articles}")

            # Create an empty list to store the data
            data = []

            #Sentiment Threshold
            threshold = 0.1
            overwhelming_threshold = 0.5

            # Loop over the articles to extract the relevant information
            for article in articles:
                if article["description"] and article["source"] and article["content"]:
                    # Perform sentiment analysis on the article text
                    text = article["title"] + " " + article["description"] + " " + article["content"]
                    blob = TextBlob(text)
                    sentiment_score = blob.sentiment.polarity
                    sentiment_score = round(sentiment_score,2)

                    
                    # Categorize the sentiment score
                    if sentiment_score >= overwhelming_threshold:
                        sentiment_category = "Article Sentiment: Overwhelmingly Positive"

                    elif sentiment_score >= threshold:
                        sentiment_category = "Article Sentiment: Positive"

                    elif sentiment_score <= -overwhelming_threshold:
                        sentiment_category = "Article Sentiment: Overwhelmingly Negative"

                    elif sentiment_score <= -threshold:
                        sentiment_category = "Article Sentiment: Negative"

                    

                    else:
                        sentiment_category = "Article Sentiment: Neutral"


                    data.append({
                        "source": article["source"]["name"],
                        "author": article["author"],
                        "title": article["title"],
                        "description": article["description"],
                        "publishedAt": article["publishedAt"],
                        "content": article["content"],
                        "url": article["url"],
                        "urlToImage": article["urlToImage"],
                        "sentiment_score": sentiment_score,
                        "sentiment_category": sentiment_category
                    })

            # Convert the data to a pandas DataFrame
            df = pd.DataFrame(data)

            df = df.drop_duplicates(subset=["title"])

            # Return the DataFrame
            return df

        except Exception as e:
            self.logger.error(f"Error occured while preprocessing the json data: {e}")
            raise
