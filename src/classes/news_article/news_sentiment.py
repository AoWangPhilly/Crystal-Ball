import os
import nltk
from nltk.sentiment import SentimentIntensityAnalyzer
from src.classes.news_article.news_article import NewsAPI
import logging
import datetime

nltk.download('vader_lexicon')
news_api_key = os.getenv("NEWS_API_KEY")

class NewsSentiment:
    def __init__(self, news_api_key: str):
        self.news_api = NewsAPI(news_api_key)
        self.sentiment_analyzer = SentimentIntensityAnalyzer()
        self.results = []
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create a log folder if it doesn't exist
        log_folder = "src/classes/news_article/sentiment/logs"
        if not os.path.exists(log_folder):
            os.makedirs(log_folder)

        current_time = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        log_file = f"news_sentiment_{current_time}.log"
        logging.basicConfig(filename=os.path.join(log_folder, log_file), level=logging.INFO,
                            format='%(asctime)s:%(levelname)s:%(message)s')

        logging.info(f"Initialized NewsSentiment class at {current_time}")
        
    def analyze_sentiment(self, text: str):
        try:
            sentiment = self.sentiment_analyzer.polarity_scores(text)["compound"]
            logging.info(f"Analyzed news sentiment: {sentiment}")
            return sentiment
        except Exception as e:
            logging.error(str(e))
            raise

    def categorize_sentiment(self, sentiment: float):
        try:
            # Decide sentiment as positive, negative and neutral
            if sentiment >= 0.05:
                logging.info(f"Sentiment score is positive: {sentiment}")
                return("Positive")

            elif sentiment <= -0.05:
                logging.info(f"Sentiment score is negative: {sentiment}")
                return("Negative")

            else:
                logging.info(f"Sentiment score is neutral: {sentiment}")
                return("Neutral")
        except Exception as e:
            logging.error(str(e))
            raise

    def process_articles(self, articles):
        try:
            for article in articles:
                sentiment = self.analyze_sentiment(article.description)
                sentiment_category = self.categorize_sentiment(sentiment)
                self.results.append({"title": article.title, "description": article.description, "sentiment": sentiment_category})
                logging.info(f"Processed article: {article.title}")
        except Exception as e:
            logging.error(str(e))
            raise


if __name__ == "__main__":
    news_api_key = os.getenv("NEWS_API_KEY")
    news_api = NewsAPI(news_api_key)
    news_articles = news_api.get_headlines()
    sentiment_analyzer = NewsSentiment()

    for article in news_articles:
        sentiment = sentiment_analyzer.analyze_sentiment(article["description"])
        sentiment_category = sentiment_analyzer.categorize_sentiment(sentiment)
        article["sentiment"] = sentiment
        article["sentiment_category"] = sentiment_category

    print(news_articles)
