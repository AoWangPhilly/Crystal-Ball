import requests
import pandas as pd
import logging
import os
import datetime
from src.classes.news_article.news_article import NewsAPI
from src.classes.wishlist.wishlist import Wishlist
import sys


class NewsRecommender:
    def __init__(self, api_key):
        self.api_key = api_key
        self.base_url = "https://newsapi.org/v2"
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Create a log folder if it doesn't exist
        log_folder = "src/classes/recommended_articles/logs"
        if not os.path.exists(log_folder):
            os.makedirs(log_folder)

        current_time = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        log_file = f"recommended_{current_time}.log"
        logging.basicConfig(
            filename=os.path.join(log_folder, log_file),
            level=logging.INFO,
            format="%(asctime)s:%(levelname)s:%(message)s",
        )

        logging.info(f"Initialized StockNewsAPI class at {current_time}")

    def get_top_headlines(
        self, wishlist=None, keywords=None, searchIn=None, domains=None, country="us"
    ):
        """
        Retrieves the top headlines from the NewsAPI based on the specified tickers.
        :param wishlist: An instance of the Wishlist class containing the user's stock ticker wishlist.
        :return: JSON data of the top headlines from the NewsAPI.
        """

        endpoint = "/everything"
        params = {
            "apiKey": self.api_key,
            "sortBy": "publishedAt",
            "language": "en",  # set the language to 'en'
            "qInTitle": " OR ".join([f'"{ticker}"' for ticker in wishlist.tickers])
            if wishlist
            else None,
            "q": keywords,
            "pageSize": 100,
            "page": 1,
        }
        tickers = None
        if wishlist:
            tickers = wishlist.tickers
            if tickers:
                params["q"] = " OR ".join([f'"{ticker}"' for ticker in tickers])
        if keywords:
            params["q"] = keywords
        if searchIn:
            params["searchIn"] = searchIn
        if domains:
            params["domains"] = domains
        url = self.base_url + endpoint

        try:
            response = requests.get(url, params=params)
            response.raise_for_status()
            data = response.json()
            self.logger.info(
                f"Successfully retrieved top headlines for tickers={tickers} or keywords={keywords}"
            )
            return data
        except requests.exceptions.HTTPError as error:
            self.logger.error(f"Error retrieving top headlines: {error}")
            sys.exit(1)

    def preprocess_json(self, json_data):
        """
        Preprocesses the JSON data from the NewsAPI to extract the relevant information and convert it to a pandas DataFrame.
        :param json_data: The JSON data from the NewsAPI.
        :return: The preprocessed data in the form of a pandas DataFrame.
        """
        try:
            # Extract the articles from the JSON data
            articles = json_data["articles"]
            self.logger.info(f"Extracted articles from JSON data: {articles}")

            # Create an empty list to store the data
            data = []

            # Loop over the articles to extract the relevant information
            for article in articles:
                if article["description"] and article["source"] and article["content"]:
                    data.append(
                        {
                            "source": article["source"]["name"],
                            "author": article["author"],
                            "title": article["title"],
                            "description": article["description"],
                            "publishedAt": article["publishedAt"],
                            "content": article["content"],
                            "url": article["url"],
                            "urlToImage": article["urlToImage"],
                        }
                    )

            # Convert the data to a pandas DataFrame
            df = pd.DataFrame(data)

            df = df.drop_duplicates(subset=["title"])

            # Return the DataFrame
            return df

        except Exception as e:
            self.logger.error(f"Error occurred while preprocessing the JSON data: {e}")
            raise

    def recommend(self, wishlist=None):
        """
        Recommends articles based on the user's wishlist.
        :param wishlist: An instance of the Wishlist class containing the user's stock ticker wishlist.
        :return: A pandas DataFrame containing the recommended articles.
        """
        # Get top headlines for tickers in the user's wishlist
        news_data = self.get_top_headlines(wishlist=wishlist)

        # Preprocess the JSON data into a pandas DataFrame
        df = self.preprocess_json(news_data)

        return df
