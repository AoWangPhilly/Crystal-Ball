import os
import logging
import datetime

class Wishlist:
    def __init__(self):
        self.name = ""
        self.tickers = []
        log_dir = 'src/classes/wishlist/logs'
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        current_time = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        log_file = f"wishlist_{current_time}.log"
        logging.basicConfig(filename=os.path.join(log_dir, log_file), level=logging.INFO,
                            format='%(asctime)s:%(levelname)s:%(message)s')
    
    def set_name(self, name):
        self.name = name
        logging.info(f'Wishlist name set to {name}')
        
    def add_ticker(self, ticker):
        self.tickers.append(ticker)
        logging.info(f'Added ticker symbol {ticker} to wishlist {self.name}')
        
    def remove_ticker(self, ticker):
        try:
            self.tickers.remove(ticker)
            logging.info(f'Removed ticker symbol {ticker} from wishlist {self.name}')
        except ValueError:
            logging.error(f'Ticker symbol {ticker} not found in wishlist {self.name}')
            print(f'Error: Ticker symbol "{ticker}" not found in the list.')
