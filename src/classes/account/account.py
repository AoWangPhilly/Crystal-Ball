import hashlib
import logging
from sqlalchemy import and_
import os
from datetime import datetime
from pydantic import ValidationError
from src import models, schemas
from src.database import get_db
from src.models import Account as act
from typing import Dict
from typing import List


import logging
import hashlib
from typing import Dict, List

from pydantic import ValidationError

from src import models, schemas
from src.database import get_db
from src.models import Account as act


class Account():
    # Initialize the logger
    log_file = f'account_{datetime.now().strftime("%Y_%m_%d")}.log'
    log_folder = 'src/classes/account/logs'
    os.makedirs(log_folder, exist_ok=True)
    log_file = os.path.join(log_folder, log_file)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    file_handler = logging.FileHandler(log_file)
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    @staticmethod
    def create_account(account_data: Dict[str, str]) -> act:
        try:
            account = schemas.AccountCreate(**account_data)
            account.password = hashlib.sha256(account.password.encode("utf-8")).hexdigest()
            new_account = models.Account(**account.dict())
            with get_db() as db:
                db.add(new_account)
                db.commit()
                db.refresh(new_account)
            Account.logger.info(f"Created account with email {new_account.email}")
            return new_account
        except ValidationError as e:
            Account.logger.error(f"Failed to create account due to validation error: {e.json()}")
            raise

    @staticmethod
    def delete_account(email: str):
        with get_db() as db:
            account = db.query(act).filter(act.email == email).first()
        if account:
            db.delete(account)
            db.commit()
            Account.logger.info(f"Deleted account with email {email}")
        else:
            Account.logger.warning(f"No account found with email {email}")

    @staticmethod
    def get_accounts() -> List[act]:
        with get_db() as db:
            accounts = db.query(act).all()
        Account.logger.info(f"Retrieved {len(accounts)} accounts from database")
        return accounts

    @staticmethod
    def print_all_accounts():
        accounts = Account.get_accounts()
        for account in accounts:
            print(account)
        Account.logger.info(f"Printed {len(accounts)} accounts")




'''
class Account:
    def __init__(self, email, password, db_session):
        self.email = email.lower()
        self.password = self.__hash_password(password)
        self.db_session = db_session

        # Initialize the logger
        log_file = f'account_{datetime.now().strftime("%Y_%m_%d")}.log'
        log_folder = 'src/classes/account/logs'
        os.makedirs(log_folder, exist_ok=True)
        log_file = os.path.join(log_folder, log_file)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        file_handler = logging.FileHandler(log_file)
        file_handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)
        

    def __hash_password(self, password):
        # Hashing the password using the SHA-256 algorithm
        logging.info(f"Password successfully hashed.")
        return hashlib.sha256(password.encode()).hexdigest()

    def register(account_data: Dict[str, str]) -> act:
        try:
            account = schemas.AccountCreate(**account_data)
            account.password = hashlib.sha256(account.password.encode("utf-8")).hexdigest()
            new_account = models.Account(**account.dict())
            with get_db() as db:
                db.add(new_account)
                db.commit()
                db.refresh(new_account)
            logging.info(f"Account successfully created.")
            return new_account
        except ValidationError as e:
            logging.error(f"{e}")

    
    def delete_account(email: str):
        with get_db() as db:
            account = db.query(Account).filter(Account.email == email).first()
        if account:
            db.delete(account)
            db.commit()


    def get_accounts() -> List[act]:
        with get_db() as db:
            accounts = db.query(Account).all()
        return accounts
'''
    