import requests


def get_website(symbol: str) -> str:
    URL = f"https://query2.finance.yahoo.com/v10/finance/quoteSummary/{symbol}?modules=defaultKeyStatistics%2CassetProfile%2CtopHoldings%2CfundPerformance%2CfundProfile%2CesgScores&ssl=true"
    res = requests.get(
        URL,
        headers={
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"
        },
    )
    data = res.json()
    try:
        return data["quoteSummary"]["result"][0]["assetProfile"]["website"]
    except:
        return ""


def get_logo(symbol: str) -> str:
    website = get_website(symbol)
    return f"https://logo.clearbit.com/{website}"


if __name__ == "__main__":
    print(get_logo("AAPL"))
