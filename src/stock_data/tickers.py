from typing import List
from os.path import join
from yahoo_fin import stock_info as si
import pandas as pd


def load_stock_ticker_symbols() -> List[str]:
    sp500 = si.tickers_sp500()
    nasdaq = si.tickers_nasdaq()
    dow = si.tickers_dow()
    other = si.tickers_other()

    all_stocks = list(
        filter(
            lambda x: x and "." not in x and "$" not in x,
            set(sp500 + nasdaq + dow + other),
        )
    )
    all_stocks.sort()
    return all_stocks


def get_nasdaq() -> pd.DataFrame:
    fname = join("src", "stock_data", "nasdaq_screener.csv")
    nasdaq_ticker_options_df = pd.read_csv(fname, dtype=str)
    return nasdaq_ticker_options_df


def get_nyse() -> pd.DataFrame:
    fname = join("src", "stock_data", "nyse_screener.csv")
    nyse_ticker_options_df = pd.read_csv(fname, dtype=str)
    return nyse_ticker_options_df


def get_ticker_name_and_symbol() -> pd.DataFrame:
    nasdaq_ticker_options_df = get_nasdaq()
    nyse_ticker_options_df = get_nyse()

    ticker_options_df = pd.concat(
        [nasdaq_ticker_options_df, nyse_ticker_options_df]
    ).drop_duplicates(subset="Symbol", keep="first")

    ticker_options_df = ticker_options_df.dropna(subset="Symbol")

    ticker_options_df["Symbol_Name"] = (
        ticker_options_df["Symbol"] + ": " + ticker_options_df["Name"]
    )
    return ticker_options_df


def find_ticker_name(ticker: str) -> str:
    ticker_options_df = get_ticker_name_and_symbol()
    ticker_options_df = ticker_options_df.set_index("Symbol")
    ticker_name = ticker_options_df.loc[ticker, "Symbol_Name"]
    return ticker_name


if __name__ == "__main__":
    print(find_ticker_name("TSLA"))
