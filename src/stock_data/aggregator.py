import datetime
from typing import List, Union

import pandas as pd
import yfinance as yf


class StockDataAggregator:
    def __init__(
            self, stock_symbols: Union[str, List[str]],
            start_time: Union[datetime.datetime, str] = None,
            end_time: Union[datetime.datetime, str] = None
    ) -> None:
        self.stock_symbols = stock_symbols
        self.start_time = start_time
        self.end_time = end_time

    def get_stock_symbols(self) -> List[str]:
        return self.stock_symbols

    def set_stock_symbols(self, stock_symbols: List[str]) -> None:
        self.stock_symbols = stock_symbols

    def get_start_time(self) -> datetime.datetime:
        return self.start_time

    def set_start_time(self, start_time: datetime.datetime) -> None:
        self.start_time = start_time

    def get_end_time(self) -> datetime.datetime:
        return self.end_time

    def set_end_time(self, end_time: datetime.datetime) -> None:
        self.end_time = end_time

    def get_historical_stock_price(self) -> pd.DataFrame:
        if self.start_time and self.end_time:
            return yf.download(
                tickers=self.stock_symbols,
                start=self.start_time,
                end=self.end_time,
                group_by="ticker"
            )
        if self.end_time:
            return yf.download(
                tickers=self.stock_symbols,
                end=self.end_time,
                group_by="ticker"
            )

        return yf.download(
            tickers=self.stock_symbols,
            period="max",
            group_by="ticker"
        )

    def get_financial_statements(self) -> pd.DataFrame:
        pass


if __name__ == "__main__":
    stock_symbols = ["TSLA"]
    agg = StockDataAggregator(stock_symbols=stock_symbols)

    # Gets all stock prices since IPO
    print(agg.get_historical_stock_price())

    agg.set_start_time(datetime.datetime(year=2022, month=11, day=1))
    agg.set_end_time(datetime.datetime.now())
    print(agg.get_historical_stock_price())
