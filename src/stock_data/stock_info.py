import yahoo_fin.stock_info as si
import logging
import os
import datetime
import pandas as pd
import requests


def get_info_backup(
    ticker: str,
    headers={
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"
    },
):
    """Inputs: @ticker

    Returns a dictionary containing over 70 elements corresponding to the
    input ticker, including company name, book value, moving average data,
    pre-market / post-market price (when applicable), and more."""
    try:
        site = "https://query1.finance.yahoo.com/v6/finance/quote?symbols=" + ticker

        resp = requests.get(
            site,
            headers={
                "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"
            },
        )

        if not resp.ok:
            raise AssertionError(
                """Invalid response from server.  Check if ticker is
                                valid."""
            )

        json_result = resp.json()
        info = json_result["quoteResponse"]["result"]

        return info[0]
    except:
        return None


class StockInfo:
    def __init__(self, ticker):
        self.ticker = ticker

        # Create a logs directory if it doesn't already exist
        logs_dir = "logs/"
        if not os.path.exists(logs_dir):
            os.makedirs(logs_dir)

        # Configure logging to save to a file
        log_file = os.path.join(
            logs_dir,
            "stock_info_{}.log".format(
                datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            ),
        )
        logging.basicConfig(
            filename=log_file,
            level=logging.INFO,
            format="%(asctime)s [%(levelname)s] %(message)s",
        )
        logging.info("Successfully initialized StockInfo for: {}".format(self.ticker))

    # Methods to Get Stock Info Data:

    def get_info(self):
        try:
            info = si.get_quote_data(self.ticker)
            logging.info("Retrieved stock info for {}: {}".format(self.ticker, info))
            return info
        except Exception as e:
            logging.exception(
                f"An error occurred while retrieving stock info for {self.ticker}: {e}"
            )
            info = get_info_backup(self.ticker)
            return info

    def get_history(self, period="1y", interval="1d"):
        try:
            history = si.get_data(
                self.ticker,
                start_date=None,
                end_date=None,
                index_as_date=True,
                interval=interval,
            )
            logging.info(
                "Retrieved stock history for {} with period={}, interval={}".format(
                    self.ticker, period, interval
                )
            )
            return history
        except Exception as e:
            logging.exception(
                "An error occurred while retrieving stock history for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_dividends(self):
        try:
            dividends = si.get_dividends(self.ticker)
            logging.info(
                "Retrieved stock dividends for {}: {}".format(self.ticker, dividends)
            )
            return dividends
        except Exception as e:
            logging.exception(
                "An error occurred while retrieving stock dividends for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_splits(self):
        try:
            splits = si.get_splits(self.ticker)
            logging.info(
                "Retrieved stock splits for {}: {}".format(self.ticker, splits)
            )
            return splits
        except Exception as e:
            logging.exception(
                "An error occurred while retrieving stock splits for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_recommendations(self):
        try:
            recommendations = si.get_analysts_info(self.ticker)
            logging.info(
                "Retrieved stock recommendations for {}: {}".format(
                    self.ticker, recommendations
                )
            )
            return recommendations
        except Exception as e:
            logging.exception(
                "An error occurred while retrieving stock recommendations for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_earnings(self):
        try:
            earnings_history = si.get_earnings(self.ticker)
            logging.info(
                "Retrieved stock earnings for {}: {}".format(
                    self.ticker, earnings_history
                )
            )
            return earnings_history
        except Exception as e:
            logging.exception(
                "An error occurred while retrieving stock earnings history for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_live_price(self):
        try:
            live_price = si.get_live_price(self.ticker)
            logging.info("Retrieved live stock price for {} ".format(self.ticker))
            return live_price
        except Exception as e:
            logging.exception(
                "An error occurred while retrieving live stock price for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_valuation_stats(self):
        try:
            valuation_stats = si.get_stats_valuation(self.ticker)
            logging.info("Retrieved valuation stats for {} ".format(self.ticker))
            return valuation_stats
        except Exception as e:
            logging.exception(
                "An error occurred while retrieving valuation stats for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_balance_sheet(self):
        try:
            financials = si.get_financials(self.ticker)
            balance_sheet = financials["balance_sheet"]
            logging.info(f"Successfully retrieved balance sheet info for {self.ticker}")
            return balance_sheet
        except Exception as e:
            error_msg = f"An error occurred while getting balance sheet info for {self.ticker}: {e}"
            logging.error(error_msg)
            raise Exception(error_msg)

    def get_income_statement(self):
        try:
            financials = si.get_financials(self.ticker)
            income_statement = financials["income_statement"]
            logging.info(
                f"Successfully retrieved income statement info for {self.ticker}"
            )
            return income_statement
        except Exception as e:
            error_msg = f"An error occurred while getting income statement info for {self.ticker}: {e}"
            logging.error(error_msg)
            raise Exception(error_msg)

    def get_cash_flow_statement(self):
        try:
            financials = si.get_financials(self.ticker)
            cash_flow_statement = financials["cash_flow"]
            logging.info(
                f"Successfully retrieved cash flow statement info for {self.ticker}"
            )
            return cash_flow_statement
        except Exception as e:
            error_msg = f"An error occurred while getting cash flow statement info for {self.ticker}: {e}"
            logging.error(error_msg)
            raise Exception(error_msg)

    # Methods to Convert Stock Info Data into Tables:

    def get_info_table(self):
        try:
            info = self.get_info()
            if info is not None:
                table = pd.DataFrame.from_dict(info, orient="index", columns=["Value"])
                table.index.name = "Attribute"
                table.reset_index(inplace=True)
                logging.info(
                    "Retrieved info table for {}:\n{}".format(self.ticker, table)
                )
                return table
            else:
                logging.info("No info available for {}".format(self.ticker))
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting info table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_history_table(self, period="1y", interval="1d"):
        try:
            history = self.get_history(period=period, interval=interval)
            if history is not None:
                table = history.reset_index()
                logging.info(
                    "Retrieved history table for {} with period={} and interval={}: \n{}".format(
                        self.ticker, period, interval, table
                    )
                )
                return table
            else:
                logging.info(
                    "No history available for {} with period={} and interval={}".format(
                        self.ticker, period, interval
                    )
                )
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting history table for {} with period={} and interval={}: {}".format(
                    self.ticker, period, interval, str(e)
                )
            )
            return None

    def get_dividends_table(self):
        try:
            dividends = self.get_dividends()
            if dividends is not None:
                table = dividends.reset_index()
                logging.info(
                    "Retrieved dividends table for {}:\n{}".format(self.ticker, table)
                )
                return table
            else:
                logging.info("No dividends available for {}".format(self.ticker))
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting dividends table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_splits_table(self):
        try:
            splits = self.get_splits()
            if splits is not None:
                table = splits.reset_index()
                logging.info(
                    "Retrieved splits table for {}:\n{}".format(self.ticker, table)
                )
                return table
            else:
                logging.info("No splits available for {}".format(self.ticker))
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting splits table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_recommendations_table(self):
        try:
            recommendations = self.get_recommendations()
            if recommendations is not None:
                table = pd.DataFrame.from_dict(
                    recommendations, orient="index", columns=["Value"]
                )
                table.index.name = "Attribute"
                table.reset_index(inplace=True)
                logging.info(
                    "Retrieved recommendations table for {}:\n{}".format(
                        self.ticker, table
                    )
                )
                return table
            else:
                logging.info("No recommendations available for {}".format(self.ticker))
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting recommendations table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_earnings_table(self):
        try:
            earnings = self.get_earnings()
            if earnings is not None:
                table = earnings.reset_index()
                logging.info(
                    "Retrieved earnings table for {}:\n{}".format(self.ticker, table)
                )
                return table
            else:
                logging.info("No earnings available for {}".format(self.ticker))
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting earnings table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_valuation_stats_table(self):
        try:
            valuation_stats = self.get_valuation_stats()
            if valuation_stats is not None:
                table = pd.DataFrame.from_dict(
                    valuation_stats, orient="index", columns=["Value"]
                )
                table.index.name = "Attribute"
                table.reset_index(inplace=True)
                logging.info(
                    "Retrieved valuation stats table for {}:\n{}".format(
                        self.ticker, table
                    )
                )
                return table
            else:
                logging.info("No valuation stats available for {}".format(self.ticker))
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting valuation stats table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_balance_sheet_table(self):
        try:
            balance_sheet = self.get_balance_sheet()
            if balance_sheet is not None:
                table = balance_sheet.reset_index()
                logging.info(
                    "Retrieved balance sheet table for {}:\n{}".format(
                        self.ticker, table
                    )
                )
                return table
            else:
                logging.info("No balance sheet available for {}".format(self.ticker))
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting balance sheet table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_income_statement_table(self):
        try:
            income_statement = self.get_income_statement()
            if income_statement is not None:
                table = income_statement.reset_index()
                logging.info(
                    "Retrieved income statement table for {}:\n{}".format(
                        self.ticker, table
                    )
                )
                return table
            else:
                logging.info("No income statement available for {}".format(self.ticker))
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting income statement table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None

    def get_cash_flow_statement_table(self):
        try:
            cash_flow_statement = self.get_cash_flow_statement()
            if cash_flow_statement is not None:
                table = cash_flow_statement.reset_index()
                logging.info(
                    "Retrieved cash flow statement table for {}:\n{}".format(
                        self.ticker, table
                    )
                )
                return table
            else:
                logging.info(
                    "No cash flow statement available for {}".format(self.ticker)
                )
                return None
        except Exception as e:
            logging.exception(
                "An error occurred while getting cash flow statement table for {}: {}".format(
                    self.ticker, str(e)
                )
            )
            return None
