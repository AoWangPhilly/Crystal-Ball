from passlib.context import CryptContext
from datetime import timedelta
import datetime as dt
import pandas_market_calendars as mcal
import uuid

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash(password: str):
    return pwd_context.hash(password)


def verify(plain_password, hashed_password) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def get_n_trading_days(start: dt.datetime, end: dt.datetime):
    nyse = mcal.get_calendar("NYSE")
    early = nyse.schedule(start, end)
    return list(map(lambda x: x.date(), mcal.date_range(early, frequency="1D")))


def generate_random_id():
    return str(uuid.uuid4())


def is_valid_uuid(uuid_to_test):
    """
    Check if uuid_to_test is a valid UUID.

     Parameters
    ----------
    uuid_to_test : str
    version : {1, 2, 3, 4}

     Returns
    -------
    `True` if uuid_to_test is a valid UUID, otherwise `False`.

     Examples
    --------
    >>> is_valid_uuid('c9bf9e57-1685-4c89-bafb-ff5af830be8a')
    True
    >>> is_valid_uuid('c9bf9e58')
    False
    """

    try:
        uuid_obj = uuid.UUID(uuid_to_test)
    except ValueError:
        return False
    return str(uuid_obj) == uuid_to_test
