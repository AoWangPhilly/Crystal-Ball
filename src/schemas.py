from datetime import datetime
from typing import Optional, List

from pydantic import (
    BaseModel,
    validator,
    PositiveInt,
    PositiveFloat,
    NonNegativeInt,
)
from email_validator import validate_email, EmailNotValidError

from pydantic.networks import EmailStr
from src.stock_data.tickers import load_stock_ticker_symbols
from src.utils import is_valid_uuid


class AccountCreate(BaseModel):
    first_name: str
    email: str
    password: str
    is_admin: Optional[bool] = False

    @validator("first_name")
    def name_must_not_contain_digits(cls, v):
        if any(char.isdigit() for char in v):
            raise ValueError("Name must not contain digits. Please enter a valid name.")
        return v

    @validator("first_name")
    def name_is_not_empty(cls, v):
        if not v:
            raise ValueError("Name must not be empty. Please enter a valid name.")
        return v

    @validator("first_name")
    def name_is_not_too_long(cls, v):
        if len(v) > 50:
            raise ValueError(
                "Name must be at most 50 characters. Please enter a valid name."
            )
        return v

    @validator("first_name")
    def name_is_not_too_short(cls, v):
        if len(v) < 3:
            raise ValueError(
                "Name must be at least 3 characters. Please enter a valid name."
            )
        return v

    @validator("email")
    def email_is_not_empty(cls, v):
        if not v:
            raise ValueError("Email must not be empty. Please enter a valid email.")
        return v

    @validator("email")
    def email_is_valid(cls, v):
        try:
            validation = validate_email(v, check_deliverability=False)
            return validation.email
        except EmailNotValidError as e:
            raise ValueError(
                f"{v} is not a valid email address. Please enter a valid name."
            )

    @validator("password")
    def password_not_empty(cls, v):
        if not v:
            raise ValueError(
                "Password must not be empty. Please enter a valid password."
            )
        return v

    @validator("password")
    def password_length(cls, v):
        if len(v) < 8:
            raise ValueError(
                "Password must be at least 8 characters. Please enter a valid password."
            )
        if len(v) > 100:
            raise ValueError(
                "Password must be at most 100 characters. Please enter a valid password."
            )
        return v


class AccountUpdate(BaseModel):
    first_name: Optional[str]
    email: Optional[EmailStr]
    password: Optional[str] = None
    is_admin: Optional[bool] = False

    @validator("first_name")
    def name_length(cls, v):
        if len(v) < 3:
            raise ValueError("first name must be at least 3 characters")
        if len(v) > 50:
            raise ValueError("first name must be at most 50 characters")
        return v

    @validator("password")
    def password_length(cls, v):
        if len(v) < 8:
            raise ValueError("password must be at least 8 characters")
        if len(v) > 100:
            raise ValueError("password must be at most 100 characters")
        return v


class NameChange(BaseModel):
    first_name: str

    @validator("first_name")
    def name_length(cls, v):
        if len(v) < 3:
            raise ValueError("first name must be at least 3 characters")
        if len(v) > 50:
            raise ValueError("first name must be at most 50 characters")
        return v


class EmailChange(BaseModel):
    email: EmailStr


class PasswordChange(BaseModel):
    old_password: str
    new_password: str
    confirm_new_password: str

    @validator("new_password")
    def password_not_empty(cls, v):
        if not v:
            raise ValueError("password must not be empty")
        return v

    @validator("new_password")
    def password_length(cls, v):
        if len(v) < 8:
            raise ValueError("password must be at least 8 characters")
        if len(v) > 100:
            raise ValueError("password must be at most 100 characters")
        return v


class AccountOut(BaseModel):
    id: str
    first_name: str
    email: EmailStr
    created_at: datetime
    is_admin: bool

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None


class PredictorModelCreate(BaseModel):
    account_id: str
    ticker_symbol: str
    days_to_predict: PositiveInt
    number_of_simulations: PositiveInt
    start_date: datetime
    end_date: datetime
    seed: NonNegativeInt
    predicted_history: List[PositiveInt]
    duration: PositiveFloat

    @validator("account_id")
    def valid_account_id(cls, v):
        if not is_valid_uuid(v):
            raise ValueError(f"{v} is not a valid account ID")
        return v

    @validator("ticker_symbol")
    def ticker_symbol_exists(cls, v):
        tickers = load_stock_ticker_symbols()
        if v not in tickers:
            raise ValueError(f"{v} is not a valid ticker symbol")
        return v


class PredictorModelOut(PredictorModelCreate):
    id: str
    timestamp: datetime

    class Config:
        orm_mode = True


class WishlistCreate(BaseModel):
    name: str

    @validator("name")
    def name_length(cls, v):
        if len(v) < 3:
            raise ValueError("name must be at least 3 characters")
        if len(v) > 50:
            raise ValueError("name must be at most 50 characters")
        return v


class WishlistOut(BaseModel):
    id: str
    account_id: str
    name: str
    stock_symbols: List[str]
    timestamp: datetime

    class Config:
        orm_mode = True


class AddStockToWishlist(BaseModel):
    ticker_symbol: str
    wishlist_id: str
