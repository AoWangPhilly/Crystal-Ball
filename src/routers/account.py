from typing import List
import base64

from fastapi import Response, status, APIRouter, HTTPException
from fastapi.params import Depends
from sqlalchemy.orm.session import Session

from src import models, schemas
from src.database import get_db
from src.models import Account
from src.oath2 import get_current_user
from src.utils import hash, verify, generate_random_id
from src.database import r

router = APIRouter(prefix="/accounts", tags=["Accounts"])


@router.get("/", response_model=List[schemas.AccountOut])
async def get_accounts(db: Session = Depends(get_db)):
    """Returns a list of all user accounts"""
    accounts = db.query(Account).all()
    return accounts


@router.get("/{id}", response_model=schemas.AccountOut)
async def get_account_by_id(id: str, db: Session = Depends(get_db)):
    """Returns the details of a user account identified by the ID."""
    account = db.query(Account).get(id)

    # Account doesn't exist
    if not account:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"{id} doesn't exist",
        )
    return account


@router.post(
    "/", status_code=status.HTTP_201_CREATED, response_model=schemas.AccountOut
)
async def create_account(
    account: schemas.AccountCreate,
    db: Session = Depends(get_db),
) -> Account:
    """Creates a new user account with the provided data"""

    account_id = generate_random_id()
    account.email = account.email.lower().strip()

    is_account_taken: bool = (
        db.query(Account).filter(Account.email == account.email).first()
    )

    if is_account_taken:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"{account.email} is taken"
        )

    # Hash password
    account.password = hash(account.password)

    # Add new account
    new_account = models.Account(**account.dict(), id=account_id)
    db.add(new_account)
    db.commit()
    db.refresh(new_account)

    # Set default profile pic
    with open("assets/images/user-icon.png", "rb") as f:
        img = base64.b64encode(f.read())

    r.set(f"{account_id}-profile-pic", f"data:image/png;base64,{img.decode()}")

    return new_account


@router.put("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def update_account(
    id: str,
    account_update: schemas.AccountUpdate,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    """
    Updates a user account with the provided data.
    Only admin users are allowed to update accounts.
    """
    if not get_current_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Need admin permissions to update",
        )

    account_query = db.query(Account).filter(Account.id == id)
    account = account_query.first()

    if not account:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"{id} doesn't exist"
        )
    updated_info = {}

    if account_update.first_name:
        updated_info["first_name"] = account_update.first_name

    if account_update.email:
        updated_info["email"] = account_update.email.lower().strip()

        existing_account: bool = (
            db.query(Account).filter(Account.email == account_update.email).first()
        )

        # Raise exception if email exists and belongs to another account
        if existing_account and account != existing_account:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"{account_update.email} is taken",
            )

        account.email = account_update.email

    if account_update.password:
        updated_info["password"] = hash(account_update.password)

    if account_update.is_admin is not None:
        updated_info["is_admin"] = account_update.is_admin

    account_query.update(updated_info, synchronize_session=False)
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_account(
    id: str,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    if not get_current_user.is_admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Need admin permissions to delete",
        )

    account = db.query(Account).get(id)
    if not account:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"{id} doesn't exist"
        )

    db.delete(account)
    db.commit()


@router.put("/api/profile/name", status_code=status.HTTP_204_NO_CONTENT)
def change_name(
    new_name: schemas.NameChange,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    """Changes the name of the current user"""
    account = db.query(Account).get(get_current_user.id)

    # Check if name is the same
    account.first_name = new_name.first_name.strip()
    db.commit()


@router.put("/api/profile/email", status_code=status.HTTP_204_NO_CONTENT)
def change_email(
    new_email: schemas.EmailChange,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    """Changes the email of the current user"""
    # Check if email is taken
    account_exists = db.query(Account).filter(Account.email == new_email.email).first()
    if account_exists and account_exists.id != get_current_user.id:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Email is already taken",
        )

    account = db.query(Account).get(get_current_user.id)
    account.email = new_email.email.strip()
    db.commit()


@router.put("/api/profile/password", status_code=status.HTTP_204_NO_CONTENT)
def change_password(
    password: schemas.PasswordChange,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    account = db.query(Account).get(get_current_user.id)

    # Check if old password is correct
    if not verify(password.old_password, account.password):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Incorrect password"
        )

    # Check if new password matches confirmed
    if password.new_password != password.confirm_new_password:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Passwords do not match"
        )

    account.password = hash(password.new_password.strip())
    db.commit()
