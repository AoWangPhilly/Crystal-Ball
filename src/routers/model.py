from typing import List

from fastapi import status, APIRouter, HTTPException
from fastapi.params import Depends
from sqlalchemy.orm.session import Session

from src import models, schemas
from src.database import get_db
from src.models import PredictorModel
from src.oath2 import get_current_user
from src.utils import generate_random_id

router = APIRouter(
    prefix="/models",
    tags=["Models"]
)


@router.get("/", response_model=List[schemas.PredictorModelOut])
async def get_models(
        email: str = "",
        ticker_symbol: str = "",
        db: Session = Depends(get_db)
):
    if email:
        models = db \
            .query(PredictorModel) \
            .filter(PredictorModel.email == email)
        return models

    if ticker_symbol:
        models = db \
            .query(PredictorModel) \
            .filter(PredictorModel.ticker_symbol == ticker_symbol)
        return models

    models = db.query(PredictorModel).all()
    return models


@router.get("/{id}", response_model=schemas.PredictorModelOut)
async def get_model_by_id(id: int, db: Session = Depends(get_db)):
    account = db.query(PredictorModel).get(id)

    if not account:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"model {id} doesn't exist"
        )
    return account


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.PredictorModelOut)
async def create_model(
        model: schemas.PredictorModelCreate,
        db: Session = Depends(get_db),
        get_current_user: str = Depends(get_current_user)
) -> PredictorModel:
    model_id = generate_random_id()

    # Add new model
    new_model = models.PredictorModel(**model.dict(), id=model_id)
    db.add(new_model)
    db.commit()
    db.refresh(new_model)
    return new_model
