from typing import List, Union

from fastapi import Response, status, APIRouter, HTTPException
from fastapi.params import Depends
from sqlalchemy.orm.session import Session

from src import models, schemas
from src.database import get_db
from src.models import Wishlist
from src.oath2 import get_current_user
from src.utils import generate_random_id

router = APIRouter(prefix="/api/wishlist", tags=["Wishlist"])


@router.get("/find", response_model=List[schemas.WishlistOut])
async def get_wishlists(wishlist_id: str, db: Session = Depends(get_db)):
    user_wishlists = db.query(Wishlist).filter(Wishlist.id == wishlist_id).all()
    return user_wishlists


@router.delete("/remove/{wishlist_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_wishlist(
    wishlist_id: str,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    # find wishlist by id
    wishlist = db.query(Wishlist).filter(Wishlist.id == wishlist_id).first()

    # check if it belongs to you
    if wishlist.account_id != get_current_user.id:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"Wishlist with id {wishlist_id} does not belong to you",
        )

    # check if wishlist exists
    if not wishlist:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Wishlist with id {wishlist_id} not found",
        )

    # delete wishlist
    db.delete(wishlist)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get("/{account_id}", response_model=List[schemas.WishlistOut])
async def get_user_wishlists(account_id: str, db: Session = Depends(get_db)):
    user_wishlists = db.query(Wishlist).filter(Wishlist.account_id == account_id).all()
    return user_wishlists


@router.post(
    "/", status_code=status.HTTP_201_CREATED, response_model=schemas.WishlistOut
)
async def create_wishlist(
    wishlist: schemas.WishlistCreate,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    wishlist_id = generate_random_id()

    user_wishlists = db.query(Wishlist).filter(
        Wishlist.account_id == get_current_user.id
    )

    special_characters = ["!", "@", "#", "$", "%", "^", "&", "*"]
    if any(sc in special_characters for sc in wishlist.name):
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Wishlist name should not contain invalid special characters: !@#$%^&*",
        )

    for wish in user_wishlists:
        if wish.name.strip().lower() == wishlist.name.strip().lower():
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"{wishlist.name} already exists",
            )

    # Add new wishlist
    new_wishlist = models.Wishlist(
        **wishlist.dict(), id=wishlist_id, account_id=get_current_user.id
    )
    db.add(new_wishlist)
    db.commit()
    db.refresh(new_wishlist)
    print(new_wishlist)

    return new_wishlist


@router.put(
    "/symbol",
    status_code=status.HTTP_204_NO_CONTENT,
)
async def add_stock_to_wishlist(
    wishlist_info: schemas.AddStockToWishlist,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    user_wishlist_query = db.query(Wishlist).filter(
        Wishlist.id == wishlist_info.wishlist_id
    )
    ticker_symbols = user_wishlist_query.first().stock_symbols
    if wishlist_info.ticker_symbol not in ticker_symbols:
        ticker_symbols.append(wishlist_info.ticker_symbol)
    else:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"{user_wishlist_query.first().name} already contains {wishlist_info.ticker_symbol}",
        )

    user_wishlist_query.update(
        {"stock_symbols": ticker_symbols}, synchronize_session=False
    )
    db.commit()


@router.delete(
    "/symbol",
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete_stock_from_wishlist(
    wishlist_info: schemas.AddStockToWishlist,
    db: Session = Depends(get_db),
    get_current_user: str = Depends(get_current_user),
):
    user_wishlist_query = db.query(Wishlist).filter(
        Wishlist.id == wishlist_info.wishlist_id
    )
    ticker_symbols = user_wishlist_query.first().stock_symbols
    print(f"ticker_symbols: {ticker_symbols}")
    if wishlist_info.ticker_symbol not in ticker_symbols:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Wishlist doesn't contain {wishlist_info.ticker_symbol}",
        )
    else:
        ticker_symbols.remove(wishlist_info.ticker_symbol)

    user_wishlist_query.update(
        {"stock_symbols": ticker_symbols}, synchronize_session=False
    )
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)
