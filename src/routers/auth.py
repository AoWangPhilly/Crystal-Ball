import base64

from fastapi import APIRouter, Depends, status, HTTPException
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from src import schemas
from src.database import get_db
from src.models import Account, BlackList
from src.oath2 import create_access_token, get_current_user, get_token_user
from src.utils import hash, verify, generate_random_id
from src.database import r

router = APIRouter(tags=["Authentication"])


@router.post("/signup", response_model=schemas.Token)
def signup(account: schemas.AccountCreate, db: Session = Depends(get_db)):
    account_id = generate_random_id()
    print(f"{account=}")
    account.email = account.email.lower().strip()

    is_account_taken: bool = (
        db.query(Account).filter(Account.email == account.email).first()
    )

    if is_account_taken:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"The email {account.email} is taken. Please enter another email.",
        )

    # Hash password
    account.password = hash(account.password)

    # Add new account
    new_account = Account(**account.dict(), id=account_id)
    db.add(new_account)
    db.commit()
    db.refresh(new_account)

    # Create access token and return it
    access_token = create_access_token(data={"email": account.email})

    # Set default profile pic
    with open("assets/images/user-icon.png", "rb") as f:
        img = base64.b64encode(f.read())

    r.set(f"{account_id}-profile-pic", f"data:image/png;base64,{img.decode()}")

    return {"access_token": access_token, "token_type": "bearer"}


@router.post("/login", response_model=schemas.Token)
def login(
    user_credentials: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db),
):
    # return username and password
    account = (
        db.query(Account).filter(Account.email == user_credentials.username).first()
    )

    if not (account and verify(user_credentials.password, account.password)):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Invalid email or password"
        )

    # Create a token and return it
    access_token = create_access_token(data={"email": account.email})
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/logout")
def logout(
    token: str = Depends(get_token_user),
    db: Session = Depends(get_db),
    get_current_user: schemas.AccountOut = Depends(get_current_user),
):
    blacklist_id = generate_random_id()
    expired_account = BlackList(
        id=blacklist_id, token=token, email=get_current_user.email
    )
    db.add(expired_account)
    db.commit()
    db.refresh(expired_account)
    return {"detail": f"{get_current_user.email} has been logged out"}
