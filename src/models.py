from sqlalchemy import Column, String, Boolean, Integer, ARRAY, Float, JSON, ForeignKey
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.sqltypes import TIMESTAMP

from .database import Base


class Account(Base):
    __tablename__ = "accounts"

    id = Column(String, primary_key=True)
    email = Column(String, unique=True, nullable=False)
    first_name = Column(String, nullable=False)
    password = Column(String, nullable=False)
    created_at = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )
    is_admin = Column(Boolean, nullable=True, default=False, server_default="f")


class Wishlist(Base):
    __tablename__ = "wishlists"

    id = Column(String, primary_key=True)
    account_id = Column(
        String, ForeignKey("accounts.id", ondelete="CASCADE"), nullable=False
    )
    name = Column(String, nullable=False)
    stock_symbols = Column(ARRAY(String), server_default="{}")
    timestamp = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )


class TrendingArticle(Base):
    __tablename__ = "trending_articles"

    id = Column(String, primary_key=True)
    stock_symbols = Column(ARRAY(String), nullable=False)
    published_at = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )
    article_url = Column(String, nullable=False)
    thumbnail_url = Column(String, nullable=False)
    timestamp = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )


class RecommendedArticle(Base):
    __tablename__ = "recommended_articles"

    id = Column(String, primary_key=True)
    account_id = Column(
        String, ForeignKey("accounts.id", ondelete="CASCADE"), nullable=False
    )
    stock_symbols = Column(ARRAY(String), nullable=False)
    published_at = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )
    article_url = Column(String, nullable=False)
    thumbnail_url = Column(String, nullable=False)
    sentiment_result = Column(Float, nullable=False)


class PredictorModel(Base):
    __tablename__ = "predictor_models"

    id = Column(String, primary_key=True)
    account_id = Column(
        String, ForeignKey("accounts.id", ondelete="CASCADE"), nullable=False
    )
    timestamp = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )
    ticker_symbol = Column(String, nullable=False)
    days_to_predict = Column(Integer, nullable=False)
    number_of_simulations = Column(Integer, nullable=False)
    start_date = Column(TIMESTAMP(timezone=True), nullable=False)
    end_date = Column(TIMESTAMP(timezone=True), nullable=False)
    seed = Column(Integer, nullable=False)
    predicted_history = Column(ARRAY(Float), nullable=False)
    duration = Column(Float, nullable=False)


class BlackList(Base):
    __tablename__ = "blacklist"

    id = Column(String, primary_key=True)
    token = Column(String, nullable=False)
    email = Column(String, nullable=False)
    timestamp = Column(
        TIMESTAMP(timezone=True), nullable=False, server_default=text("now()")
    )
