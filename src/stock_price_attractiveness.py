import pandas as pd
# from src.monte_carlo.model import MonteCarloModel
from src.monte_carlo.model import (
    generate_predicted_prices,
    show_historical_and_predicted_price,
    graph_monte_carlo_simulations,
    graph_predicted_price_distribution,
    graph_returns_distribution,
)
from src.stock_data.stock_info import StockInfo
from src.stock_data.aggregator import StockDataAggregator

"""#TODO: 
    clamp attractiveness value between 0 and 1
    get volatility, get overall market sentiment (news etc.), weights for each metric,
    define default target values for each metric that can also be set (what is a 'good' P/E ratio?)) 
    NOTE: stockinfo.get_info() does not always produce same keys! use .get() to test rather than
    just indexing with ['index'] and test to see if it produced a value.
    need a source for sensible default targets or a way to calculate them; S&P missing
    certain values (such as forwardPE) in StockInfo.
    add inverse flag, max value flag, range flag, etc. to target values. Currently assumes
    that all higher values = better, which is not always true for certain metrics (lower P/E 
    ratios are better for example)
    """


class StockPriceAttractiveness:
    def __init__(
            self,
            ticker: str,
            number_of_days_to_predict: int = 252,
    ) -> None:
        self.ticker = ticker
        self.stock_info = StockInfo(self.ticker)
        self.stock_data_aggregator = StockDataAggregator([self.ticker])
        self.number_of_days_to_predict = number_of_days_to_predict
        self._current_price = None
        self._model = None
        self._current_info = None
        self._current_valuation_stats = None
        self._attractiveness = None
        self._target_benchmark_ticker = "SPY"  # s&p ticker
        self._target_benchmark_attractiveness_calculator = None
        self._target_value_template = {"weight": 1, "value": None},
        self._target_values = { #add way more factors!
            "annual_dividend_yield_percent": {"weight": 1, "inverse": False, "value": None},
            "price_to_book_ratio": {"weight": 1, "inverse": True, "value": None},
            "predicted_current_price_difference_percent": {"weight": 1, "inverse": False, "value": None}
        }

    def get_ticker(self):
        return self.ticker

    def get_current_info(self):
        if self._current_info is None:
            self._current_info = self.stock_info.get_info()
        return self._current_info

    def get_current_valuation_stats(self):
        if self._current_valuation_stats is None:
            self._current_valuation_stats = self.stock_info.get_valuation_stats()
        return self._current_valuation_stats

    def get_price_to_book_ratio(self):
        info = self.get_current_info()
        price_to_book = info.get('priceToBook')
        if price_to_book is None:
            price_to_book = 0
            print("Could not retrieve price to book for ticker", self.get_ticker(), "defaulting to 0.")
        return price_to_book

    def get_forward_price_to_earnings_ratio(self):
        info = self.get_current_info()
        price_to_earnings_ratio = info['forwardPE']
        return price_to_earnings_ratio

    def get_trailing_price_to_earnings_ratio(self):
        info = self.get_current_info()
        price_to_earnings_ratio = info['trailingPE']
        return price_to_earnings_ratio

    def get_price_to_earnings_growth_ratio(self):  # 5 year price to earnings growth ratio
        stats = self.get_current_valuation_stats()
        peg_row_index = 4
        peg_value_index = 1
        peg_row = stats.iloc[peg_row_index]
        peg = peg_row[peg_value_index]
        return peg

    def get_current_price(self):  # known issue: what if current_price is zero? (current price is used as divisor!)
        if self._current_price is None:
            self._current_price = self.stock_info.get_live_price()
        return self._current_price

    def get_latest_dividend(self):
        dividends_table = self.stock_info.get_dividends_table()
        last_index = dividends_table.count() - 1
        latest_dividend_row = dividends_table.iloc[last_index]
        latest_dividend = latest_dividend_row['dividend'].values[0]
        return latest_dividend

    # def calculate_annual_dividend_yield_percent(self):
    #     div_amount = self.get_latest_dividend()
    #     current_price = self.get_current_price()
    #     yearly_div_amount = div_amount * 4  # known issue: some stocks don't do dividends quarterly!
    #     annual_dividend_yield_percent = (yearly_div_amount / current_price) * 100
    #     return annual_dividend_yield_percent

    def set_target_value(self, name, value):  # weight optional.
        target_values = self.get_target_values()
        if name in target_values.keys():
            target_values[name] = value
        else:
            print("No such stat", name, "found in target_values!")

    def get_annual_dividend_yield_percent(self):
        info = self.get_current_info()
        annual_dividend_yield = info.get('trailingAnnualDividendYield')
        annual_dividend_yield_percent = 0
        if annual_dividend_yield is not None:
            annual_dividend_yield_percent = annual_dividend_yield * 100
        else:
            print("Could not retrieve annual dividend yield percent for ticker", self.get_ticker(), "defaulting to 0.")
        return annual_dividend_yield_percent

    # def get_model(self):
    #     historical_data = self.stock_data_aggregator.get_historical_stock_price()
    #     model = MonteCarloModel(stock_data=historical_data, number_of_days_to_predict=self.number_of_days_to_predict)
    #     self._model = model
    #     return self._model
    def get_predicted_price(self):
        #model = self.get_model()
        #predicted_price = model.predict_stock_price()
        historical_data = self.stock_data_aggregator.get_historical_stock_price()
        predicted_prices = generate_predicted_prices(historical_data)
        means = predicted_prices.mean(axis=1)
        price = means.iloc[-1]
        print("PREDICTED PRICE FOR", self.ticker, ":", price)
        return price

    def get_predicted_current_price_difference_percent(self):
        predicted_price = self.get_predicted_price()
        current_price = self.get_current_price()
        price_difference = predicted_price - current_price
        percent_difference = (price_difference / current_price) * 100
        return percent_difference

    def get_target_values(self):
        return self._target_values

    def get_target_benchmark_ticker(self):
        return self._target_benchmark_ticker

    def get_target_benchmark_attractiveness_calculator(self):
        if (self._target_benchmark_attractiveness_calculator is None
                or self.get_target_benchmark_ticker()
                != self._target_benchmark_attractiveness_calculator.get_ticker()):
            self._target_benchmark_attractiveness_calculator = StockPriceAttractiveness(
                self.get_target_benchmark_ticker())
        return self._target_benchmark_attractiveness_calculator

    def replace_blank_target_values(self):
        target_values = self.get_target_values()
        benchmark_ticker = self.get_target_benchmark_ticker()
        default_attractive_calc = self.get_target_benchmark_attractiveness_calculator()
        for i in target_values:
            if target_values[i]['value'] is None:
                val = default_attractive_calc.get_target_value_from_calculator_with_key(i)
                #print("Replaced target value", i, "with value from benchmark ticker", benchmark_ticker)
                target_values[i]['value'] = val

    def get_target_value_from_calculator_with_key(self, key): #calc is optional
        return getattr(self, 'get_' + key)()  # make cleaner

    def get_attractiveness(self):
        self._attractiveness = 0
        self.replace_blank_target_values()
        target_values = self.get_target_values()
        for i in target_values:
            actual_value = self.get_target_value_from_calculator_with_key(i)
            target_data = target_values[i]
            target_value = target_data['value']
            weight = target_data['weight']
            inverse = target_data['inverse']
            #print(i, "actual value:", actual_value, "target value:", target_value)
            change = 0
            if target_value != 0:
                change = (actual_value/target_value) * weight
                if inverse:
                    change = 1/change
            self._attractiveness += change
        self._attractiveness = self._attractiveness/len(target_values)
        return self._attractiveness


if __name__ == "__main__":
    test_ticker = "AAPL"
    test_ticker_2 = "AMC"
    sp_500_ticker = "SPY"  # s&p 500 ticker
    sp_500_attractiveness_calculator = StockPriceAttractiveness(sp_500_ticker)
    attractiveness_calculator = StockPriceAttractiveness(test_ticker)
    amc_attractiveness_calculator = StockPriceAttractiveness(test_ticker_2)
    dividend_test = attractiveness_calculator.get_latest_dividend()
    dividend_yield_test = attractiveness_calculator.get_annual_dividend_yield_percent()
    diff_percent_test = attractiveness_calculator.get_predicted_current_price_difference_percent()
    attractiveness_test = attractiveness_calculator.get_attractiveness()
    amc_attractiveness_test = amc_attractiveness_calculator.get_attractiveness()
    print("Test ticker:", test_ticker)
    print("Latest dividend amount:", dividend_test)
    print("Annual dividend yield:", dividend_yield_test, "%")
    print("Current price:", attractiveness_calculator.get_current_price(),
          "Predicted price:", attractiveness_calculator.get_predicted_price(),
          "Percent difference:", diff_percent_test, "%")
    print("P/B ratio:", attractiveness_calculator.get_price_to_book_ratio())
    print("Forward P/E ratio:", attractiveness_calculator.get_forward_price_to_earnings_ratio())
    print("Trailing P/E ratio:", attractiveness_calculator.get_trailing_price_to_earnings_ratio())
    print("PEG ratio: ", attractiveness_calculator.get_price_to_earnings_growth_ratio())
    print("Overall attractiveness:", test_ticker, attractiveness_test)
    print("Overall attractiveness:", test_ticker_2, amc_attractiveness_test)
