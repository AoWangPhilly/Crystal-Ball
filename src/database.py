from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base
from sqlalchemy_utils import database_exists, create_database, drop_database
import redis
import pandas as pd
import pyarrow as pa

from .config import settings

username = settings.database_username
password = settings.database_password
hostname = settings.database_hostname
port = settings.database_port
db_name = settings.database_name

SQLALCHEMY_DATABASE_URL = (
    f"postgresql://{username}:{password}@{hostname}:{port}/{db_name}"
)
engine = create_engine(SQLALCHEMY_DATABASE_URL)

Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

r = redis.Redis(
    host=settings.redis_hostname,
    port=settings.redis_port,
    password=settings.redis_password,
)


def cache_dataframe(account_id: str, key: str, df: pd.DataFrame) -> bool:
    return r.set(
        f"{account_id}-{key}",
        pa.serialize(df).to_buffer().to_pybytes(),
    )


def get_cached_dataframe(account_id: str, key: str) -> pd.DataFrame:
    return pa.deserialize(r.get(f"{account_id}-{key}"))


def get_db() -> None:
    db = Session()
    try:
        yield db
    finally:
        db.close()


@contextmanager
def get_db_context() -> None:
    db = Session()
    try:
        yield db
    finally:
        db.close()


def create_db() -> None:
    if not database_exists(engine.url):
        create_database(engine.url)
        print("DB created! :)")
    else:
        print("DB exists! :)")


if __name__ == "__main__":
    import sys
    import src.models as models

    _, build_or_drop = sys.argv
    if build_or_drop == "build":
        create_db()
        models.Base.metadata.create_all(bind=engine)
    elif build_or_drop == "drop":
        print("crystal-ball db dropped")
        drop_database(url=SQLALCHEMY_DATABASE_URL)
