from datetime import datetime, timedelta

from fastapi import Depends, status, HTTPException
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from sqlalchemy.orm.session import Session

from src.config import settings
from src.database import get_db, get_db_context
from src.models import Account, BlackList
from src.schemas import TokenData

# SECRET KEY
# Algorithm HS256
# Expiration time of token

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
# openssl rand -hex 32
SECRET_KEY = settings.secret_key
ALGORITHM = settings.algorithm
ACCESS_TOKEN_EXPIRE_MINUTES = settings.access_token_expire_minutes


def create_access_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        claims=to_encode, key=SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def verify_access_token(token: str, credentials_exception):
    try:
        payload = jwt.decode(
            token=token,
            key=SECRET_KEY,
            algorithms=[ALGORITHM]
        )
        email: str = payload.get("email")

        if email is None:
            raise credentials_exception

        with get_db_context() as db:
            blacklist_token = db.query(BlackList).filter(BlackList.token == token).first()

        if blacklist_token:
            raise credentials_exception

        token_data = TokenData(email=email)
    except JWTError:
        raise credentials_exception
    return token_data


def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    credentials_exception = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                          detail="Could not validate credentials",
                                          headers={"WWW-Authenticate": "Bearer"})
    token = verify_access_token(
        token=token, credentials_exception=credentials_exception)
    user = db.query(Account).filter(Account.email == token.email).first()
    return user


def get_token_user(token: str = Depends(oauth2_scheme)):
    return token
