import numpy as np
import pandas as pd
import datetime as dt

from src.utils import get_n_trading_days

import plotly
from scipy.stats import norm
import plotly.figure_factory as ff
import plotly.express as px
import plotly.graph_objects as go
from scipy.stats import norm
import plotly


def calculate_log_returns(stock_price: pd.DataFrame) -> pd.Series:
    return np.log(1 + stock_price.pct_change())


def graph_returns_distribution(
    stock_price: pd.DataFrame,
) -> plotly.graph_objs._figure.Figure:
    group_labels = ["value"]
    log_returns = calculate_log_returns(stock_price=stock_price.iloc[:, 0])

    fig = ff.create_distplot(
        [log_returns.dropna().values],
        group_labels=group_labels,
        show_rug=False,
        bin_size=0.003,
        colors=["#7F56D9"],
    )

    fig.update_layout(
        template="seaborn",
        title=f"{stock_price.columns[0]}'s Daily Returns",
        showlegend=False,
        xaxis_title="Daily Return",
        yaxis_title="Frequency",
    )

    fig.update_traces(
        hovertemplate="<br>".join(
            ["Daily Return: %{x:.3}", "Frequency: %{y}", "<extra></extra>"]
        )
    )  # Make hover text more informative
    return fig


def generate_predicted_prices(
    data: pd.DataFrame, end: dt.datetime, trials: int = 100_000
) -> pd.DataFrame:
    stock_price = data.iloc[:, 0]
    days = get_n_trading_days(start=stock_price.index[-1], end=end)

    Z = norm.ppf(np.random.rand(len(days), trials))
    log_returns = calculate_log_returns(stock_price=stock_price)
    mean = log_returns.mean()
    var = log_returns.var()
    stdev = log_returns.std()
    drift = mean - 0.5 * var
    daily_returns = np.exp(drift + stdev * Z)

    price_paths = np.zeros_like(daily_returns)
    price_paths[0] = stock_price.iloc[-1]
    for t in range(1, len(days)):
        price_paths[t] = price_paths[t - 1] * daily_returns[t]

    simultations_df = pd.DataFrame(price_paths, index=days)
    return simultations_df


def show_historical_and_predicted_price(
    data: pd.DataFrame,
    predicted_price: pd.DataFrame,
) -> plotly.graph_objs._figure.Figure:
    fig = go.Figure()
    mean_df = predicted_price.mean(axis=1)
    historical_price = data.iloc[:, 0]
    fig.add_traces(
        go.Scatter(
            x=historical_price.index,
            y=historical_price.values,
            name="Actual",
            hovertemplate="Month: %{x}<br>" "Price: $%{y:.2f}<br>" "<extra></extra>",
            line_color="#7F56D9",
        )
    )
    fig.add_traces(
        go.Scatter(
            x=mean_df.index,
            y=mean_df.values,
            name="Predicted",
            hovertemplate="Month: %{x}<br>" "Price: $%{y:.2f}<br>" "<extra></extra>",
            line_color="#fce217",
        )
    )

    start = historical_price.index[0].strftime("%m/%d/%y")
    end = predicted_price.index[-1].strftime("%m/%d/%y")
    fig.update_layout(
        template="seaborn",
        title=f"{data.columns[0]}'s Historical Stock Price and Predicted<br>({start} - {end})",
        xaxis_title="Date",
        yaxis_title="Price",
        updatemenus=[
            dict(
                type="buttons",
                y=0,
                showactive=True,
                buttons=list(
                    [
                        dict(
                            label="Average",
                            method="update",
                            args=[
                                {
                                    "y": [
                                        historical_price.values,
                                        predicted_price.mean(axis=1).values,
                                    ]
                                },
                                [1],
                            ],
                        ),
                        dict(
                            label="Max",
                            method="update",
                            args=[
                                {
                                    "y": [
                                        historical_price.values,
                                        predicted_price.max(axis=1).values,
                                    ]
                                },
                                [1],
                            ],
                        ),
                        dict(
                            label="Min",
                            method="update",
                            args=[
                                {
                                    "y": [
                                        historical_price.values,
                                        predicted_price.min(axis=1).values,
                                    ]
                                },
                                [1],
                            ],
                        ),
                    ]
                ),
            )
        ],
        xaxis=dict(rangeslider=dict(visible=True), type="date"),
    )
    return fig


def graph_monte_carlo_simulations(
    predicted_prices: pd.DataFrame, show_n: int = 10
) -> plotly.graph_objs._figure.Figure:
    fig = px.line(predicted_prices.iloc[:, :show_n], labels={"variable": "Simulations"})
    start_date = predicted_prices.index[0].strftime("%m/%d/%y")
    end_date = predicted_prices.index[1].strftime("%m/%d/%y")
    fig.update_layout(
        template="seaborn",
        title=f"Monte Carlo Simulations<br>({start_date} - {end_date})",
        xaxis_title="Date",
        yaxis_title="Price",
    )

    fig.update_traces(
        hovertemplate="Month: %{x}<br>" "Price: $%{y:.2f}<br>" "<extra></extra>",
    )  # Make hover text more informative
    return fig


def graph_predicted_price_distribution(
    predicted_prices: pd.DataFrame,
) -> plotly.graph_objs._figure.Figure:
    group_labels = ["value"]
    days = len(predicted_prices)
    fig = ff.create_distplot(
        [predicted_prices.iloc[-1].values],
        group_labels=group_labels,
        show_rug=False,
        bin_size=5,
        colors=["#7F56D9"],
    )
    full_fig = fig.full_figure_for_development(warn=False)
    _, y_max = full_fig.layout.yaxis.range
    y_ = np.linspace(0, y_max, 100)
    # Pull top 10% of possible outcomes
    top_ten = np.percentile(predicted_prices.iloc[-1], 100 - 10)

    # Pull bottom 10% of possible outcomes
    bottom_ten = np.percentile(predicted_prices.iloc[-1], 10)

    x_mean = np.full(y_.shape, predicted_prices.iloc[-1].mean())
    x_actual = np.full(y_.shape, predicted_prices.iloc[0, 0])

    fig.update_traces(
        hovertemplate="<br>".join(
            ["Price: $%{x:.2f}", "Frequency: %{y}", "<extra></extra>"]
        )
    )  # Make hover text more informative

    fig.add_traces(
        go.Scatter(
            x=x_mean,
            y=y_,
            hovertemplate="<br>".join(["Expected Price: $%{x:.2f}", "<extra></extra>"]),
            line=dict(color="firebrick", width=4, dash="dash"),
        )
    )

    fig.add_traces(
        go.Scatter(
            x=x_actual,
            y=y_,
            hovertemplate="<br>".join(
                ["Last Actual Price: $%{x:.2f}", "<extra></extra>"]
            ),
            line=dict(color="royalblue", width=4, dash="dash"),
        )
    )

    fig.update_layout(
        template="seaborn",
        title=f"Distributions of End Price Simulations after {days} days"
        if days > 1
        else f"Distributions of End Price Simulations after {days} day",
        showlegend=False,
        xaxis_title="Price",
        yaxis_title="Frequency",
    )

    return fig
