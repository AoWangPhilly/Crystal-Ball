import numpy as np
import pandas as pd
from typing import Dict


def calulate_expected_returns(predicted_price: pd.DataFrame) -> float:
    return predicted_price.iloc[-1].mean()


def calculate_returns(predicted_price: pd.DataFrame) -> float:
    expected_return = calulate_expected_returns(predicted_price=predicted_price)
    initial_price = predicted_price.iloc[0, 0]
    return 100 * (expected_return - initial_price) / expected_return


def calculate_beta(data: pd.DataFrame) -> float:
    # Calculate logarithmic daily returns
    sec_returns = np.log(data / data.shift(1))
    number_of_trading_days = 252
    # To calculate the beta, we need the covariance between the specific stock and the market...
    cov = (
        sec_returns.cov() * number_of_trading_days
    )  # Annualize by multiplying by 252 (trading days in a year)
    cov_with_market = cov.iloc[0, 1]

    # ...we also need the variance of the daily returns of the market
    market_var = sec_returns["^GSPC"].var() * number_of_trading_days

    # Calculate Beta
    beta = cov_with_market / market_var
    return beta


def calculate_capm(
    data: pd.DataFrame, risk_free: float = 0.025, risk_premium: float = 0.05
) -> float:
    beta = calculate_beta(data=data)
    return risk_free + beta * risk_premium


def calculate_sharpe_ratio(data: pd.DataFrame, risk_free: float = 0.025) -> float:
    number_of_trading_days = 252
    log_returns = np.log(data / data.shift(1))
    capm_return = calculate_capm(data=data)
    sharpe_ratio = (capm_return - risk_free) / (
        log_returns.iloc[:, 0].std() * number_of_trading_days**0.5
    )
    return sharpe_ratio


def calculate_stock_metrics(
    data: pd.DataFrame,
    predicted_price: pd.DataFrame,
    risk_free: float = 0.025,
    risk_premium: float = 0.05,
) -> Dict[str, float]:
    returns = calculate_returns(predicted_price=predicted_price)
    expected_return = calulate_expected_returns(predicted_price=predicted_price)
    beta = calculate_beta(data=data)
    capm = calculate_capm(data=data, risk_free=risk_free, risk_premium=risk_premium)
    sharpe_ratio = calculate_sharpe_ratio(data=data, risk_free=risk_free)
    return {
        "last_price": round(data.iloc[-1, 0], 2),
        "returns": round(returns, 2),
        "expected_return": round(expected_return, 2),
        "beta": round(beta, 2),
        "capm": round(capm, 2),
        "sharpe_ratio": round(sharpe_ratio, 2),
    }


def interpret_sharpe_ratio(sharpe_ratio: float) -> str:
    if sharpe_ratio >= 3:
        return (
            "A Sharpe Ratio of 3 or higher indicates a very strong level of risk-adjusted performance. "
            "It suggests that the investment or portfolio has provided a significantly higher excess "
            "return over the risk-free rate compared to its standard deviation of returns, "
            "indicating potential superior performance."
        )
    elif sharpe_ratio >= 2:
        return (
            "A Sharpe Ratio of 2 indicates a high level of risk-adjusted performance. "
            "It suggests that the investment or portfolio has provided a substantial "
            "excess return over the risk-free rate compared to its standard deviation "
            "of returns, indicating potentially attractive risk-adjusted performance."
        )
    elif sharpe_ratio >= 1:
        return (
            "A Sharpe Ratio of 1 indicates a reasonable level of risk-adjusted performance. "
            "It suggests that the investment or portfolio has provided an excess return over "
            "the risk-free rate that is equal to its standard deviation of returns, indicating "
            "that the investment's returns have adequately compensated for the level of risk taken."
        )
    else:
        return (
            "A Sharpe Ratio of less than one is considered unacceptable. "
            "The risk your portfolio encounters isn't being offset well enough by its return."
        )


def interpret_beta(beta: float) -> str:
    if beta > 1:
        return (
            "A beta greater than 1 suggests that the stock or portfolio may be more volatile "
            "than the overall market or benchmark, meaning it may have larger price swings in "
            "response to market movements. "
        )
    elif beta < 1:
        return (
            "A beta less than 1 suggests that the stock or portfolio may be less volatile than "
            "the overall market or benchmark, indicating it may have smaller price swings in "
            "response to market movements."
        )
    else:
        return (
            "A stock or portfolio with a beta close to 0 is often considered to be uncorrelated "
            "with the overall market or benchmark, meaning its returns are not influenced by general "
            "market movements."
        )
