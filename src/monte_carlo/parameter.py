from typing import Dict
from dateutil import parser
import datetime


from src.monte_carlo.error import (
    InvalidTimeRange,
    InvalidPredictionDate,
    InvalidSimulationNumber,
)


def check_parameters(parameters: Dict[str, float]) -> None:
    start_time = parser.parse(parameters["start_date"])
    end_time = parser.parse(parameters["end_date"])
    predict_to_time = parser.parse(parameters["predict_for_date"])

    if invalid_time_range(start_time, end_time):
        raise InvalidTimeRange("Start time must be before end time")
    elif invalid_predict_to_time(start_time, end_time, predict_to_time):
        raise InvalidPredictionDate("Prediction date must be after start and end time")
    elif invalid_number_of_simulations(parameters["number_of_simulations"]):
        raise InvalidSimulationNumber("Number of simulations must be greater than 0")


def invalid_time_range(
    start_time: datetime.datetime, end_time: datetime.datetime
) -> bool:
    return start_time >= end_time


def invalid_predict_to_time(
    start_time: datetime.datetime,
    end_time: datetime.datetime,
    predict_to_time: datetime.datetime,
) -> bool:
    return predict_to_time <= start_time or predict_to_time <= end_time


def invalid_number_of_simulations(number_of_simulations: int) -> bool:
    return number_of_simulations <= 0
