class InvalidParameter(Exception):
    ...


class InvalidTimeRange(InvalidParameter):
    ...


class InvalidPredictionDate(InvalidParameter):
    ...


class InvalidSimulationNumber(InvalidParameter):
    ...
