import datetime

import numpy as np
import pandas as pd

from src.monte_carlo.model import MonteCarloModel
from src.stock_data.aggregator import StockDataAggregator


class WellnessCalculator:
    def __init__(
            self,
            stock_data: pd.DataFrame,
            number_of_days_to_predict: int = 252,
            number_of_simulations: int = 1000
    ) -> None:
        self.stock_data = stock_data
        self.number_of_days_to_predict = number_of_days_to_predict
        self.number_of_simulations = number_of_simulations

    def calculate_accuracy(self, end_time: datetime.datetime) -> float:
        self.stock_data.index = pd.to_datetime(self.stock_data.index).date
        limited_view = self.stock_data[self.stock_data.index <= end_time.date()]
        model = MonteCarloModel(
            stock_data=limited_view,
            number_of_days_to_predict=self.number_of_days_to_predict
        )
        predicted_stock_price = model.predict_stock_price(
            number_of_simulations=self.number_of_simulations
        )
        days = self.number_of_days_to_predict
        predicted_date = (datetime.timedelta(days=days) + end_time).date()
        actual_price = self.stock_data[self.stock_data.index == predicted_date]["Adj Close"].values[0]

        while predicted_date not in self.stock_data.index:
            predicted_date += datetime.timedelta(days=1)
            actual_price = self.stock_data[self.stock_data.index == predicted_date]["Adj Close"].values[0]
        return (np.abs(predicted_stock_price - actual_price) / actual_price) * 100


if __name__ == "__main__":
    agg = StockDataAggregator(stock_symbols="AAPL")
    stock_data = agg.get_historical_stock_price()
    wellness = WellnessCalculator(stock_data=stock_data)
    print(wellness.calculate_accuracy(datetime.datetime.now() - datetime.timedelta(days=252)))
