from pydantic import BaseSettings


class Settings(BaseSettings):
    database_hostname: str
    database_port: str
    database_password: str
    database_username: str
    database_name: str
    news_api_key: str
    secret_key: str
    algorithm: str
    access_token_expire_minutes: int
    heroku_on: bool
    testing: bool
    mongodb_username: str
    mongodb_password: str
    redis_hostname: str
    redis_port: int
    redis_username: str
    redis_password: str

    class Config:
        env_file = ".env"


settings = Settings()
