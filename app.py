import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from a2wsgi import WSGIMiddleware
from fastapi.responses import RedirectResponse

from dashapp import create_dash_app
from src.config import settings
from src.routers import account, auth, wishlist, model

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(account.router)
app.include_router(auth.router)
app.include_router(wishlist.router)
app.include_router(model.router)


@app.get("/")
def main():
    return RedirectResponse("/dash")


# A bit odd, but the only way I've been able to get prefixing of the Dash app
# to work is by allowing the Dash/Flask app to prefix itself, then mounting
# it to root

if not settings.testing:
    dash_app = create_dash_app(requests_pathname_prefix="/dash/")
    app.mount("/dash", WSGIMiddleware(dash_app.server))

if __name__ == "__main__":
    uvicorn.run(app, port=8000)
