CREATE TABLE `accounts` (
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `created_at` datetime NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `email_UNIQUE` (`email`)
);

CREATE TABLE `predictor_models` (
  `email` varchar(256) NOT NULL,
  `model_id` int NOT NULL,
  `timestamp` datetime NOT NULL,
  `parameter` json NOT NULL,
  `output` json NOT NULL,
  `duration` float NOT NULL,
  `accuracy` float NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `email_UNIQUE` (`email`)
);

CREATE TABLE `recommended_articles` (
  `email` varchar(256) NOT NULL,
  `stock_symbols` json NOT NULL,
  `published_at` datetime NOT NULL,
  `article_url` varchar(2048) NOT NULL,
  `thumbnail_url` varchar(2048) NOT NULL,
  `sentiment_result` float NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `email_UNIQUE` (`email`)
);

CREATE TABLE `trending_volatile_articles` (
  `id` int NOT NULL,
  `stock_symbols` json NOT NULL,
  `published_at` datetime NOT NULL,
  `article_url` varchar(2048) NOT NULL,
  `thumbnail_url` varchar(2048) NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);

CREATE TABLE `wishlists` (
  `id` int NOT NULL,
  `email` varchar(256) NOT NULL,
  `name` varchar(64) NOT NULL,
  `stock_symbols` json DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);

/* Specify foreign keys */
ALTER TABLE recommended_articles 
ADD FOREIGN KEY (email) 
REFERENCES accounts(email);

ALTER TABLE wishlists 
ADD FOREIGN KEY (email) 
REFERENCES accounts(email);

ALTER TABLE predictor_models 
ADD FOREIGN KEY (email) 
REFERENCES accounts(email);
