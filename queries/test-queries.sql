/* test inserting data into accounts table */
INSERT INTO accounts
VALUES ('test1@test.com', 'pass1', 'Test1', '2001-12-31 23:59:59', 1);

INSERT INTO accounts
VALUES ('test2@test.com', 'pass2', 'Test2', '2022-11-22 23:59:59', 0);

INSERT INTO accounts
VALUES ('test3@test.com', 'pass3', 'Test3', '2022-11-22 23:59:59', 0);

/* test inserting data into wishlists table */
INSERT INTO wishlists VALUES (1, 'test1@test.com', 'example_wishlist', '["MSFT", "TSLA"]', '2001-12-31 23:59:59');

/* test selecting data from accounts table */
SELECT * FROM accounts;

SELECT email FROM accounts WHERE first_name = 'Test1';
